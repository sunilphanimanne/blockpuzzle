//
//  DynamicConstants.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 04/05/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "DynamicConstants.h"

#import "GameConstants.h"

const CGFloat iPhone4SHeight = 480.f;
const CGFloat iPhone5Or5SHeight = 568.f;

@implementation DynamicConstants

+ (CGFloat)masterBoarderLength
{
    CGFloat masterBoarderLength = 0.0f;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        masterBoarderLength = 48.5f;
    }else {
        masterBoarderLength = 28.5f;
    }
    
    return masterBoarderLength;
}

+ (CGFloat)puzzleBlockLength
{
    static CGFloat puzzleBlockLengthValue = 0;
    static dispatch_once_t puzzleBlockLengthToken;
    dispatch_once(&puzzleBlockLengthToken, ^{
        puzzleBlockLengthValue = [self computePuzzleBlockLength];
    });
    
    return puzzleBlockLengthValue;
}

+ (CGFloat)computePuzzleBlockLength
{
    CGFloat totalAvailableWidth = (GetScreenSize().width - [self masterBoarderLength] * 2);
    CGFloat puzzleBlockLength = (totalAvailableWidth/PuzzleColumns) - PuzzleBlockPadding;
    
    return puzzleBlockLength;
}

//TODO: Try to avoid redundancy...

+ (CGFloat)movingPuzzleBlockPadding
{
    static CGFloat movingPuzzleBlockPaddingValue = 0;
    static dispatch_once_t movingPuzzleBlockPaddingToken;
    dispatch_once(&movingPuzzleBlockPaddingToken, ^{
        movingPuzzleBlockPaddingValue = [self computeMovingPuzzleBlockPadding];
    });
    
    return movingPuzzleBlockPaddingValue;
}

+ (CGFloat)computeMovingPuzzleBlockPadding
{
    CGFloat padding = 0.;
    
    padding += PuzzleBlockPadding;
    
    CGFloat puzzleBlockLength = [self puzzleBlockLength];
    CGFloat puzzleBlockLengthWhileMoving = puzzleBlockLength * BlockShapeStateMovingScale;
    
    padding += (puzzleBlockLength - puzzleBlockLengthWhileMoving);
    
    DDLogDebug(@"bufferedPuzzleBlockPadding:%f", padding);
    
    return padding;
}

+ (CGFloat)scoreIndicatorFontSize
{
    static CGFloat scoreIndicatorFontSizeValue = 0;
    static dispatch_once_t scoreIndicatorFontSizeToken;
    dispatch_once(&scoreIndicatorFontSizeToken, ^{
        scoreIndicatorFontSizeValue = [self computeScoreIndicatorFontSize];
    });
    
    return scoreIndicatorFontSizeValue;
}

+ (CGFloat)computeScoreIndicatorFontSize
{
    CGFloat computedFontSize = 0;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        computedFontSize = GameSceneScoreIndicatorFontSize4S;
    }else {
        computedFontSize = GameSceneScoreIndicatorFontSize;
    }
    
    return computedFontSize;
}

+ (CGFloat)scoreLiveValueFontSize
{
    static CGFloat scoreValueFontSizeValue = 0;
    static dispatch_once_t scoreValueFontSizeToken;
    dispatch_once(&scoreValueFontSizeToken, ^{
        scoreValueFontSizeValue = [self computeScoreValueFontSize];
    });
    
    return scoreValueFontSizeValue; 
}

+ (CGFloat)computeScoreValueFontSize
{
    CGFloat computedFontSize = 0;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        computedFontSize = GameSceneScoreLiveValueFontSize4S;
    }else {
        computedFontSize = GameSceneScoreLiveValueFontSize;
    }
    
    return computedFontSize;
}

+ (CGFloat)hiScoreLiveValueFontSize
{
    static CGFloat hiScoreValueFontSizeValue = 0;
    static dispatch_once_t hiscoreValueFontSizeToken;
    dispatch_once(&hiscoreValueFontSizeToken, ^{
        hiScoreValueFontSizeValue = [self computeHiScoreValueFontSize];
    });
    
    return hiScoreValueFontSizeValue;
}

+ (CGFloat)computeHiScoreValueFontSize
{
    CGFloat computedFontSize = 0;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        computedFontSize = GameSceneHighScoreLiveValueFontSize4S;
    }else {
        computedFontSize = GameSceneHighScoreLiveValueFontSize;
    }
    
    return computedFontSize;
}

+ (CGFloat)scoreLabelTopPadding
{
    static CGFloat scoreLabelTopPaddingValue = 0;
    static dispatch_once_t scoreLabelTopPaddingToken;
    dispatch_once(&scoreLabelTopPaddingToken, ^{
        scoreLabelTopPaddingValue = [self computeScoreLabelTopPadding];
    });
    
    return scoreLabelTopPaddingValue;
}

+ (CGFloat)computeScoreLabelTopPadding
{
    CGFloat computeScoreLabelTopPadding = 0;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        computeScoreLabelTopPadding = heightInPercent(13.5);
    }else {
        computeScoreLabelTopPadding = heightInPercent(9.0);
    }
    
    return computeScoreLabelTopPadding;
}

+ (CGFloat)hiScoreLabelBottomPadding
{
    static CGFloat hiScoreLabelBottomPaddingValue = 0;
    static dispatch_once_t hiScoreLabelBottomPaddingToken;
    dispatch_once(&hiScoreLabelBottomPaddingToken, ^{
        hiScoreLabelBottomPaddingValue = [self computeHiScoreLabelBottomPadding];
    });
    
    return hiScoreLabelBottomPaddingValue;
}

+ (CGFloat)computeHiScoreLabelBottomPadding
{
    CGFloat computeHiScoreLabelBottomPadding = 0;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        computeHiScoreLabelBottomPadding = heightInPercent(-9.0);
    }else {
        computeHiScoreLabelBottomPadding = heightInPercent(-4.0);
    }
    
    return computeHiScoreLabelBottomPadding;
}

+ (CGFloat)scoreIndicatorTopPadding
{
    static CGFloat scoreIndicatorTopPaddingValue = 0;
    static dispatch_once_t scoreIndicatorTopPaddingToken;
    dispatch_once(&scoreIndicatorTopPaddingToken, ^{
        scoreIndicatorTopPaddingValue = [self computeScoreIndicatorTopPadding];
    });
    
    return scoreIndicatorTopPaddingValue;
}

+ (CGFloat)computeScoreIndicatorTopPadding
{
    CGFloat computeScoreIndicatorTopPadding = 0;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        computeScoreIndicatorTopPadding = heightInPercent(12.5);
    }else {
        computeScoreIndicatorTopPadding = heightInPercent(7.5);
    }
    
    return computeScoreIndicatorTopPadding;
}

+ (CGFloat)gameRestartAlertFontSize
{
    static CGFloat gameRestartAlertFontSize = 0;
    static dispatch_once_t gameRestartAlertFontToken;
    dispatch_once(&gameRestartAlertFontToken, ^{
        gameRestartAlertFontSize = [self computeGameRestartAlertFontSize];
    });
    
    return gameRestartAlertFontSize;
}

+ (CGFloat)computeGameRestartAlertFontSize
{
    CGFloat computedFontSize = 0;
    if ([self isiPhone4SOrBelow]) {
        computedFontSize = GameRestartAlertFontSize4S;
    }else {
        computedFontSize = GameRestartAlertFontSize;
    }
    
    return computedFontSize;
}

+ (CGFloat)leaderBoardErrorAlertFontSize
{
    static CGFloat leaderBoardErrorAlertFontSize = 0;
    static dispatch_once_t leaderBoardErrorAlertFontSizeToken;
    dispatch_once(&leaderBoardErrorAlertFontSizeToken, ^{
        leaderBoardErrorAlertFontSize = [self computeLeaderBoardErrorAlertFontSize];
    });
    
    return leaderBoardErrorAlertFontSize;
}

+ (CGFloat)computeLeaderBoardErrorAlertFontSize
{
    CGFloat computedFontSize = 0;
    if ([self isiPhone4SOrBelow]) {
        computedFontSize = LeaderBoardAlertFontSize4S;
    }else {
        computedFontSize = LeaderBoardAlertFontSize;
    }
    
    return computedFontSize;
}

+ (CGFloat)gameMusicVolume
{
    CGFloat gameMusicVolume = 1.0;
    if ([self isiPhone4SOrBelow]) {
        gameMusicVolume = 0.1;
    }else {
        gameMusicVolume = 0.03;
    }
    
    return gameMusicVolume;
}

+ (CGSize)itemSizeForSidebar
{
    CGSize itemSize = CGSizeMake(75, 75);
    
    if ([self isiPhone4SOrBelow]) {
        itemSize = CGSizeMake(60, 60);
    }else if([self isiPhone5Or5S]) {
        itemSize = CGSizeMake(70, 70);
    }else {
        //{75, 75}
    }
    
    return itemSize;
}

+ (BOOL)isiPhone4SOrBelow
{
    BOOL isiPhone4S = NO;
    
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        isiPhone4S = YES;
    }
    
    return isiPhone4S;
}

+ (BOOL)isiPhone5Or5S
{
    BOOL isiPhone5Or5S = NO;
    
    CGFloat height = GetScreenSize().height;
    
    if (height == iPhone5Or5SHeight) {
        isiPhone5Or5S = YES;
    }
    
    return isiPhone5Or5S;
}

+ (CGFloat)puzzleBufferBlockYOffset
{
    CGFloat blockYOffsetPercent = 0.f;
    CGFloat height = GetScreenSize().height;
    if (height <= iPhone4SHeight) {
        blockYOffsetPercent = 10.0f;
    }else {
        blockYOffsetPercent = 4.5f;
    }
    
    return heightInPercent(blockYOffsetPercent);
}

+ (CGSize)puzzleBlockBoardSize
{
    CGFloat boardHeight = 0.f;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        boardHeight = heightInPercent(5.5f);
    }else {
        boardHeight = heightInPercent(12.5f);
    }
    
    return CGSizeMake(GetScreenSize().width, boardHeight);
}

+ (CGSize)sidebarMenuSize
{
    CGSize sidebarMenuSize = CGSizeZero;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        sidebarMenuSize = CGSizeMake(widthInPercent(24.f), heightInPercent(24.f));
    }else {
        sidebarMenuSize = CGSizeMake(widthInPercent(24.f), heightInPercent(20.f));
    }
    
    return sidebarMenuSize;
}

+ (CGPoint)sidebarMenuPosition
{
    CGPoint sidebarMenuPosition = CGPointZero;
    CGFloat height = GetScreenSize().height;
    
    if (height <= iPhone4SHeight) {
        sidebarMenuPosition = CGPointMake(50, CGRectGetMidY(GetScoreboardRect()) - heightInPercent(5.f));
    }else {
        sidebarMenuPosition = CGPointMake(50, CGRectGetMidY(GetScoreboardRect()));
    }
    
    return sidebarMenuPosition;
}

@end


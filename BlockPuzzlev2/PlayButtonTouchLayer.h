//
//  PlayButtonTouchLayer.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 01/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "TouchLayer.h"

@interface PlayButtonTouchLayer : TouchLayer

@property (nonatomic, weak) SKSpriteNode* playButton;

@end

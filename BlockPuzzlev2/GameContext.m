//
//  GameContext.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 11/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "GameContext.h"

#import "AdContext.h"

@implementation GameContext

+ (instancetype)sharedContext {
    static GameContext *sharedContext = nil;
    static dispatch_once_t gameContextToken;
    dispatch_once(&gameContextToken, ^{
        sharedContext = [[self alloc] init];
        sharedContext->_adContext = [AdContext sharedContext];
    });
    return sharedContext;
}

- (void)setViewController:(UIViewController *)viewController
{
    if (![_viewController isEqual:viewController]) {
        _viewController = viewController;
        _adContext.viewController = viewController;
    }
}

@end

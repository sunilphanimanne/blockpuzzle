//
//  LevelListener.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 06/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LevelListener <NSObject>

@required
- (void)gameAdvancedToNewLevel:(NSUInteger)newLevel
                      oldLevel:(NSUInteger)oldLevel
                       context:(NSDictionary *)context;

@end

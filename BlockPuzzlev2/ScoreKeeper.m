//
//  ScoreKeeper.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 27/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "ScoreKeeper.h"

#import "GameScene.h"
#import "GameConstants.h"
#import "ScoreListener.h"
#import "PersistanceManager.h"
#import "ScoreKeeper+Private.h"

NSString *const ScoreKeeperPersistanceKey = @"Persistence.ScoreKeeperKey";
NSUInteger const allClearScore = 50;

@implementation ScoreKeeper

Player *_player;
NSMutableArray *_scoreListeners;
BOOL _overtookHighScore;
NSDictionary *_highScoreContext;
NSDictionary *_currentScoreContext;
BOOL _isAllClearScore = NO;

- (instancetype)init
{
    if (self = [super init]) {
        _currentScore = 0;
        _highScore = 0;
        _overtookHighScore = NO;
        _scoreListeners = [NSMutableArray array];
    }
    
    return self;
}

- (instancetype)initWithPlayer:(Player *)player
{
    if (self = [self init]) {
        _player = player;
        
        [self subscribeForScoreUpdates:_player];
        
        _highScore = _player.highScore;
    }
    
    return self;
}

- (void)incrementScoreForPlacingBlock:(BlockShape *)block
{
    //Note:Usage of property is intentional, setter has got some logic. DONOT bypass.
    self.currentScore += [self scoreForPlacingBlock:block];
    
    DDLogDebug(@"+++ Block Placed Score +++");
}

- (void)incrementScoreForClearingAllBlocks
{
    _isAllClearScore = YES;
    self.currentScore += allClearScore;
    _isAllClearScore = NO;
}

- (NSUInteger)scoreForPlacingBlock:(BlockShape *)block
{
    return [GameUtils numberOfMinuteBlocksInShape:block];
}

- (void)incrementScoreForFinishingLines:(NSUInteger)numberOfLines
                               forBlock:(BlockShape *)block
{
    //Note:Usage of property is intentional, setter has got some logic. DONOT bypass.
    NSUInteger cumulativeScore = [self scoreForPlacingBlock:block] +
                                        (numberOfLines * PuzzleColumns);
    self.currentScore += cumulativeScore;
    
    DDLogDebug(@"*** Line Done & Block Placed Score ***");
}

- (void)setCurrentScore:(int64_t)currentGameScore
{
    if (currentGameScore != _currentScore) {
        int64_t oldValue = _currentScore;
        _currentScore = currentGameScore;
        
        [self prepareCurrentScoreContext];
        
        [self sendUpdatedValue:_currentScore
                      oldValue:oldValue
                       forType:ScoreKeeperScoreTypeScore
                       context:_currentScoreContext];
        
        //Reset currentScoreContexgt
        [self resetCurrentScoreContext];
        
        if (_currentScore > _highScore) {
            //Important: prepare the context, before updating the highscore!
            [self prepareHighScoreContext];
            
            //Note:Usage of property is intentional, setter has got some logic. DONOT bypass.
            self.highScore = _currentScore;
        }
        
        DDLogDebug(@"Current Score:%ld", (unsigned long)_currentScore);
        DDLogDebug(@"High Score:%ld", (unsigned long)self.highScore);
    }
}

- (void)prepareCurrentScoreContext
{
    if (_isAllClearScore) {
        _currentScoreContext = @{@"AllClearScore":@(YES)};
        _overtookHighScore = YES;
    }
}

- (void)resetCurrentScoreContext
{
    _currentScoreContext = nil;
}

- (void)prepareHighScoreContext
{
    if (!_overtookHighScore) {
        _highScoreContext = @{@"overtookHighScore":@(YES)};
        _overtookHighScore = YES;
    }
}

- (void)resetHighScoreContext
{
   _highScoreContext = nil;
}

- (void)setHighScore:(int64_t)highScore
{
    if (highScore != _highScore) {
        int64_t oldValue = _highScore;
        _highScore = highScore;
        
        [self sendUpdatedValue:_highScore
                      oldValue:oldValue
                       forType:ScoreKeeperScoreTypeHighScore
                       context:_highScoreContext];
        
        //Important:Don't forget to reset the context...
        [self resetHighScoreContext];
    }
}

- (void)sendUpdatedValue:(int64_t)newValue
                oldValue:(int64_t)oldValue
                 forType:(ScoreKeeperScoreType)type
                 context:(NSDictionary *)contextDict
{
    for (id <ScoreListener>aListener in _scoreListeners) {
        [aListener scoreType:type newValue:newValue oldValue:oldValue context:contextDict];
    }
}


#pragma mark - Score Listening methods

- (BOOL)subscribeForScoreUpdates:(id <ScoreListener>)listener
{
    BOOL isSuccess = NO;
    
    if (listener && ![_scoreListeners containsObject:listener]) {
        [_scoreListeners addObject:listener];
        isSuccess = YES;
    }
    
    return isSuccess;
}

- (BOOL)unsubscribeForScoreUpdates:(id <ScoreListener>)listener
{
    BOOL isSuccess = NO;
    
    if (listener && [_scoreListeners containsObject:listener]) {
        [_scoreListeners removeObject:listener];
        isSuccess = YES;
    }
    
    return isSuccess;
}

- (BOOL)unsubscribeAllListeners
{
    BOOL isSuccess = NO;
    
    if ([_scoreListeners count]) {
        [_scoreListeners removeAllObjects];
        isSuccess = YES;
    }
    
    return isSuccess;
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end

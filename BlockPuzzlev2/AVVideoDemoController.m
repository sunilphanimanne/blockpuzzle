//
//  AVVideoDemoController.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 16/01/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import "AVVideoDemoController.h"

#import "PlayerManager.h"

#define CURRENT_PLAYER [PlayerManager sharedManager].currentPlayer

@interface AVVideoDemoController () <UIGestureRecognizerDelegate>
{
@private
    NSURL *_videoURL;
    id _demoObserver;
    id _resignActiveObserver;
    id _becomeActiveObserver;
    id _foregroundObserver;
    UITapGestureRecognizer *_doubleTapGesture;
    UIViewController *_viewController;
    void (^_playDemoAction)(void);
}
@end

@implementation AVVideoDemoController

- (instancetype)initWithVideoName:(NSString *)videoName
          presentOnViewController:(UIViewController *)viewController
{
    self = [super init];
    if (self) {
        [self doInitialisation:videoName
                viewController:viewController];
    }
    return self;
}

- (void)setupViewsOnViewController:(UIViewController *)viewController
{
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    [viewController.view addSubview:self.view];
    self.view.tag = 007;
    
    self.view.frame = viewController.view.frame;
    self.showsPlaybackControls = NO;
    
    [self createButtonsAndHide:self];
    
    [self showOrHideButton:[self getSkipDemoButton] show:YES];
    
    [self setupDoubleTapGesture:self];
}

- (void)doInitialisation:(NSString *)videoName viewController:(UIViewController *)viewController
{
    if (videoName) {
        _videoURL = [[NSBundle mainBundle] URLForResource:videoName
                                            withExtension:nil];
    
        self.player = [AVPlayer playerWithURL:_videoURL];
    }
    
    _viewController = viewController;
}

- (void)createButtonsAndHide:(AVVideoDemoController *)videoController
{
    UIButton *playGamebutton = [self createPlayGameButton];
    [self addButtonAndBringToFront:playGamebutton toController:videoController];
    
    UIButton *replayVideoButton = [self createReplayButton];
    [self addButtonAndBringToFront:replayVideoButton toController:videoController];
    
    UIButton *resumeVideoButton = [self createResumeButton];
    [self addButtonAndBringToFront:resumeVideoButton toController:videoController];
    
    UIButton *skipVideoButton = [self createSkipButton];
    [self addButtonAndBringToFront:skipVideoButton toController:videoController];
}

- (void)setupDoubleTapGesture:(AVVideoDemoController *)videoController
{
    if (!_doubleTapGesture) {
        _doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                    action:@selector(handleDoubleTapGesture:)];
        _doubleTapGesture.numberOfTapsRequired = 2;
        [videoController.view addGestureRecognizer:_doubleTapGesture];
        _doubleTapGesture.delegate = self;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldBeRequiredToFailByGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    BOOL result = NO;
    if ((gestureRecognizer == _doubleTapGesture) &&
        [[otherGestureRecognizer view] isDescendantOfView:[gestureRecognizer view]]) {
        result = YES;
    }
    return result;
}

-(void)handleDoubleTapGesture:(UIGestureRecognizer*)gestureRecognizer
{
    DDLogDebug(@"Killing the double tap gesture!");
}

- (void)addButtonAndBringToFront:(UIButton *)button toController:(AVVideoDemoController *)videoController
{
    [videoController.view addSubview:button];
    [videoController.view bringSubviewToFront:button];
}

- (UIButton *)createPlayGameButton
{
    CGPoint position = CGPointMake(CENTERX, CENTERY + 28.75 /* height/2 */);
    
    
    return [self getButtonWithName:@"PlayGame"
                          position:position
                               tag:123
                            action:@selector(playGameAction:)];
}

- (UIButton *)createReplayButton
{
    CGPoint position = CGPointMake(CENTERX, CENTERY + 57.5 /* height */ + 50 /* Padding */);
    
    return [self getButtonWithName:@"ReplayDemo"
                          position:position
                               tag:456
                            action:@selector(replayAction:)];
}

- (UIButton *)createResumeButton
{
    CGPoint position = CGPointMake(CENTERX, CENTERY + 57.5 /* height */ + 50 /* Padding */);
    
    return [self getButtonWithName:@"ResumeDemo"
                          position:position
                               tag:789
                            action:@selector(resumeAction:)];
}

- (UIButton *)createSkipButton
{
    UIImage *wrongMarkImage = [UIImage imageNamed:@"error"];
    
    CGPoint position = CGPointMake(GetScreenRect().origin.x +
                                   GetScreenSize().width -
                                   wrongMarkImage.size.width,
                                   GetScreenRect().origin.y +
                                   wrongMarkImage.size.height);
    
    return [self getButtonWithName:@"error"
                          position:position
                               tag:987
                            action:@selector(playGameAction:)];
}

- (UIButton *)getButtonWithName:(NSString *)name
                       position:(CGPoint)position
                            tag:(NSInteger)tag
                         action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = tag;
    
    [button addTarget:self
               action:action
     forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *playImage = [UIImage imageNamed:name];
    [button setImage:playImage forState:UIControlStateNormal];
    [button sizeToFit];
    button.alpha = 0.7;
    button.layer.zPosition = 1;
    
    button.frame = CGRectMake(position.x - button.frame.size.width/2,
                              position.y - button.frame.size.width/2,
                              button.frame.size.width,
                              button.frame.size.height);
    
    button.hidden = YES;
    
    return button;
}

- (void)playGameAction:(id)sender
{
    [self removeButtons];
 
    [self setDemoCompletedForUser];
    
    if (_playDemoAction) {
        _playDemoAction();
    }
    
    [self cleanupVideoPlayer];
}

- (void)replayAction:(id)sender
{
    CURRENT_PLAYER.demoStatus = PlayerDemoStatusNotStarted;
    
    [self resumeAction:sender];
}

- (void)resumeAction:(id)sender
{
    [self showButtonPalette:NO];
    
    __block void (^action)(void) = [_playDemoAction copy];
    
    [self playDemo:^{
        if (action) {
            action();
            action = nil;
        }
    }];
}

- (void)cleanupVideoPlayer
{
    [self unregisterForNotifications];
    
    UIView *videoView = [self.view viewWithTag:007];
    [self willMoveToParentViewController:nil];
    
    [videoView removeFromSuperview];
    videoView = nil;
    
    [self.player pause];
    self.player = nil;
    
    [self removeFromParentViewController];
}

- (void)unregisterForNotifications
{
    [self unregisterForVideoEnded];
    [self unregisterForAppWillEnterForegroundNotification];
}

- (void)removeButtons
{
    UIButton *continueButton = [self getPlayGameButton];
    [continueButton removeFromSuperview];
    
    UIButton *replayButton = [self getReplayDemoButton];
    [replayButton removeFromSuperview];
}

- (UIButton *)getPlayGameButton
{
    return [self getButtonWithTag:123];
}

- (UIButton *)getReplayDemoButton
{
    return [self getButtonWithTag:456];
}

- (UIButton *)getResumeDemoButton
{
    return [self getButtonWithTag:789];
}

- (UIButton *)getSkipDemoButton
{
    return [self getButtonWithTag:987];
}

- (void)showButtonPalette:(BOOL)show
{
    [self showButtonPalette:show
              forDemoStatus:CURRENT_PLAYER.demoStatus];
}

- (void)showButtonPalette:(BOOL)show
            forDemoStatus:(PlayerDemoStatus)demoStatus
{
    if (show) {
        switch (demoStatus) {
            case PlayerDemoStatusNotStarted:
                //Nothing
                break;
                
            case PlayerDemoStatusStarted:
                [self showPlayGameButton:show];
                [self showResumeDemoButton:show];
                break;
                
            case PlayerDemoStatusEnded:
                [self showPlayGameButton:show];
                [self showReplayDemoButton:show];
                break;
        }
    }else {
        [self showPlayGameButton:NO];
        [self showResumeDemoButton:NO];
        [self showReplayDemoButton:NO];
    }
}

- (void)showPlayGameButton:(BOOL)show
{
    UIButton *button = [self getPlayGameButton];
    [self showOrHideButton:button show:show];
}

- (void)showReplayDemoButton:(BOOL)show
{
    UIButton *button = [self getReplayDemoButton];
    [self showOrHideButton:button show:show];
}

- (void)showResumeDemoButton:(BOOL)show
{
    UIButton *button = [self getResumeDemoButton];
    [self showOrHideButton:button show:show];
}

- (void)showSkipDemoButton:(BOOL)show
{
    UIButton *button = [self getResumeDemoButton];
    [self showOrHideButton:button show:show];
}

- (void)showOrHideButton:(UIButton *)button show:(BOOL)show
{
    button.hidden = !show;
}

- (BOOL)isButtonPaletteShown
{
    UIButton *playButton = [self getPlayGameButton];
    
    return !playButton.hidden;
}

- (UIButton *)getButtonWithTag:(NSInteger)tag
{
    return [self.view viewWithTag:tag];
}

- (void)registerVideoEnded:(void (^)())handler
{
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [self unregisterForVideoEnded];
    
    if (handler) {
        _demoObserver = [defaultCenter addObserverForName:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:self.player.currentItem
                                                    queue:[NSOperationQueue mainQueue]
                                               usingBlock:^(NSNotification * _Nonnull note) {
                                                   if (handler) {
                                                       handler();
                                                   }
                                               }];
    }
}

- (void)unregisterForVideoEnded
{
    if (_demoObserver) {
        [[NSNotificationCenter defaultCenter] removeObserver:_demoObserver];
    }
}

- (void)registerForAppWillEnterForegroundNotification:(void (^)())handler
{
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [self unregisterForAppWillEnterForegroundNotification];
    
    if (handler) {
        _foregroundObserver = [defaultCenter addObserverForName:UIApplicationWillEnterForegroundNotification
                                                         object:[UIApplication sharedApplication]
                                                          queue:[NSOperationQueue mainQueue]
                                                     usingBlock:^(NSNotification * _Nonnull note) {
                                                         if (handler) {
                                                             handler();
                                                         }
                                                     }];
    }
}

- (void)unregisterForAppWillEnterForegroundNotification
{
    if (_foregroundObserver) {
        [[NSNotificationCenter defaultCenter] removeObserver:_foregroundObserver];
    }
}

- (void)registerForAppWillResignActiveNotification:(void (^)())handler
{
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [self unregisterForAppWillResignActiveNotification];
    
    if (handler) {
        _resignActiveObserver = [defaultCenter addObserverForName:UIApplicationWillResignActiveNotification
                                                         object:[UIApplication sharedApplication]
                                                          queue:[NSOperationQueue mainQueue]
                                                     usingBlock:^(NSNotification * _Nonnull note) {
                                                         if (handler) {
                                                             handler();
                                                         }
                                                     }];
    }
}

- (void)unregisterForAppWillResignActiveNotification
{
    if (_resignActiveObserver) {
        [[NSNotificationCenter defaultCenter] removeObserver:_resignActiveObserver];
    }
}

- (void)registerForAppDidBecomActiveNotification:(void (^)())handler
{
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    
    [self unregisterForAppDidBecomActiveNotification];
    
    if (handler) {
        _becomeActiveObserver = [defaultCenter addObserverForName:UIApplicationDidBecomeActiveNotification
                                                           object:[UIApplication sharedApplication]
                                                            queue:[NSOperationQueue mainQueue]
                                                       usingBlock:^(NSNotification * _Nonnull note) {
                                                           if (handler) {
                                                               handler();
                                                           }
                                                       }];
    }
}

- (void)unregisterForAppDidBecomActiveNotification
{
    if (_becomeActiveObserver) {
        [[NSNotificationCenter defaultCenter] removeObserver:_becomeActiveObserver];
    }
}

- (void)playDemo:(void (^)())completion
{
    __block BOOL isDemoPausedDueToInactivity = NO;
    
    if (completion) {
        if (_playDemoAction) {
            _playDemoAction = nil;
        }
        
        _playDemoAction = [completion copy];
    }
    
    [self setupViewsOnViewController:_viewController];
    
    PlayerDemoStatus demoStatus = CURRENT_PLAYER.demoStatus;
    
    [self registerVideoEnded:^{
        [self setDemoCompletedForUser];
        [self showOrHideButton:[self getSkipDemoButton] show:NO];
        [self showButtonPalette:YES];
    }];
    
    [self registerForAppWillEnterForegroundNotification:^{
        [self appBecameActiveHandler];
    }];
    
    [self registerForAppWillResignActiveNotification:^{
        isDemoPausedDueToInactivity = YES;
        [self.player pause];
    }];
    
    [self registerForAppDidBecomActiveNotification:^{
        if (isDemoPausedDueToInactivity) {
            isDemoPausedDueToInactivity = NO;
            [self appBecameActiveHandler];
        }
    }];
    
    if (demoStatus == PlayerDemoStatusNotStarted ||
        demoStatus == PlayerDemoStatusEnded) {
        //Update the status
        CURRENT_PLAYER.demoStatus = PlayerDemoStatusStarted;
        
        //and play!
        [self.player seekToTime:kCMTimeZero];
        [self.player play];
    }else {
        [self.player play];
    }
}

- (void)appBecameActiveHandler
{
    [self showOrHideButton:[self getSkipDemoButton] show:NO];
    if (![self isButtonPaletteShown]) {
        [self showButtonPalette:YES forDemoStatus:PlayerDemoStatusStarted];
    }
}

- (void)playDemoIfRequired:(void (^)())completion
{
    PlayerDemoStatus demoStatus = CURRENT_PLAYER.demoStatus;
    
    if (demoStatus != PlayerDemoStatusEnded) {
        [self playDemo:completion];
    }else {
        if (completion) {
            completion();
        }
    }
}

- (void)setDemoCompletedForUser
{
    PlayerManager *playerManager = [PlayerManager sharedManager];
    playerManager.currentPlayer.demoStatus = PlayerDemoStatusEnded;
    
    [playerManager save];
}

@end

//
//  ThemeManager.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "ThemeManager.h"

#import "GameUtils.h"
#import "ThemeListener.h"
#import "PersistanceManager.h"

NSString *const ThemeManagerPersistanceKey = @"Persistence.ThemeManagerKey";

@interface ThemeManager ()
{
    @private
        NSMutableArray *_themeListeners;
        BOOL _isCurrentThemeInsync;
}

@end

@implementation ThemeManager

- (instancetype)init
{
    if (self = [super init]) {
        _themeListeners = [NSMutableArray arrayWithCapacity:1];
        _isCurrentThemeInsync = NO;
    }
    
    return self;
}

#pragma mark - NSDecoder methods

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [self init]) {
        _currentTheme = [aDecoder decodeObjectForKey:ThemeManagerCurrentThemeDataKey];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_currentTheme forKey:ThemeManagerCurrentThemeDataKey];
}

+ (instancetype)loadInstance
{
    return [PersistanceManager loadInstanceForClass:self
                                             andKey:ThemeManagerPersistanceKey];
}

- (void)save
{
    [PersistanceManager saveObject:self forKey:ThemeManagerPersistanceKey];
}

#pragma mark - Theme manager related methods

- (BOOL)applyTheme:(Theme *)theme
{
    BOOL isThemeApplied = NO;
    BOOL shouldApply = NO;
    
    if ([theme isKindOfClass:[Theme class]]){
        if (_isCurrentThemeInsync == NO) {
            shouldApply = YES;
        }else {
            if ([_currentTheme isEqual:theme]) {
                shouldApply = NO;
            }else {
                shouldApply = YES;
            }
        }
        
        if (shouldApply) {
            Theme *oldTheme = nil;
            _currentTheme = theme;
            _isCurrentThemeInsync = YES;
            
            [self notifyThemeUpdated:_currentTheme
                            oldTheme:oldTheme];
            
            isThemeApplied = YES;
        }
    }
    
    return isThemeApplied;
}

- (void)notifyThemeUpdated:(Theme *)newTheme oldTheme:(Theme *)oldTheme
{
    for (id <ThemeListener> aListener in _themeListeners) {
        [aListener themeApplied:newTheme from:oldTheme];
    }
}

//Note: This is not an asynchronous method, but has a completion handler!
- (void)applyTheme:(Theme *)theme
        completion:(ThemeCompletionBlock)block
{
    BOOL isSuccess = [self applyTheme:theme];
    block(isSuccess);
    
    //Persist the save...
    [self save];
}

- (void)applyDarkThemeWithCompletion:(ThemeCompletionBlock)block
{
    [self applyTheme:[Theme getDarkTheme] completion:block];
}

- (void)applyLightThemeWithCompletion:(ThemeCompletionBlock)block
{
    [self applyTheme:[Theme getLightTheme] completion:block];
}

#pragma mark - Theme Listening methods

- (BOOL)subscribeForThemeUpdates:(id <ThemeListener>)listener
{
    BOOL isSuccess = NO;
    
    if (listener && ![_themeListeners containsObject:listener]) {
        [_themeListeners addObject:listener];
        isSuccess = YES;
    }
    
    return isSuccess;
}

- (BOOL)unsubscribeForThemeUpdates:(id <ThemeListener>)listener
{
    BOOL isSuccess = NO;
    
    if (listener && [_themeListeners containsObject:listener]) {
        [_themeListeners removeObject:listener];
        isSuccess = YES;
    }
    
    return isSuccess;
}

- (BOOL)unsubscribeAllListeners
{
    BOOL isSuccess = NO;
    
    if ([_themeListeners count]) {
        [_themeListeners removeAllObjects];
        isSuccess = YES;
    }
    
    return isSuccess;
}

- (void)reset
{
    [self unsubscribeAllListeners];
    _isCurrentThemeInsync = NO;
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end

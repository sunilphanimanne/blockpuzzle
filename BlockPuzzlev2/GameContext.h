//
//  GameContext.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 11/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <Foundation/Foundation.h>

#import "AdUtils.h"
#import "AdContext.h"

@interface GameContext : NSObject

@property (nonatomic, weak) SKScene *currentScene;
@property (nonatomic, readonly) AdContext *adContext;
@property (nonatomic, weak) UIViewController *viewController;
@property (nonatomic, assign) BOOL didUserFinishSignificantEvent;
@property (nonatomic, assign) BOOL appDidEnterBackground;

+ (instancetype)sharedContext;

@end

//
//  SidebarController.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 10/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ThemeListener.h"

@interface SidebarController : NSObject

- (void)flipSlidebar;

@end
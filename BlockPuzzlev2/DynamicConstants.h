//
//  DynamicConstants.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 04/05/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DynamicConstants : NSObject

+ (CGFloat)puzzleBlockLength;
+ (CGFloat)masterBoarderLength;
+ (CGFloat)movingPuzzleBlockPadding;

+ (CGFloat)gameMusicVolume;
+ (CGSize)itemSizeForSidebar;
+ (CGFloat)scoreLiveValueFontSize;
+ (CGFloat)hiScoreLiveValueFontSize;
+ (CGFloat)scoreIndicatorFontSize;
+ (CGFloat)gameRestartAlertFontSize;
+ (CGFloat)leaderBoardErrorAlertFontSize;

+ (CGFloat)scoreLabelTopPadding;
+ (CGFloat)hiScoreLabelBottomPadding;
+ (CGFloat)scoreIndicatorTopPadding;

+ (CGFloat)puzzleBufferBlockYOffset;
+ (CGSize)puzzleBlockBoardSize;

+ (CGSize)sidebarMenuSize;
+ (CGPoint)sidebarMenuPosition;

@end

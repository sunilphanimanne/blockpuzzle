//
//  BlockFence.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 08/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "BlockShape.h"

@interface BlockFence : NSObject

@property (nonatomic, assign) CGRect frame;

- (instancetype)initWithFrame:(CGRect)frame;

- (BOOL)isPointInsideTheFence:(CGPoint)point forShape:(BlockShape *)shape;
- (BOOL)isPointInsideTheFence:(CGPoint)point;
- (BOOL)areAnyPointsInsideTheFence:(CGPoint)corner1
                           corner2:(CGPoint)corner2
                           corner3:(CGPoint)corner3
                           corner4:(CGPoint)corner4;

@end

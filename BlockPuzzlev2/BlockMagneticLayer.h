//
//  BlockMagneticLayer.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 08/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface BlockMagneticLayer : NSObject

@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) BOOL isMagneticEffectOn;

- (instancetype)initWithFrame:(CGRect)frame;
- (BOOL)findAttractingBlockPosition:(CGPoint *)attractingPosition
                             column:(NSInteger *)column
                             andRow:(NSInteger *)row
                           forPoint:(CGPoint)point;
@end

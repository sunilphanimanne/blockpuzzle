//
//  BufferBlockTouchLayer.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "BufferBlockTouchLayer.h"

#import "GameScene.h"

const BOOL ShouldZoom = NO;


@interface BufferBlockTouchLayer ()
{
    BOOL _isZooming;
    SKAction *_zoomSequence;
}
@end

@implementation BufferBlockTouchLayer

- (instancetype)init
{
    if (self = [super init]) {
        _isZooming = NO;
        _zoomSequence = [self zoomSequence];
        self.zPosition = BufferBlockTouchLayerZPosition;
    }
    return self;
}

- (void)preTouchesHandlerForTouchesAtPoint:(CGPoint)point
{
    DDLogDebug(@"BufferedBlockTouchLayer: preTouchesHandlers:");
    if (!CURRENT_BUFF_BLOCK && self.index >= 0) {
        CURRENT_BUFF_BLOCK = self.shape;
        CURRENT_BUFF_BLOCK_INDEX = self.index;
    }
}

- (void)touchesBeganAtPoint:(CGPoint)point
{
    DDLogDebug(@"BufferedBlockTouchLayer: touchesBeganAtPoint:");
    
    if (CURRENT_BUFF_BLOCK.state != BlockShapeStatePlaced) {
        if (ShouldZoom) {
            [self zoomTheSelectedNodeMomentarily:CURRENT_BUFF_BLOCK completion:^{
                [self makeTheblockMovingTo:point];
            }];
        }else {
            [self makeTheblockMovingTo:point];
        }
    }
}

- (void)makeTheblockMovingTo:(CGPoint)point
{
    if (CURRENT_BUFF_BLOCK.state == BlockShapeStateDisabled) {
        CURRENT_BUFF_BLOCK.state = BlockShapeStateDisabledMoving;
    }else {
        CURRENT_BUFF_BLOCK.state = BlockShapeStateMoving;
    }
    
    CURRENT_BUFF_BLOCK.position = [GameUtils point:point
                                        withOffset:[GameUtils effectiveOffsetForShape:CURRENT_BUFF_BLOCK]];
}

- (void)touchesEndedAtPoint:(CGPoint)point
{
    if (CURRENT_BUFF_BLOCK.state != BlockShapeStatePlaced) {
        if (!SCENE.blockMagneticLayer.isMagneticEffectOn) {
            DDLogDebug(@">>>>>>>>Placing it back...");
            [GameUtils sendBlockBackToBufferBoardForScene:SCENE];
        }
    }
}

//TODO: Fix this, this is causing instable state of the block...
- (void)zoomTheSelectedNodeMomentarily:(SKNode *)node
                            completion:(void (^)())block
{
    if (!_isZooming) {
        _isZooming = YES;
        
        [node runAction:_zoomSequence
             completion:^{
                 block();
                 _isZooming = NO;
             }
         ];
    }
}

- (SKAction *)zoomSequence
{
    SKAction *sequence = [SKAction sequence:
                          @[[SKAction scaleTo:1 duration:0],
                            [SKAction scaleTo:BlockShapeTransientScale
                                     duration:BlockShapeTransientScaleAnimateDuration],
                            [SKAction scaleTo:1
                                     duration:BlockShapeReturnTransientScaleAnimateDuration]]
                          ];
    return sequence;
}

@end

//
//  Player.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 03/05/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "Player.h"
#import "Player+Private.h"

#import "GameConstants.h"
#import "PlayerManager.h"
#import "PersistanceManager.h"

NSString *const PlayerPersistanceKey = @"Persistence.PlayerKey";

@implementation Player

- (instancetype)init
{
    if (self = [super init]) {
        _demoStatus = PlayerDemoStatusNotStarted;
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [self init]) {
        _highScore = [aDecoder decodeInt64ForKey:PlayerHighScoreDataKey];
        _demoStatus = [aDecoder decodeIntegerForKey:PlayerDemoStatusKey];
        _playerID = [aDecoder decodeObjectForKey:PlayerPlayerIDDataKey];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeInt64:_highScore forKey:PlayerHighScoreDataKey];
    [aCoder encodeInteger:_demoStatus forKey:PlayerDemoStatusKey];
    [aCoder encodeObject:_playerID forKey:PlayerPlayerIDDataKey];
}

+ (instancetype)loadInstance
{
    return [PersistanceManager loadInstanceForClass:self
                                             andKey:PlayerPersistanceKey];
}

- (void)save
{
    [PersistanceManager saveObject:self forKey:PlayerPersistanceKey];
}

- (void)scoreType:(ScoreKeeperScoreType)scoreType
         newValue:(int64_t)newValue oldValue:(int64_t)oldValue
          context:(NSDictionary *)contextDict
{
    if (scoreType == ScoreKeeperScoreTypeScore) {
        _currentScore += newValue;
    }else if (scoreType == ScoreKeeperScoreTypeHighScore) {
        _highScore = newValue;
        [[PlayerManager sharedManager] save];
    }
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end

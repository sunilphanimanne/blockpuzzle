//
//  Theme.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <Foundation/Foundation.h>

@interface Theme : NSObject <NSSecureCoding>

@property (nonatomic, readonly) NSString *name;

@property (nonatomic, readonly) SKColor *gameScoreColor;
@property (nonatomic, readonly) SKColor *gameHiScoreColor;
@property (nonatomic, readonly) SKColor *scorePanelColor;
@property (nonatomic, readonly) SKColor *puzzleBoardColor;
@property (nonatomic, readonly) SKColor *bufferBoardColor;
@property (nonatomic, readonly) SKColor *masterBackgroundColor;
@property (nonatomic, readonly) SKColor *puzzleBoardBlockColor;

@property (nonatomic, readonly) BOOL isDark;

+ (Theme *)getDarkTheme;
+ (Theme *)getLightTheme;

@end

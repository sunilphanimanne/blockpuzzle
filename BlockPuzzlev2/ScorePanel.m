//
//  ScorePanel.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 28/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "ScorePanel.h"

#import "Theme.h"
#import "GameScene.h"
#import "GameConstants.h"
#import "AnimatingSpriteNode.h"

NSString *const ScoreSmokeFileName  = @"ScoreSmoke";
NSString *const Star = @"star";

@interface ScorePanel ()
{
@private
    AnimatingLabelNode *_scorePanelScoreLabel;
    AnimatingLabelNode *_scorePanelHighScoreLabel;
    SKLabelNode *_scorePanelScoreIndicatorLabel;
    AnimatingSpriteNode *_scorePanelHighScoreIndicatorNode;
    SKColor *_scorePanelScoreColor;
    SKColor *_scorePanelScoreIndicatorColor;
    SKColor *_scorePanelHighScoreColor;
}
@end

@implementation ScorePanel

- (instancetype)init
{
    self = [super init];
    
    if (self) {
        self.anchorPoint = CGPointMake(0, 0);
        self.size = GetScoreboardRect().size;
        self.position = GetScoreboardPosition();
        self.zPosition = GameSceneScoreboardZPosition;
        
        [self setupLabels];
    }
    
    return self;
}

- (void)setupLabels
{
    DDLogDebug(@"SCENE before adding the scoreLabels:%@", SCENE);

    [self setupScoreLabel];
    [self setupHighScoreLabel];
    [self setupScoreIndicatorLabel];
    [self setupHiScoreIndicatorNode];
    
    DDLogDebug(@"ScorePanel Rect:%@",
               NSStringFromCGRect(GetScoreboardRect()));
    DDLogDebug(@"ScoreLabel Position:%@",
               NSStringFromCGPoint(_scorePanelScoreLabel.position));
    DDLogDebug(@"HighScoreLabel Position:%@",
               NSStringFromCGPoint(_scorePanelHighScoreLabel.position));
    DDLogDebug(@"ScoreLabelIndicator Position:%@",
               NSStringFromCGPoint(_scorePanelScoreIndicatorLabel.position));
    DDLogDebug(@"HiScoreIndicatorNode Position:%@",
               NSStringFromCGPoint(_scorePanelHighScoreIndicatorNode.position));
}


- (void)setupScoreLabel
{
    _scorePanelScoreColor = GameSceneScoreLiveValueColor;
    _scorePanelScoreLabel = CREATE_ANIMATING_SCORE_LABEL(_scorePanelScoreLabel,
                                         [@(self.currentScore) description],
                                         GameSceneScoreLiveValueFontName,
                                         GetScoreLiveValueFontSize(),
                                         _scorePanelScoreColor,
                                         SCORE_LABEL_POSITION,
                                         SKLabelVerticalAlignmentModeCenter,
                                         GameSceneScoreLabelZPosition);
    
    [self adjustScoreLabelPosition];
    [SCENE addChild:_scorePanelScoreLabel];
}

- (void)adjustScoreLabelPosition
{
    CGFloat scoreLabelTopPadding = GetScoreLabelTopPadding();
    CGPoint scorePosition = HISCORE_LABEL_POSITION;
    scorePosition.y -= (_scorePanelScoreLabel.calculateAccumulatedFrame.size.height)/2. + scoreLabelTopPadding;
    
    _scorePanelScoreLabel.position = scorePosition;
}

- (void)setupHighScoreLabel
{
    _scorePanelScoreColor = GameSceneHighScoreLiveValueColor;
    _scorePanelHighScoreLabel = CREATE_ANIMATING_SCORE_LABEL(_scorePanelHighScoreLabel,
                                             [@(self.highScore) description],
                                             GameSceneHighScoreLiveValueFontName,
                                             GetHiScoreLiveValueFontSize(),
                                             _scorePanelHighScoreColor,
                                             HISCORE_LABEL_POSITION,
                                             SKLabelVerticalAlignmentModeCenter,
                                             GameSceneScoreLabelZPosition);
    
    [self adjustHiScoreLabelPosition];
    [SCENE addChild:_scorePanelHighScoreLabel];
}

- (void)adjustHiScoreLabelPosition
{
    CGFloat hiScoreLabelBottomPadding = GetHiScoreLabelBottomPadding();
    CGPoint hiScorePosition = HISCORE_LABEL_POSITION;
    hiScorePosition.y -= (_scorePanelHighScoreLabel.calculateAccumulatedFrame.size.height)/2. - hiScoreLabelBottomPadding;
    
    _scorePanelHighScoreLabel.position = hiScorePosition;
}

- (void)setupScoreIndicatorLabel
{
    _scorePanelScoreIndicatorColor = GameSceneScoreIndicatorColor;
    _scorePanelScoreIndicatorLabel =
                        CREATE_SCORE_LABEL(_scorePanelScoreIndicatorLabel,
                                           ScoreLabelString,
                                           GameSceneScoreIndicatorFontName,
                                           GetScoreIndicatorFontSize(),
                                           _scorePanelScoreIndicatorColor,
                                           SCORE_LABEL_POSITION,
                                           SKLabelVerticalAlignmentModeTop,
                                           GameSceneScoreIndicatorZPosition);
    
    [self adjustScoreIndicatorLabelPosition];
    [SCENE addChild:_scorePanelScoreIndicatorLabel];
}

- (void)adjustScoreIndicatorLabelPosition
{
    CGFloat scoreIndicatorTopPadding = GetScoreIndicatorTopPadding();
    CGPoint scoreIndicatorPosition = SCORE_LABEL_POSITION;
    
    scoreIndicatorPosition.y -= (_scorePanelHighScoreLabel.calculateAccumulatedFrame.size.height + _scorePanelScoreIndicatorLabel.calculateAccumulatedFrame.size.height) + scoreIndicatorTopPadding;
    
    _scorePanelScoreIndicatorLabel.position = scoreIndicatorPosition;
}

- (void)setupHiScoreIndicatorNode
{
    _scorePanelHighScoreIndicatorNode = [AnimatingSpriteNode spriteNodeWithImageNamed:Star];
    _scorePanelHighScoreIndicatorNode.zPosition = GameSceneScoreLabelZPosition;
    
    [self adjustHiScoreIndicatorPosition];
    [SCENE addChild:_scorePanelHighScoreIndicatorNode];
}

- (void)adjustHiScoreIndicatorPosition
{
    CGFloat hiScoreIndicatorRightPadding = widthInPercent(2.7);
    CGFloat x = _scorePanelHighScoreLabel.calculateAccumulatedFrame.origin.x - _scorePanelHighScoreIndicatorNode.calculateAccumulatedFrame.size.width - _scorePanelHighScoreLabel.calculateAccumulatedFrame.size.width - hiScoreIndicatorRightPadding;
    CGFloat y = _scorePanelHighScoreLabel.position.y;
    
    _scorePanelHighScoreIndicatorNode.position = CGPointMake(x, y);
}

- (void)setCurrentScore:(int64_t)currentScore
{
    [self setCurrentScore:currentScore animated:YES];
}

- (void)setCurrentScore:(int64_t)currentScore animated:(BOOL)animated
{
    if (_currentScore != currentScore) {
        _currentScore = currentScore;
        
        if (animated) {
            _scorePanelScoreLabel.text = [@(self.currentScore) description];
        }else {
            [_scorePanelScoreLabel setText:[@(self.currentScore) description]
                                  animated:NO];
        }
    }
}

- (void)setHighScore:(int64_t)highScore
{
    [self setHighScore:highScore animated:YES];
}

- (void)setHighScore:(int64_t)highScore animated:(BOOL)animated
{
    if (_highScore != highScore) {
        _highScore = highScore;
        
        if (animated) {
            _scorePanelHighScoreLabel.text = [@(self.highScore) description];
        }else {
            [_scorePanelHighScoreLabel setText:[@(self.highScore) description]
                                      animated:NO];
        }
    }
}

- (void)scoreType:(ScoreKeeperScoreType)scoreType
         newValue:(int64_t)newValue oldValue:(int64_t)oldValue
          context:(NSDictionary *)context
{
    switch (scoreType) {
        case ScoreKeeperScoreTypeScore:
            self.currentScore = newValue;
            break;
        
        case ScoreKeeperScoreTypeHighScore:
            if ([self shouldShowHighScoreExceededAnimation:context]) {
                [self playHighScoreExceededAnimations];
            }
            self.highScore = newValue;
            break;
    }
}

- (BOOL)shouldShowHighScoreExceededAnimation:(NSDictionary *)context
{
    BOOL shouldShow = NO;
    if (context) {
        shouldShow = [context[OvertookHighScore] boolValue];
    }
    
    return shouldShow;
}

- (void)playHighScoreExceededAnimations
{
    //Animate the star first...
    [_scorePanelHighScoreIndicatorNode applyAnimationMomentarily];
    
    //Play the score smoke animation...
     [self playScoreSmokeAnimationAtPosition:
      _scorePanelHighScoreIndicatorNode.position];
}

- (void)playScoreSmokeAnimationAtPosition:(CGPoint)position
{
    SKEmitterNode *smoke = [self prepareSmokeForScoreAtPosition:position];
    SKAction *fadeOutAction = [SKAction fadeOutWithDuration:0.5];
    [smoke runAction:fadeOutAction];
}

- (SKEmitterNode *)prepareSmokeForScoreAtPosition:(CGPoint)position
{
    NSString *magicFilePath = [[NSBundle mainBundle] pathForResource:ScoreSmokeFileName
                                                              ofType:@"sks"];
    
    SKEmitterNode *scoreSmoke = [NSKeyedUnarchiver unarchiveObjectWithFile:magicFilePath];
    scoreSmoke.position = position;
    scoreSmoke.particleColor = SCENE.themeManager.currentTheme.gameHiScoreColor;
    scoreSmoke.particleColorSequence = nil;
    scoreSmoke.particleColorBlendFactor = 1.0;
    
    [SCENE addChild:scoreSmoke];
    
    return scoreSmoke;
}

- (void)mapColorsToUIElements
{
    _scorePanelScoreLabel.fontColor = _scorePanelScoreColor;
    _scorePanelHighScoreLabel.fontColor = _scorePanelHighScoreColor;
    _scorePanelScoreIndicatorLabel.fontColor = _scorePanelScoreIndicatorColor;
}

- (void)themeApplied:(Theme *)newTheme from:(Theme *)oldTheme
{
    _scorePanelScoreColor = newTheme.gameScoreColor;
    _scorePanelHighScoreColor = newTheme.gameHiScoreColor;
    _scorePanelScoreIndicatorColor = newTheme.gameScoreColor;
    
    [self mapColorsToUIElements];
}

@end

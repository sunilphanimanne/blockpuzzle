//
//  ScoreboardTouchLayer.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 31/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "ScoreboardTouchLayer.h"

#import "GameScene.h"
#import "ScorePanel.h"
#import "SidebarController.h"

@interface ScoreboardTouchLayer()
{
    BOOL _sidebarFlipTouchesStarted;
}

@property (nonatomic, strong) NSMutableIndexSet *optionIndices;
@end

@implementation ScoreboardTouchLayer

- (instancetype)init
{
    if (self = [super init]) {
        self.zPosition = ScoreBoardTouchLayerZPosition;
    }
    return self;
}

- (void)touchesBeganAtPoint:(CGPoint)point
{
    [self handleTouchesBeganForSidebarForPoint:point];
}

- (void)touchesEndedAtPoint:(CGPoint)point
{
    [self handleTouchesEndedForSidebarForPoint:point];
}


#pragma mark - Private methods

- (BOOL)checkIfTouchesStartedFor:(CGPoint)point inFrame:(CGRect)frame
{
    DDLogDebug(@"Point:%@ Frame:%@", NSStringFromCGPoint(point), NSStringFromCGRect(frame));
    
    return CGRectContainsPoint(frame, point);
}


- (void)handleTouchesBeganForSidebarForPoint:(CGPoint)point
{
    _sidebarFlipTouchesStarted = [self checkIfTouchesStartedFor:point
                                                      inFrame:self.sidebarMenuFrame];
    
    DDLogDebug(@"Sidebar Flip Touch? %@", BoolToString(_sidebarFlipTouchesStarted));
}

- (void)handleTouchesEndedForSidebarForPoint:(CGPoint)point
{
    if (_sidebarFlipTouchesStarted) {
        DDLogDebug(@"Flipping the SideBarMenu...");
        
        [SCENE.sidebarController flipSlidebar];
        _sidebarFlipTouchesStarted = NO;
    }
}

@end

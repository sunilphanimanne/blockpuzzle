//
//  LevelManager.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 06/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "LevelManager.h"
#import "LevelListener.h"
#import "AnalyticsLogUtils.h"

static const NSUInteger LevelManagerTotalLevels = 5;

@interface LevelManager () {
@private
    NSArray *_levelScoreLimits;
    NSMutableArray *_levelListeners;
    NSDate *_levelStartDate;
}
@end


@implementation LevelManager

- (instancetype)init
{
    if (self = [super init]) {
        _currentLevel = 0;
        _totalLevels = LevelManagerTotalLevels;
        _levelListeners = [NSMutableArray arrayWithCapacity:1];
        _levelScoreLimits = [self getLevelScoreLimits];
        _levelStartDate = [NSDate new];
    }
    
    return self;
}

- (NSArray *)getLevelScoreLimits
{
    return @[@0, @100, @200, @500, @1000];
}

- (BOOL)hasNextLevel
{
    return (_currentLevel < (LevelManagerTotalLevels - 1));
}

- (NSUInteger)nextLevel
{
    NSUInteger nextLevel = _currentLevel + 1;
    
    if (!(nextLevel <= (LevelManagerTotalLevels - 1))) {
        nextLevel = _currentLevel;
    }
    
    return nextLevel;
}

- (NSUInteger)getScoreLimitForLevel:(NSUInteger)level
{
    NSUInteger scoreLimit = NSUIntegerMax;
    
    if (level < [_levelScoreLimits count]) {
        scoreLimit = [_levelScoreLimits[level] unsignedIntegerValue];
    }
    
    return scoreLimit;
}

- (CGFloat)currentLevelPercentage
{
    CGFloat percent = ([GameUtils getCurrentScore] * 100.)/[self getScoreLimitForLevel:_currentLevel];
    
    DDLogInfo(@"Current Level Percentage:%f", percent);
    return percent;
}

- (BOOL)getAdvancedLevelForScore:(int64_t)score level:(NSUInteger *)level
{
    BOOL didAdvance = NO;
    
    for (NSUInteger i= [self nextLevel]; i < LevelManagerTotalLevels; i++) {
        if ([self isLevelPassed:i forScore:score]) {
            didAdvance = YES;
            *level = i;
        }else {
            break;
        }
    }
    
    return didAdvance;
}

- (BOOL)isLevelPassed:(NSUInteger)level forScore:(int64_t)score
{
    BOOL isLevelPassed = NO;
    
    if (score >= [self getScoreLimitForLevel:level]) {
        isLevelPassed = YES;
    }
    
    return isLevelPassed;
}

- (void)scoreType:(ScoreKeeperScoreType)scoreType
         newValue:(int64_t)newValue oldValue:(int64_t)oldValue
          context:(NSDictionary *)context
{
    //listen to ONLY score updates...
    if (scoreType == ScoreKeeperScoreTypeScore) {
        if ([self hasNextLevel]) {
            NSUInteger newLevel = _currentLevel;
            if ([self getAdvancedLevelForScore:newValue level:&newLevel]) {
                NSUInteger oldLevel = _currentLevel;
                _currentLevel = newLevel;
                
                NSTimeInterval timeTakenForPreviousLevel = [[NSDate new] timeIntervalSinceDate:_levelStartDate];
                
                _levelStartDate = [NSDate new];
                
                NSDictionary *levelContext = @{@"timeTakenForPreviousLevel": @(timeTakenForPreviousLevel)};
                
                //Post the notification...
                [self sendUpdatedValue:_currentLevel
                              oldValue:oldLevel
                               context:levelContext];
            }
        }
    }
}

- (void)sendUpdatedValue:(NSUInteger)newValue
                oldValue:(NSUInteger)oldValue
                 context:(NSDictionary *)context
{
    for (id <LevelListener> aListener in _levelListeners) {
        [aListener gameAdvancedToNewLevel:newValue
                                 oldLevel:oldValue
                                  context:context];
    }
}

- (BlockShapeType)shapeForLevel:(NSUInteger)level
{
    BlockShapeType levelShape = BlockShapeTypeSmallL;
    
    switch (level) {
        case 0:
            levelShape = BlockShapeTypeSmallL;
            break;
            
        case 1:
            levelShape = BlockShapeTypeSmallSquare;
            break;
            
        case 2:
            levelShape = BlockShapeTypeBigL;
            break;
            
        case 3:
            levelShape = BlockShapeTypeT;
            break;
            
        case 4:
            levelShape = BlockShapeTypeZ;
            break;
    }
    
    return levelShape;
}

#pragma mark - Level Listening methods

- (BOOL)subscribeForLevelUpdates:(id <LevelListener>)listener
{
    BOOL isSuccess = NO;
    
    if (![_levelListeners containsObject:listener]) {
        [_levelListeners addObject:listener];
        isSuccess = YES;
    }
    
    return isSuccess;
}

- (BOOL)unsubscribeForLevelUpdates:(id <LevelListener>)listener
{
    BOOL isSuccess = NO;
    
    if ([_levelListeners containsObject:listener]) {
        [_levelListeners removeObject:listener];
        isSuccess = YES;
    }
    
    return isSuccess;
}

- (BOOL)unsubscribeAllListeners
{
    BOOL isSuccess = NO;
    
    if ([_levelListeners count]) {
        [_levelListeners removeAllObjects];
        isSuccess = YES;
    }
    
    return isSuccess;
}

@end

//
//  MinuteBlock.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 24/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface MinuteBlock : SKSpriteNode

@property (nonatomic, assign) BOOL didFallDownToGround;

+ (instancetype)minuteBlockWithColor:(SKColor *)color;
+ (instancetype)minuteBlockWithColor:(SKColor *)color ofLength:(CGFloat)length;

@end

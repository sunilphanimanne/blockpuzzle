//
//  AnimatingSpriteNode.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 18/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "AnimatingSpriteNode.h"

NSString *const zoomActionKey = @"zoomAction";

@implementation AnimatingSpriteNode

BOOL _isAnimating;

- (instancetype)initWithImageNamed:(NSString *)name
{
    self = [super initWithImageNamed:name];
    
    if (self) {
        [self initProperties];
        [self startAnimation];
    }
    
    return self;
}

- (void)initProperties
{
    _isAnimating = NO;
    self.waitDuration = 5;
    self.zoomInScale = 1.5;
    self.zoomOutScale = 0.8;
    self.zoomInDuration = 0.1;
    self.zoomOutDuration = 0.1;
    self.zoomToNormalDuration = 0.2;
}

+ (instancetype)spriteNodeWithImageNamed:(NSString *)name
{
    AnimatingSpriteNode *node = [super spriteNodeWithImageNamed:name];
    
    [node initProperties];
    [node startAnimation];
    
    return node;
}

- (void)startAnimation
{
    if (!_isAnimating) {
        [self applyAnimationForEver:YES];
        _isAnimating = YES;
    }
}

- (SKAction *)prepareAnimation
{
    SKAction *zoomInAction = [SKAction scaleTo:self.zoomInScale
                                      duration:self.zoomInDuration];
    SKAction *zoomOutAction = [SKAction scaleTo:self.zoomOutScale
                                       duration:self.zoomOutDuration];
    SKAction *zoomNormalAction = [SKAction scaleTo:1.0
                                          duration:self.zoomToNormalDuration];
    SKAction *waitAction = [SKAction waitForDuration:self.waitDuration];
    
    SKAction *sequence = [SKAction sequence:@[zoomInAction,
                                              zoomOutAction,
                                              zoomNormalAction,
                                              waitAction]];
    
    sequence.timingMode = SKActionTimingEaseInEaseOut;
    
    return sequence;
}

- (void)applyAnimationForEver:(BOOL)forEver
{
    SKAction *finalAnimation = nil;
    SKAction *animation = [self prepareAnimation];
    
    if (forEver) {
        finalAnimation = [SKAction repeatActionForever:animation];
    }else {
        finalAnimation = animation;
    }
    
    [self runAction:finalAnimation withKey:zoomActionKey];
}


- (void)stopAnimation
{
    if (_isAnimating) {
        [self removeActionForKey:zoomActionKey];
        _isAnimating = NO;
    }
}

- (void)applyAnimationMomentarily
{
    //Stop any animations, first...
    [self stopAnimation];
    
    //Apply momentary animation
    [self applyAnimationForEver:NO];
    
    //Stop momentary animation
    [self stopAnimation];
    
    //Apply the repeated animations again...
    [self applyAnimationForEver:YES];
}

@end

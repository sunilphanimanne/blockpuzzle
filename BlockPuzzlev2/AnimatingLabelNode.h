//
//  AnimatingLabelNode.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 01/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

static const CGFloat AnimatingLabelAnimationMultiplier = 0.1;

@interface AnimatingLabelNode : SKLabelNode

- (void)setText:(NSString *)text animated:(BOOL)animated;

@end

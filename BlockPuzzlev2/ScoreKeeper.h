//
//  ScoreKeeper.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 27/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Player.h"
#import "BlockShape.h"
#import "ScoreListener.h"

@interface ScoreKeeper : NSObject

@property (nonatomic, readonly) Player *player;
@property (nonatomic, readonly) int64_t currentScore;
@property (nonatomic, readonly) int64_t highScore;

- (instancetype)initWithPlayer:(Player *)player;
- (void)incrementScoreForPlacingBlock:(BlockShape *)block;
- (void)incrementScoreForFinishingLines:(NSUInteger)numberOfLines
                               forBlock:(BlockShape *)block;
- (void)incrementScoreForClearingAllBlocks;

- (BOOL)subscribeForScoreUpdates:(id <ScoreListener>)listener;
- (BOOL)unsubscribeForScoreUpdates:(id <ScoreListener>)listener;
- (BOOL)unsubscribeAllListeners;

@end

//
//  AnalyticsLogUtils.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 04/04/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import "AnalyticsLogUtils.h"

#import "GameConstants.h"

@import Firebase;

@implementation AnalyticsLogUtils

+ (void)logOpenAppEvent
{
    [FIRAnalytics logEventWithName:kFIREventAppOpen
                        parameters:nil];
}

+ (void)logCurrentTheme:(NSString *)themeName
{
    [FIRAnalytics logEventWithName:kFIREventSelectContent
                        parameters:@{kFIRParameterContentType:themeName}];
}

+ (void)logAppStartDate:(NSDate *)startDate
{
    NSString *dateTemplate = @"y-MM-dd";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = dateTemplate;
    NSString *startDateString = [formatter stringFromDate:startDate];
    formatter = nil;
    
    [FIRAnalytics logEventWithName:AppStartDate
                        parameters:@{kFIRParameterStartDate:startDateString}];
}

+ (void)logUserLeveledUp:(NSUInteger)level
{
    [FIRAnalytics logEventWithName:kFIREventLevelUp
                        parameters:@{kFIRParameterLevel:@(level)}];
}

+ (void)logTimeTakenForLevel:(NSTimeInterval)time level:(NSUInteger)level
{
    [FIRAnalytics logEventWithName:TimeTakenForLevel
                        parameters:@{kFIRParameterValue:@(time),
                                     kFIRParameterLevel:@(level)}];
}

+ (void)logSidebarAction:(NSString *)sidebarAction
{
    [FIRAnalytics logEventWithName:SidebarAction
                        parameters:@{kFIRParameterItemID:sidebarAction}];
}

+ (void)logScore:(int64_t)score level:(int64_t)level
{
    [FIRAnalytics logEventWithName:kFIREventPostScore
                        parameters:@{kFIRParameterScore: @(score),
                                     kFIRParameterLevel: @(level)}];
}

+ (void)logNumberOfTimesConsecutivelyPlayed:(NSUInteger)noOfTimes
{
    [FIRAnalytics logEventWithName:NumberOfConsecutivePlays
                        parameters:@{kFIRParameterValue: @(noOfTimes)}];
}

+ (void)logAppiraterChoice:(NSString *)choiceString
{
    [FIRAnalytics logEventWithName:AppRatingChoice
                        parameters:@{kFIRParameterItemID: choiceString}];
}

@end

//
//  ThemeManager.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Theme;
@protocol ThemeListener;

typedef  void (^ThemeCompletionBlock) (BOOL isSuccesful);

@interface ThemeManager : NSObject <NSSecureCoding>

@property (nonatomic, readonly) Theme *currentTheme;
//init methods
+ (instancetype)loadInstance;

//Theme methods
- (void)applyTheme:(Theme *)theme
        completion:(ThemeCompletionBlock)block;
- (void)applyDarkThemeWithCompletion:(ThemeCompletionBlock)block;
- (void)applyLightThemeWithCompletion:(ThemeCompletionBlock)block;

//Theme Listening methods
- (BOOL)subscribeForThemeUpdates:(id <ThemeListener>)listener;
- (BOOL)unsubscribeForThemeUpdates:(id <ThemeListener>)listener;
- (BOOL)unsubscribeAllListeners;

//Persistance Methods
- (void)save;

//Misc Methdos
- (void)reset;

@end

//
//  AdUtils.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 26/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AdUtils.h"

#import "Utils.h"
#import "SoundManager.h"
#import "GameConstants.h"

@import Firebase;
@import GoogleMobileAds;

#define AD_AUTOLOAD_ENABLED YES

static NSUInteger InterstitialThresholdCount = 0;

@interface AdUtils () <GADInterstitialDelegate,
                       GADBannerViewDelegate,
                       GADNativeAdDelegate,
                       GADNativeAppInstallAdLoaderDelegate>

@end

@implementation AdUtils

+ (instancetype)sharedInstance {
    static AdUtils *sharedInstance = nil;
    static dispatch_once_t adUtilsToken;
    dispatch_once(&adUtilsToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

+ (BOOL)isAdsEnabled
{
    return YES;
}

+ (void)refreshBannerAdView:(UIView *)aBannerView
{
    if ([self isAdsEnabled]) {
        GADBannerView *bannerView = (GADBannerView *)aBannerView;
        
        [bannerView loadRequest:[self getGADRequest]];
    }
}

+ (UIView *)getBottomBannerAdViewWithRootViewController:(UIViewController *)viewController
{
    UIView *bottomBannerAdView = nil;
    
    if ([self isAdsEnabled]) {
        bottomBannerAdView = [self getGoogleAdMobBottomBannerViewWithRootViewController:viewController];
    }
    
    return bottomBannerAdView;
}

+ (UIView *)getTopBannerAdViewWithRootViewController:(UIViewController *)viewController
{
    UIView *topBannerAdView = nil;
    
    if ([self isAdsEnabled]) {
        BOOL shouldShowTopBanner = [self shouldShowTopBanner];
        DDLogDebug(@"shouldShowTopBanner: %@", BoolToString(shouldShowTopBanner));
        
        if (shouldShowTopBanner) {
            topBannerAdView = [self getGoogleAdMobTopBannerViewWithRootViewController:viewController];
        }
    }
    
    return topBannerAdView;
}

+ (BOOL)shouldShowTopBanner
{
    BOOL shouldShow = NO;
    
    FIRRemoteConfigValue *shouldShowConfigValue = [Utils getRemoteConfigValueWithKey:RemoteConfigAdsShowTopBannerKey];
    
    if (shouldShowConfigValue) {
        shouldShow = shouldShowConfigValue.boolValue;
    }
    
    return shouldShow;
}

+ (GADBannerView *)getGoogleAdMobBottomBannerViewWithRootViewController:(UIViewController *)viewController
{
    return [self getGoogleAdMobBottomBannerViewWithRootViewController:viewController
                                                      loadImmediately:YES];
}

+ (GADBannerView *)getGoogleAdMobTopBannerViewWithRootViewController:(UIViewController *)viewController
{
    return [self getGoogleAdMobTopBannerViewWithRootViewController:viewController
                                                      loadImmediately:YES];
}

+ (GADBannerView *)getGoogleAdMobBottomBannerViewWithRootViewController:(UIViewController *)viewController
                                            loadImmediately:(BOOL)loadNow
{
    GADBannerView *bottomBanner =  [self getGoogleAdMobBannerViewWithRootViewController:viewController
                                                                                 origin:GetBottomAdFrame().origin
                                                                        loadImmediately:loadNow];
    
    bottomBanner.adUnitID = GoogleAdMobBottomBannerAdUnitID;
    
    return bottomBanner;
}

+ (GADBannerView *)getGoogleAdMobTopBannerViewWithRootViewController:(UIViewController *)viewController
                                                        loadImmediately:(BOOL)loadNow
{
    GADBannerView *topBannerView = [self getGoogleAdMobBannerViewWithRootViewController:viewController
                                                         origin:GetTopAdFrame().origin
                                                loadImmediately:loadNow];
    topBannerView.adUnitID = GoogleAdMobTopBannerAdUnitID;
    
    return topBannerView;
}

+ (GADBannerView *)getGoogleAdMobBannerViewWithRootViewController:(UIViewController *)viewController
                                                           origin:(CGPoint)origin
                                                  loadImmediately:(BOOL)loadNow
{
    GADBannerView *adView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerPortrait
                                                           origin:origin];
    adView.rootViewController = viewController;
    
    if (!adView.delegate) {
        adView.delegate = [AdUtils sharedInstance];
    }
    
    AdContext *adContext = [AdContext sharedContext];
    adContext.bannerAdDelegate = adView.delegate;
    
    if (loadNow) {
        [adView loadRequest:[self getGADRequest]];
    }
    
    adView.autoloadEnabled = AD_AUTOLOAD_ENABLED;
    DDLogInfo(@"Ad AutoLoadEnabled:%@", BoolToString(adView.isAutoloadEnabled));
    
    return adView;
}

+ (void)presentInterstitialAd:(id)interstitialAd
             onViewController:(UIViewController *)viewController
{
    [self presentGoogleInterstitialAd:interstitialAd
                     onViewController:viewController];
}

+ (void)presentGoogleInterstitialAd:(GADInterstitial *)intersititalAd
                   onViewController:(UIViewController *)viewController
{
    BOOL isReady = [intersititalAd isReady];
    DDLogInfo(@"Interstitial Ad Ready?:%@", BoolToString(isReady));
    
    if (isReady) {
        [intersititalAd presentFromRootViewController:viewController];
    }
}

+ (id)getInterstitialAd
{
    return [self getGoogleInterstitialAd];
}

+ (GADInterstitial *)getGoogleInterstitialAd
{
    GADInterstitial *interstitialAd = [[GADInterstitial alloc] initWithAdUnitID:GoogleAdMobInterstitialAdUnitID];
    
    AdContext *adContext = [AdContext sharedContext];
    adContext.interstitalAdDelegate = [AdUtils sharedInstance];
    interstitialAd.delegate = adContext.interstitalAdDelegate;
    
    [interstitialAd loadRequest:[self getGADRequest]];
    
    return interstitialAd;
}

+ (GADRequest *)getGADRequest
{
    GADRequest *request = [GADRequest request];
    
    NSArray <NSString *> *testDevices = getTestDevices();
    
    if (testDevices) {
        request.testDevices = testDevices;
    }
    
    return request;
}

+ (void)prepareInterstitialAd
{
    id interstitialAd = nil;
    
    if ([self isAdsEnabled]) {
        interstitialAd = [AdUtils getInterstitialAd];
        [GameContext sharedContext].adContext.interstitalAd = interstitialAd;
    }
}

+ (void)incrementInterstitialThresholdCount
{
    InterstitialThresholdCount++;
}

+ (void)showIntersititalAdInternal
{
    if ([self isAdsEnabled]) {
        [[SoundManager sharedManager] pauseMusic];
        
        //show the current one...
        AdContext *adContext = [AdContext sharedContext];
        [self presentInterstitialAd:adContext.interstitalAd
                   onViewController:adContext.viewController];
    }
}

+ (void)showIntersititalAd
{
    [self showIntersititalAd:NO];
}

+ (void)showIntersititalAd:(BOOL)force
{
    if ([self shouldShowInterstital:force]) {
        [self showIntersititalAdInternal];
    }
}

+ (BOOL)shouldShowInterstital:(BOOL)force
{
    BOOL shouldShow = NO;
    if (force) {
        shouldShow = YES;
    }else {
        NSInteger interstitialFrequency = [self getInterstitialFrequency];
        if (InterstitialThresholdCount >= interstitialFrequency) {
            shouldShow = YES;
        }
    }
    
    if (shouldShow) {
        //Reset the count...
        InterstitialThresholdCount = 0;
    }
    
    return shouldShow;
}

+ (NSInteger)getInterstitialFrequency
{
    NSInteger interstitialFrequency = 0;
    
    FIRRemoteConfigValue *interstitialFrequencyValue = [Utils getRemoteConfigValueWithKey:RemoteConfigAdsInterstitialFrequencyKey];
    
    if (interstitialFrequencyValue.numberValue) {
        interstitialFrequency = interstitialFrequencyValue.numberValue.integerValue;
    }
    
    return interstitialFrequency;
}

+ (void)showIntersititalAdAfterDelay:(NSTimeInterval)delay
{
    [self showIntersititalAdAfterDelay:delay force:NO];
}

+ (void)showIntersititalAdAfterDelay:(NSTimeInterval)delay force:(BOOL)force
{
    if ([self isAdsEnabled]) {
        SKAction *waitAction = [SKAction waitForDuration:delay];
        SKAction *sequence = [SKAction sequence:@[waitAction]];
        
        [[GameContext sharedContext].currentScene runAction:sequence completion:^{
            [self showIntersititalAd:force];
        }];
    }
}

#pragma mark - Interstitial delegate methods

-(void)interstitialDidDismissScreen:(GADInterstitial *)ad
{    
    [[SoundManager sharedManager] resumeMusic];
    
    //prepare the next interstitial Ad...
    [[self class] prepareInterstitialAd];
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    DDLogInfo(@"Interstitial Ad is received now!");
    [AdContext sharedContext].interstitalAdReady = YES;
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    DDLogError(@"Error occurred showing intersitital Ad:%@", error.localizedDescription);
}

#pragma mark - Native Ad methods

+ (void)prepareNativeAd
{
    if ([self isAdsEnabled]) {
        DDLogInfo(@"Preparing native Ad...");
        AdContext *adContext = [AdContext sharedContext];
        adContext.nativeAdView = [self getNativeAdView];
        adContext.nativeAdLoader = [self getNativeAdLoader];
        [adContext.nativeAdLoader loadRequest:[[self class] getGADRequest]];
    }
}

+ (id)getNativeAdLoader
{
    return [self getGoogleNativeAdLoader];
}

+ (GADAdLoader *)getGoogleNativeAdLoader
{
    UIViewController *rootViewController = [GameContext sharedContext].viewController;
    GADAdLoader *adLoader = [[GADAdLoader alloc]
                             initWithAdUnitID:@"/6499/example/native"
                             rootViewController:rootViewController
                             adTypes:@[kGADAdLoaderAdTypeNativeAppInstall]
                             options:nil];
    
    //use the same instance...
    [AdContext sharedContext].nativeAdDelegate = [AdContext sharedContext].interstitalAdDelegate;
    adLoader.delegate = [AdContext sharedContext].nativeAdDelegate;
    
    return adLoader;
}

+ (id)getNativeAdView
{
    return [self getGoogleNativeAdView];
}

+ (GADNativeAppInstallAdView *)getGoogleNativeAdView
{
//    GADNativeAppInstallAdView *nativeAdView = [[GADNativeAppInstallAdView alloc] initWithFrame:GetScreenRect()];
    
    GADNativeAppInstallAdView *nativeAdView =
    [[[NSBundle mainBundle] loadNibNamed:@"NativeAdView"
                                   owner:nil
                                 options:nil] firstObject];
    
    nativeAdView.frame = GetScreenRect();
    return nativeAdView;
}

+ (void)mapAdData:(GADNativeAppInstallAd *)adData
  toInstallAdView:(GADNativeAppInstallAdView *)adView
{
    ((UILabel *)adView.headlineView).text = adData.headline;
    
    [((UIButton *)adView.callToActionView)
     setTitle:adData.callToAction
     forState:UIControlStateNormal];
    
    ((UIImageView *)adView.iconView).image = adData.icon.image;
    ((UILabel *)adView.bodyView).text = adData.body;
    ((UILabel *)adView.storeView).text = adData.store;
    ((UILabel *)adView.priceView).text = adData.price;
    ((UIImageView *)adView.imageView).image =
    ((GADNativeAdImage *)[adData.images firstObject]).image;
//    ((UIImageView *)adView.starRatingView).image =
//    [self imageForStars:adData.starRating];
    
    adView.callToActionView.userInteractionEnabled = NO;
}

+ (void)mapAdData:(GADNativeAppInstallAd *)adData
  toContentAdView:(GADNativeContentAd *)adView
{
    
}

+ (void)showNativeAd
{
    if ([AdContext sharedContext].isNativeAdReady) {
        UIViewController *nativeAdViewController = [[UIViewController alloc] init];
        GADNativeContentAdView *nativeAdView = [AdContext sharedContext].nativeAdView;
        nativeAdViewController.view = nativeAdView;
        
        UIViewController *viewController = [GameContext sharedContext].viewController;
        [viewController presentViewController:nativeAdViewController animated:YES
                                   completion:^{
                                       DDLogInfo(@"*** Native Ad Shown ***");
                                   }];
        
        [AdContext sharedContext].nativeAdReady = NO;
    }
}

+ (void)showNativeAdAfterDelay:(NSTimeInterval)delay
{
    SKAction *waitAction = [SKAction waitForDuration:delay];
    SKAction *sequence = [SKAction sequence:@[waitAction]];
    
    [[GameContext sharedContext].currentScene runAction:sequence completion:^{
        [self showNativeAd];
    }];
}

+ (void)showAd
{
    if ([AdContext sharedContext].isNativeAdReady) {
        DDLogInfo(@"Showing Native Ad");
        [self showNativeAd];
    }else {
        DDLogInfo(@"Showing Interstital Ad");
        [self showIntersititalAdInternal];
    }
}

+ (void)showAdAfterDelay:(NSTimeInterval)delay
{
    SKAction *waitAction = [SKAction waitForDuration:delay];
    SKAction *sequence = [SKAction sequence:@[waitAction]];
    
    [[GameContext sharedContext].currentScene runAction:sequence completion:^{
        [self showAd];
    }];
}


#pragma mark - Native Ad delegate methods

- (void)nativeAdDidDismissScreen:(GADNativeAd *)nativeAd
{
    GADAdLoader *nativeAdLoader = [AdContext sharedContext].nativeAdLoader;
    [nativeAdLoader loadRequest:[[self class] getGADRequest]];
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveNativeAppInstallAd:(GADNativeAppInstallAd *)nativeAppInstallAd
{
    GADNativeAppInstallAdView *adView = [AdContext sharedContext].nativeAdView;
    adView.nativeAppInstallAd = nativeAppInstallAd;
    
    [[self class] mapAdData:nativeAppInstallAd toInstallAdView:adView];
    
    [AdContext sharedContext].nativeAdReady = YES;
    DDLogInfo(@"Native Ad is Ready to use");
}

- (void)adLoader:(GADAdLoader *)adLoader
didReceiveNativeContentAd:(GADNativeContentAd *)nativeContentAd
{
    
}

- (void)adLoader:(GADAdLoader *)adLoader
didFailToReceiveAdWithError:(GADRequestError *)error
{
    [AdContext sharedContext].nativeAdReady = NO;
    DDLogError(@"Error:%@", error.localizedDescription);
}

#pragma mark - Banner Ad delegate methods

- (void)adViewWillLeaveApplication:(GADBannerView *)bannerView
{
    DDLogInfo(@"BannerAd clicked...");
    [AdContext sharedContext].isBannerAdClicked = YES;
}

@end

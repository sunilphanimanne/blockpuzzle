//
//  AdContext.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 27/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "AdContext.h"

@implementation AdContext

+ (instancetype)sharedContext {
    static AdContext *sharedContext = nil;
    static dispatch_once_t adContextToken;
    dispatch_once(&adContextToken, ^{
        sharedContext = [[self alloc] init];
    });
    return sharedContext;
}

@end

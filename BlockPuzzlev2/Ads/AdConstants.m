//
//  AdConstants.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 25/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "AdConstants.h"

#define PRODUCTION 1

#if PRODUCTION
// *********** Production IDs ***********//

//App ID
NSString *const GoogleAdMobAppID = @"ca-app-pub-4506963918665363~4990148336";

//Ad unit name: blocks.topbanner
NSString *const GoogleAdMobTopBannerAdUnitID = @"ca-app-pub-4506963918665363/1118446733";

//Ad unit name: blocks.bottombanner
NSString *const GoogleAdMobBottomBannerAdUnitID = @"ca-app-pub-4506963918665363/6466881532";

NSString *const GoogleAdMobNativeAdUnitID = @"ca-app-pub-3940256099942544/3986624511";

//Ad unit name: blocks.interstitial
NSString *const GoogleAdMobInterstitialAdUnitID = @"ca-app-pub-4506963918665363/8722343932";

#else

// *********** Test IDs ***********//

//App ID
NSString *const GoogleAdMobAppID = @"ca-app-pub-3940256099942544~1458002511";

//Ad unit name: blocks.topbanner
NSString *const GoogleAdMobTopBannerAdUnitID = @"ca-app-pub-3940256099942544/2934735716";

//Ad unit name: blocks.bottombanner
NSString *const GoogleAdMobBottomBannerAdUnitID = @"ca-app-pub-3940256099942544/2934735716";

NSString *const GoogleAdMobNativeAdUnitID = @"ca-app-pub-3940256099942544/3986624511";

//Ad unit name: blocks.interstitial
NSString *const GoogleAdMobInterstitialAdUnitID = @"ca-app-pub-3940256099942544/4411468910";

#endif

NSArray <NSString *> * getTestDevices() {
#if PRODUCTION
    return nil;
#else 
    return @[@"b36b968585f6838dda4b6f84df4d65b7"];
#endif
}


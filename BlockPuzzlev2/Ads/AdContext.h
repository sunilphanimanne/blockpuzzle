//
//  AdContext.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 27/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdContext : NSObject

@property (nonatomic, strong) id interstitalAd;
@property (nonatomic, strong) id interstitalAdDelegate;
@property (nonatomic, strong) id bannerAdDelegate;
@property (nonatomic, assign) BOOL isBannerAdClicked;
@property (nonatomic, strong) id nativeAdView;
@property (nonatomic, strong) id nativeAdLoader;
@property (nonatomic, strong) id nativeAdDelegate;
@property (nonatomic, assign, getter=isNativeAdReady) BOOL nativeAdReady;
@property (nonatomic, assign, getter=isInsterstitalAdReady) BOOL interstitalAdReady;
@property (nonatomic, weak) UIViewController *viewController;

+ (instancetype)sharedContext;

@end

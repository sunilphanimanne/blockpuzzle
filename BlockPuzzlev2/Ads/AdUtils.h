//
//  AdUtils.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 26/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdUtils : NSObject

+ (instancetype)sharedInstance;

+ (BOOL)isAdsEnabled;

//Banner ad related methods
+ (UIView *)getBottomBannerAdViewWithRootViewController:(UIViewController *)viewController;
+ (UIView *)getTopBannerAdViewWithRootViewController:(UIViewController *)viewController;
+ (void)refreshBannerAdView:(UIView *)bannerView;

//Interstitial Ad related methods
+ (id)getInterstitialAd;

//Prepare methods
+ (void)prepareNativeAd;
+ (void)prepareInterstitialAd;

//Shows the native if available, else will falls back to Interstitial Ad
+ (void)showAd;
+ (void)showAdAfterDelay:(NSTimeInterval)delay;

//Shows the Interstitial Ad if available
+ (void)showIntersititalAd;
+ (void)showIntersititalAd:(BOOL)force;
+ (void)showIntersititalAdAfterDelay:(NSTimeInterval)delay;
+ (void)showIntersititalAdAfterDelay:(NSTimeInterval)delay force:(BOOL)force;

//Shows the Native Ad if available
+ (void)showNativeAd;
+ (void)showNativeAdAfterDelay:(NSTimeInterval)delay;

+ (void)presentInterstitialAd:(id)interstitialAd
             onViewController:(UIViewController *)viewController;

+ (void)incrementInterstitialThresholdCount;

+ (BOOL)shouldShowTopBanner;

@end

//
//  AdConstants.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 25/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "GameConstants.h"

#define GetBottomAdFrame() CGRectMake(0, GetScreenSize().height-50, GetScreenSize().width, 50)
#define GetTopAdFrame() CGRectMake(0, 0, GetScreenSize().width, 50)

static const CGFloat InterstitialAdDisplayDelay = 1.5;

FOUNDATION_EXPORT NSString *const GoogleAdMobAppID;
FOUNDATION_EXPORT NSString *const GoogleAdMobTopBannerAdUnitID;
FOUNDATION_EXPORT NSString *const GoogleAdMobBottomBannerAdUnitID;
FOUNDATION_EXPORT NSString *const GoogleAdMobNativeAdUnitID;
FOUNDATION_EXPORT NSString *const GoogleAdMobInterstitialAdUnitID;

NSArray <NSString *> * getTestDevices();

//
//  TouchLayer.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TouchLayerProtocol.h"

#import "GameConstants.h"

@interface TouchLayer : NSObject <TouchLayerProtocol>

@end

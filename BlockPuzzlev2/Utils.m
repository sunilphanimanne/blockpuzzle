//
//  Utils.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 31/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "Utils.h"

#import "AdConstants.h"
#import "GameConstants.h"
#import "AnalyticsLogUtils.h"

@import Firebase;
@import GoogleMobileAds;

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

#define UTILS_DEBUG_MODE 0

@interface Utils () <AppiraterDelegate>

@end

@implementation Utils

+ (void)configureLogging
{
    [DDLog addLogger:[DDTTYLogger sharedInstance]]; // TTY = Xcode console
    [DDLog addLogger:[DDASLLogger sharedInstance]]; // ASL = Apple System Logs
}

+ (void)configureGoogleAdmob
{
    [GADMobileAds configureWithApplicationID:GoogleAdMobAppID];
}

+ (void)configureFirebase
{
    [FIRApp configure];
}

+ (void)configureRemoteConfig
{
    [FIRRemoteConfig remoteConfig];
    
#if UTILS_DEBUG_MODE
    FIRRemoteConfigSettings *settings = [[FIRRemoteConfigSettings alloc] initWithDeveloperModeEnabled:YES];
    [[FIRRemoteConfig remoteConfig] setConfigSettings:settings];
#endif
    NSMutableDictionary *defaults = [NSMutableDictionary dictionaryWithCapacity:5];
    
    [self populateAdsNameSpaceDefaults:defaults];
    [self populateLocalNotificationsDefaults:defaults];
    
    [[FIRRemoteConfig remoteConfig] setDefaults:defaults];
}

+ (void)populateLocalNotificationsDefaults:(NSMutableDictionary *)defaults
{
    NSDictionary *localNotificationDefaults = @{
                                                RemoteConfigLocalNotificationsScheduleStringKey: RemoteConfigLocalNotificationsScheduleStringDefaultValue,
                                                RemoteConfigLocalNotificationsAlertTitleKey: RemoteConfigLocalNotificationsAlertTitleDefaultValue,
                                                RemoteConfigLocalNotificationsAlertBodyKey:
                                                    RemoteConfigLocalNotificationsAlertBodyDefaultValue,
                                                RemoteConfigLocalNotificationsAlertActionKey:
                                                    RemoteConfigLocalNotificationsAlertActionDefaultValue
                                                };
    
    [defaults addEntriesFromDictionary:localNotificationDefaults];
}

+ (void)populateAdsNameSpaceDefaults:(NSMutableDictionary *)defaults
{
    NSDictionary *namespaceDefaults = @{RemoteConfigAdsShowTopBannerKey: @(RemoteConfigAdsShowTopBannerDefaultValue),
                               RemoteConfigAdsInterstitialFrequencyKey: @(RemoteConfigAdsInterstitialFrequencyDefaultValue)};
    
    [defaults addEntriesFromDictionary:namespaceDefaults];
}

+ (void)fetchRemoteConfigValuesWithExpirationDuration:(NSTimeInterval)expiration
                                           completion:(FIRRemoteConfigFetchCompletion)completion
{
    FIRRemoteConfig *remoteconfig = [FIRRemoteConfig remoteConfig];
    
    [remoteconfig fetchWithExpirationDuration:expiration completionHandler:^(FIRRemoteConfigFetchStatus status, NSError * _Nullable error) {
        if (status != FIRRemoteConfigFetchStatusSuccess) {
            DDLogError(@"Error:fetchRemoteConfigValuesFromServer:%@", error);
        }
        
        BOOL isRemoteConfigApplied = [[FIRRemoteConfig remoteConfig] activateFetched];
        
        DDLogDebug(@"fetchRemoteConfigValuesFromServer: %ld and applied:%@",
                   (long)status, BoolToString(isRemoteConfigApplied));

        if (completion) {
            completion(status, error);
        }
    }];
}

+ (FIRRemoteConfigValue *)getRemoteConfigValueWithKey:(NSString *)key
{
    FIRRemoteConfigValue *value = nil;
    
    if (key) {
        FIRRemoteConfig *remoteConfig = [FIRRemoteConfig remoteConfig];
        FIRRemoteConfigValue *tempValue = remoteConfig[key];
        
        if (tempValue.source != FIRRemoteConfigSourceStatic) {
            value = tempValue;
        }
    }
    
    return value;
}

+ (void)configureAppiraterAndShow
{
    [Appirater setAppId:AppleApplicationID];
    
    [Appirater setDaysUntilPrompt:AppiraterDaysUntilPrompt];
    
    [Appirater setUsesUntilPrompt:AppiraterUsesUntilPrompt];
    
    [Appirater setSignificantEventsUntilPrompt:AppiraterSignificantEventsUntilPrompt];
    
    [Appirater setTimeBeforeReminding:AppiraterTimeBeforeRemindingInDays];
    
    [Appirater setDebug:NO];
    
    [Appirater appLaunched:YES];
    
    [Appirater setDelegate:[Utils new]];
}

#pragma mark - Remote Notifications

+ (void)configureRemoteNotifications:(id)delegate
{
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        [self registerForRemoteNotificationsForLessThaniOS10:delegate];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        [self registerForRemoteNotificationsForiOS10AndAbove:delegate];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

+ (UIUserNotificationType)subscribeForNotificationTypes
{
    return (UIUserNotificationTypeSound |
            UIUserNotificationTypeAlert |
            UIUserNotificationTypeBadge);
}

+ (UNAuthorizationOptions)subscribeForAuthorizationOptions
{
    return (UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge);
}

+ (void)registerForRemoteNotificationsForLessThaniOS10:(id)delegate
{
    UIUserNotificationType allNotificationTypes = [self subscribeForNotificationTypes];
    UIUserNotificationSettings *settings =
    [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}

+ (void)registerForRemoteNotificationsForiOS10AndAbove:(id)delegate
{
    // For iOS 10 display notification (sent via APNS)
    [UNUserNotificationCenter currentNotificationCenter].delegate = delegate;
    UNAuthorizationOptions authOptions = [self subscribeForAuthorizationOptions];
    
    [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
    }];
    
    // For iOS 10 data message (sent via FCM)
    [FIRMessaging messaging].remoteMessageDelegate = delegate;
}

#pragma mark - Local Notifications

+ (void)configureLocalNotifications
{
    [Utils fetchRemoteConfigValuesWithExpirationDuration:0 completion:^(FIRRemoteConfigFetchStatus status, NSError * _Nullable error) {
        NSArray <NSDictionary *> *optionsDictionaries = [self localNotificationOptionsFromRemoteConfig];
        [self configureLocalNotifications:optionsDictionaries];
    }];
}

+ (void)configureLocalNotifications:(NSArray <NSDictionary *> *)optionsDictionaries
{
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        [self cancelAllLocalNotificationsOldWay];
        
        for (NSDictionary *options in optionsDictionaries) {
            [self scheduleLocalNotificationsOldWay:options];
        }
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        [self cancelAllLocalNotificationsNewWay];
        
        for (NSDictionary *options in optionsDictionaries) {
            [self configureNotificationCategoriesOnce:options];
            [self scheduleLocalNotificationsNewWay:options];
        }
#endif
    }
}

+ (NSArray <NSDictionary *> *)localNotificationOptionsFromRemoteConfig
{
    NSString *scheduleString = [Utils getRemoteConfigValueWithKey:RemoteConfigLocalNotificationsScheduleStringKey].stringValue;
    NSString *alertTitle = [Utils getRemoteConfigValueWithKey:RemoteConfigLocalNotificationsAlertTitleKey].stringValue;
    NSString *alertBody = [Utils getRemoteConfigValueWithKey:RemoteConfigLocalNotificationsAlertBodyKey].stringValue;
    NSString *alertAction = [Utils getRemoteConfigValueWithKey:RemoteConfigLocalNotificationsAlertActionKey].stringValue;
    
    NSArray <NSDate *> *scheduleDates = [self scheduleDatesFromScheduleString:scheduleString];
    NSMutableArray *optionDictionaries = [NSMutableArray arrayWithCapacity:3];
    
    for (NSDate *scheduleDate in scheduleDates) {
        NSDictionary *optionDictionary = @{
                                           LocalNotificationsFireDate: scheduleDate,
                                           LocalNotificationsAlertTitle: alertTitle,
                                           LocalNotificationsAlertBody: alertBody,
                                           LocalNotificationsAlertAction: alertAction
                                          };
        
        [optionDictionaries addObject:optionDictionary];
    }
    
    return [optionDictionaries copy];
}

//Eg: Schedule String => 4-1, 4-2, 4-3
+ (NSArray <NSDate *> *)scheduleDatesFromScheduleString:(NSString *)scheduleString
{
    NSArray <NSArray <NSString *> *> *values = [self scheduleValuesFromScheduleString:scheduleString];
    
    return [self scheduleDatesFromScheduleValues:values];
}

+ (NSArray <NSArray <NSString *> *> *)scheduleValuesFromScheduleString:(NSString *)scheduleString
{
    NSString *const ScheduleValueDelimiter = @",";
    NSString *const ScheduleValuePairDelimiter = @"-";
    
    __block NSMutableArray *scheduleValues = [NSMutableArray arrayWithCapacity:3];
    
    //@[4-1, 4-2, 4-3]
    NSArray <NSString *> *strings = [scheduleString componentsSeparatedByString:ScheduleValueDelimiter];
    
    [strings enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSCharacterSet *whitespaceAndNewLineCharSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //>4-1< >4-2< >4-3<
        NSString *trimmedString = [obj stringByTrimmingCharactersInSet:whitespaceAndNewLineCharSet];
        
        //[4, 1]; [4, 2]; [4, 3];
        NSArray <NSString *> *scheduleComponents = [trimmedString componentsSeparatedByString:ScheduleValuePairDelimiter];
        
        [scheduleValues addObject:scheduleComponents];
    }];
    
    return [scheduleValues copy];
}

+ (NSArray <NSDate *> *)scheduleDatesFromScheduleValues:(NSArray <NSArray <NSString *> *> *)values
{
    NSMutableArray <NSDate *> *dates = [NSMutableArray arrayWithCapacity:12];
    NSDate *startDate = [NSDate new];
    
    for (NSArray <NSString *> *valuePair in values) {
        NSInteger numberOfDays = -1;
        NSInteger daysToSkip = -1;
        
        if ([valuePair count] >= 2) {
            id value0 = valuePair[0];
            id value1 = valuePair[1];
            
            @try {
                numberOfDays = [value0 integerValue];
                daysToSkip = [value1 integerValue];
            } @catch (NSException *exception) {
                DDLogError(@"Exception while parsing an Invalid number %@!", exception.description);
            }
            
            NSArray <NSDate *> *datesComputed = [self datesForValues:numberOfDays
                                                          daysToSkip:daysToSkip
                                                       fromStartDate:startDate];
            [dates addObjectsFromArray:datesComputed];
            
            startDate = datesComputed[datesComputed.count - 1];
        }
    }
    
    DDLogError(@"Local Notification Dates:%@", dates);
    
    return dates;
}

+ (NSArray <NSDate *> *)datesForValues:(NSInteger)numberOfDays
                            daysToSkip:(NSInteger)daysToSkip
                         fromStartDate:(NSDate *)startDate
{
#if UTILS_DEBUG_MODE
    const NSTimeInterval oneDay = 10; //10 secs, fake :P
#else 
    const NSTimeInterval oneDay = 24 * 60 * 60; //one day, really :D!
#endif
    
    NSMutableArray <NSDate *> *dates = nil;
    NSDate *currentDate = startDate;
    
    if (numberOfDays > 0 && daysToSkip >= 0) {
        dates = [NSMutableArray arrayWithCapacity:3];
        
        for (int i = 1; i <= numberOfDays; i++) {
            NSTimeInterval timeInterval = oneDay * daysToSkip;
            
            NSDate *nextDate = [[NSDate alloc] initWithTimeInterval:timeInterval
                                                          sinceDate:currentDate];
            [dates addObject:nextDate];
            currentDate = nextDate;
        }
    }
    
    return [dates copy];
}

+ (void)scheduleLocalNotificationsOldWay:(NSDictionary *)options
{
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    NSDate *fireDate = options[@"fireDate"];
    NSString *alertTitle = options[@"alertTitle"];
    NSString *alertBody = options[@"alertBody"];
    NSString *alertAction = options[@"alertAction"];
    
    localNotification.fireDate = fireDate;
    localNotification.alertTitle = alertTitle;
    localNotification.alertBody = alertBody;
    localNotification.alertAction = alertAction;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = 0;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

+ (void)cancelAllLocalNotificationsOldWay
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

+ (void)cancelAllLocalNotificationsNewWay
{
    [[UNUserNotificationCenter currentNotificationCenter] removeAllPendingNotificationRequests];
}

+ (void)scheduleLocalNotificationsNewWay:(NSDictionary *)options
{
    static int requestNumber = 0;
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    
    NSDate *fireDate = options[@"fireDate"];
    NSString *alertTitle = options[@"alertTitle"];
    NSString *alertBody = options[@"alertBody"];
    NSString *alertActionIdentifier = options[@"alertAction"];
    
    content.title = alertTitle;
    content.body = alertBody;
    content.categoryIdentifier = alertActionIdentifier;
    content.sound = [UNNotificationSound defaultSound];
    content.badge = @0;
    
    NSDateComponents *dateComponents = [self getDateComponentsForDate:fireDate];
    
    UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:dateComponents
                                                                                                      repeats:NO];
    NSString *identifier = [NSString stringWithFormat:@"localNotification-%d", requestNumber];
    
    DDLogError(@"Identifier:%@", identifier);
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier
                                                                          content:content
                                                                          trigger:trigger];
    requestNumber++;
    
    [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request
       withCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            DDLogError(@"Something went wrong: %@",error);
        }
    }];
}

+ (void)configureNotificationCategories:(NSDictionary *)options
{
    NSString *alertActionString = options[@"alertAction"];
    
    UNNotificationAction *action = [UNNotificationAction actionWithIdentifier:alertActionString
                                                                        title:alertActionString
                                                                      options:UNNotificationActionOptionForeground];
    
    UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:alertActionString
                                                                              actions:@[action]
                                                                    intentIdentifiers:@[]
                                                                              options:UNNotificationCategoryOptionNone];
    
    NSSet *categories = [NSSet setWithObjects:category, nil];
    
    [[UNUserNotificationCenter currentNotificationCenter] setNotificationCategories:categories];
}

+ (void)configureNotificationCategoriesOnce:(NSDictionary *)options
{
    static dispatch_once_t notificationCategoryToken;
    dispatch_once(&notificationCategoryToken, ^{
        [self configureNotificationCategories:options];
    });
}

+ (NSDateComponents *)getDateComponentsForDate:(NSDate *)date
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSCalendarUnit interestedUnits = NSCalendarUnitDay  | NSCalendarUnitMonth  | NSCalendarUnitYear  |
                                     NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    NSDateComponents *fireDataComponents = [calendar components:interestedUnits
                                                       fromDate:date];
    return fireDataComponents;
}

#pragma mark - AppiraterDelegate methods

- (void)appiraterDidOptToRate:(Appirater *)appirater
{
    [AnalyticsLogUtils logAppiraterChoice:AppiraterChoiceRate];
}

- (void)appiraterDidDeclineToRate:(Appirater *)appirater
{
    [AnalyticsLogUtils logAppiraterChoice:AppiraterChoiceDecline];
}

- (void)appiraterDidOptToRemindLater:(Appirater *)appirater
{
    [AnalyticsLogUtils logAppiraterChoice:AppiraterChoiceLater];
}

#pragma mark - Remote Notification method

/*
 Reference:
 ~~~~~~~~~
 {
     aps =     {
         alert = "Hello, How are you enjoying the blocks game?";
         sound = default;
     };

     "gcm.message_id" = "0:1491918968082950%0d1af6e50d1af6e5";
     "gcm.n.e" = 1;
     "gcm.notification.sound2" = default;
     "google.c.a.c_id" = 3202019519674991433;
     "google.c.a.c_l" = "Greetings!";
     "google.c.a.e" = 1;
     "google.c.a.ts" = 1491918967;
     "google.c.a.udt" = 0;
 }
 
 */

+ (void)showAlertForRemoteNotificationUserInfo:(NSDictionary *)userInfo
                              inViewController:(UIViewController *)viewController
{
    if (viewController) {
        NSString *title = [self getTitleFromUserInfo:userInfo];
        NSString *message = [self getMessageFromUserInfo:userInfo];
        
        UIAlertController *alert = [self getAlertWithTitle:title message:message];

        [viewController presentViewController:alert
                                     animated:YES
                                   completion:nil];
    }
}

+ (UIAlertController *)getAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:RemoteNotificationAlertDefaultActionTitle
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    
    return alert;
}

+ (NSString *)getTitleFromUserInfo:(NSDictionary *)userInfo
{
    NSString *title = userInfo[RemoteNotificationTitleKey];
    
    if (!title) {
        title = RemoteNotificationDefaultTitle;
    }
    
    return title;
}

+ (NSString *)getMessageFromUserInfo:(NSDictionary *)userInfo
{
    NSString *message = userInfo[RemoteNotificationAPSKey][RemoteNotificationMessageKey];
    return message;
}

@end

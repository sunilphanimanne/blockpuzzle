//
//  PlayerManager.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 03/05/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Player.h"

@interface PlayerManager : NSObject <NSSecureCoding>

@property (nonatomic, readonly) Player *currentPlayer;

+ (instancetype)sharedManager;

+ (instancetype)loadInstance;
- (void)save;
- (void)loadCurrentPlayer;

@end

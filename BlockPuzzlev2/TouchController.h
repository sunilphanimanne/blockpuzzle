//
//  TouchController.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <Foundation/Foundation.h>

#import "TouchLayerProtocol.h"

@interface TouchController : NSObject

#define SYNTHESIZE_TOUCH_METHODS_FOR_TOUCH_CONTROLLER(touchController) \
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event { \
    [touchController touchesBegan:touches withEvent:event];\
} \
\
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event\
{\
    [touchController touchesMoved:touches withEvent:event];\
}\
\
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event\
{\
    [touchController touchesEnded:touches withEvent:event];\
}\
\
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event\
{\
    [touchController touchesCancelled:touches withEvent:event];\
}\


- (instancetype)initWithNode:(SKNode *)node;

@property (nonatomic, assign) BOOL shouldBypassTouches;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;

/*!
 @brief Subscribe the given touchLayer for touches in the specified frame.
 @param TouchLayer to be subscribed.
 @note The frame property determines the area in which the touches will be tracked.
 
 @return YES if succesful. NO if not.
*/
- (BOOL)subscribeTouchLayer:(id <TouchLayerProtocol>)touchLayer;

/*!
 @brief Unsubscribe the given touchLayer for touches.
 @param TouchLayer to be subscribed.
 
 @return YES if succesful. NO if not.
 */
- (BOOL)unsubscribeTouchLayer:(id <TouchLayerProtocol>)touchLayer;

/*!
 @brief Unsubscribe ALL the subscribed touchlayers.
 
 @return YES if succesful. NO if not.
 */
- (BOOL)unsubscribeAllTouchLayers;

@end

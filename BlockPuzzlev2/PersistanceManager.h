//
//  PersistanceManager.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 08/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersistanceManager : NSObject

+ (id)retrieveObjectForKey:(NSString *)key;
+ (void)saveObject:(id)object forKey:(NSString *)key;
+ (id)loadInstanceForClass:(Class)class andKey:(NSString *)key;

@end

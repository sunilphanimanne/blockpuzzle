//
//  ScorePanel.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 28/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

/*!
 Enumeration indicating the different DisplayTypes.
 */

#import <SpriteKit/SpriteKit.h>

#import "ScoreKeeper.h"
#import "ThemeListener.h"

@interface ScorePanel : SKSpriteNode <ScoreListener, ThemeListener>

@property (nonatomic, assign) int64_t highScore;
@property (nonatomic, assign) int64_t currentScore;

- (void)setCurrentScore:(int64_t)currentScore animated:(BOOL)animated;
- (void)setHighScore:(int64_t)highScore animated:(BOOL)animated;

@end

//
//  ScheduleValueParsingTests.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 04/04/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Utils.h"

@interface Utils (Private)

+ (NSArray <NSArray <NSString *> *> *)scheduleValuesFromScheduleString:(NSString *)scheduleString;

@end

@interface ScheduleValueParsingTests : XCTestCase

@end

@implementation ScheduleValueParsingTests

- (void)testScheduleStringParsingWithSpacesInBetweenCommas {
    
    NSArray <NSArray <NSString *> *> *expectedArray = @[@[@"4", @"1"], @[@"4", @"2"], @[@"4", @"3"]];
    NSArray <NSArray <NSString *> *> *resultArray = [Utils scheduleValuesFromScheduleString:@"4-1, 4-2, 4-3"];

    XCTAssertEqualObjects(resultArray, expectedArray, @"Error in parsing the Schedule String!");
}

- (void)testScheduleStringParsingWithNoSpacesInBetweenCommas {
    
    NSArray <NSArray <NSString *> *> *expectedArray = @[@[@"4", @"1"], @[@"4", @"2"], @[@"4", @"3"]];
    NSArray <NSArray <NSString *> *> *resultArray = [Utils scheduleValuesFromScheduleString:@"4-1,4-2,4-3"];
    
    XCTAssertEqualObjects(resultArray, expectedArray, @"Error in parsing the Schedule String!");
}

@end

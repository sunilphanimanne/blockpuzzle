//
//  SubmatrixTests.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 18/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "YCMatrix.h"

@interface SubmatrixTests : XCTestCase
{
    Matrix *masterMatrix;

}
@end

@implementation SubmatrixTests

- (void)setUp {
    [super setUp];
    masterMatrix = [Matrix matrixFromNSArray:@[@1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               @1,@0,@1,@0,@1,@0,@1,@0,@1,@0,
                                               ] rows:10 columns:10];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testSubmatrixCase1 {
    Matrix *submatrix = [masterMatrix submatrixWithRowRange:NSMakeRange(0, 10)
                                             andColumnRange:NSMakeRange(0, 10)];
    XCTAssertTrue([submatrix isEqualToMatrix:masterMatrix tolerance:0]);
    
    submatrix = [masterMatrix submatrixWithRowRange:NSMakeRange(1, 2) andColumnRange:NSMakeRange(1, 2)];
    Matrix *knownResultMatrix = [Matrix matrixFromNSArray:@[@0,@1,
                                                            @0,@1] rows:2 columns:2];
    
    XCTAssertTrue([submatrix isEqualToMatrix:knownResultMatrix tolerance:0]);
}

- (void)testSubmatrixCase2 {
    Matrix *submatrix = [masterMatrix submatrixWithRowRange:NSMakeRange(0, 12)
                                             andColumnRange:NSMakeRange(0, 12)];
    XCTAssertFalse([submatrix isEqualToMatrix:masterMatrix tolerance:0]);
    XCTAssertNil(submatrix);
    
    submatrix = [masterMatrix submatrixWithRowRange:NSMakeRange(12, 1) andColumnRange:NSMakeRange(12, 1)];
    Matrix *knownResultMatrix = [Matrix matrixFromNSArray:@[@0,@1,
                                                            @0,@1] rows:2 columns:2];
    
    XCTAssertFalse([submatrix isEqualToMatrix:knownResultMatrix tolerance:0]);
    XCTAssertNil(submatrix);
}

- (void)testSetMatrix {

    Matrix *submatrix = [Matrix matrixFromNSArray:@[@0,@1,
                                                    @0,@1] rows:2 columns:2];
    
    
    Matrix *matrix1 = [Matrix matrixFromNSArray:@[@0,@0,
                                                       @0,@0] rows:2 columns:2];
    
    [matrix1 setMatrix:submatrix atColumn:0 andRow:0];
    
    
    XCTAssertTrue([matrix1 isEqualToMatrix:submatrix tolerance:0]);
}

- (void)testSetMatrix1 {
    
    Matrix *matrix1 = [Matrix matrixOfRows:10 columns:10 value:0];
    
    [matrix1 setValue:1 row:9 column:0];
    
    
    Matrix *submatrix = [Matrix matrixFromNSArray:@[@0, @1, @0,
                                                    @1, @1, @1] rows:2 columns:3];
    
    [matrix1 setMatrix:submatrix atColumn:0 andRow:0];
    
    DDLogDebug(@"Matrix1: %@", matrix1);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end

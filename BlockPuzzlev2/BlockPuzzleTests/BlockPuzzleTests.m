//
//  BlockPuzzleTests.m
//  BlockPuzzleTests
//
//  Created by Sunil Phani Manne on 12/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "YCMatrix.h"

@interface BlockPuzzleTests : XCTestCase
{
    //Boards
    Matrix *emptyTwoByTwoBoard;
    Matrix *fullTwoByTwoBoard;
    
    //Shapes
    Matrix *shapeITwoByTwo;
    Matrix *shapeDotTwoOneByTwo;
    Matrix *shapeLTwoByTwo;
    Matrix *shapeSquareTwoByTwo;
}
@end

@implementation BlockPuzzleTests

- (void)setUp {
    [super setUp];
    
    //Boards...
    emptyTwoByTwoBoard = [Matrix matrixOfRows:2 columns:2];
    fullTwoByTwoBoard = [Matrix matrixOfRows:2 columns:2 value:1];
    
    //Shapes...
    shapeITwoByTwo = [Matrix matrixFromNSArray:@[@1,@0,@0,@1] rows:2 columns:2];
    shapeDotTwoOneByTwo = [Matrix matrixFromNSArray:@[@1,@0,@0,@0] rows:2 columns:2];
    shapeLTwoByTwo = [Matrix matrixFromNSArray:@[@1,@0,@0,@1] rows:2 columns:2];
    shapeSquareTwoByTwo = [Matrix matrixOfRows:2 columns:2 value:1];
}

- (void)tearDown {
    [super tearDown];
}


- (void)testSameRankCase1 {
    XCTAssertTrue([emptyTwoByTwoBoard canAccomodate:shapeDotTwoOneByTwo],
                  @"Empty board unable to Accomodate Dot shape");
    XCTAssertTrue([emptyTwoByTwoBoard canAccomodate:shapeITwoByTwo],
                  @"Empty board unable to Accomodate I shape");
    XCTAssertTrue([emptyTwoByTwoBoard canAccomodate:shapeLTwoByTwo],
                  @"Empty board unable to Accomodate L shape");
    XCTAssertTrue([emptyTwoByTwoBoard canAccomodate:shapeSquareTwoByTwo],
                  @"Empty board unable to Accomodate Square shape");
}

- (void)testSameRankCase2 {
    XCTAssertFalse([fullTwoByTwoBoard canAccomodate:shapeDotTwoOneByTwo],
                  @"Full board should not Accomodate Dot shape");
    XCTAssertFalse([fullTwoByTwoBoard canAccomodate:shapeITwoByTwo],
                  @"Full board should not Accomodate I shape");
    XCTAssertFalse([fullTwoByTwoBoard canAccomodate:shapeLTwoByTwo],
                  @"Full board should not Accomodate L shape");
    XCTAssertFalse([fullTwoByTwoBoard canAccomodate:shapeSquareTwoByTwo],
                  @"Full board should not Accomodate Square shape");
}

- (void)testSameRankCase3
{
    Matrix *puzzleBoard1 = [Matrix matrixFromNSArray:@[@0,@0,@1,@0] rows:2 columns:2];
    Matrix *shape1 = [Matrix matrixFromNSArray:@[@1,@0,@0,@1] rows:2 columns:2];
    
    XCTAssertTrue([puzzleBoard1 canAccomodate:shape1], @"Puzzleboard1 SHOULD accomodate Shape1");
    
    Matrix *puzzleBoard2 = [Matrix matrixFromNSArray:@[@1,@0,@1,@0] rows:2 columns:2];
    Matrix *shape2 = [Matrix matrixFromNSArray:@[@1,@0,@0,@1] rows:2 columns:2];
    
    XCTAssertFalse([puzzleBoard2 canAccomodate:shape2], @"Puzzleboard2 SHOULD NOT accomodate Shape2");
}

@end

//
//  DatesForValues.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 05/04/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Utils.h"

@interface Utils (unitTests)

+ (NSArray <NSDate *> *)datesForValues:(NSInteger)numberOfDays
                            daysToSkip:(NSInteger)daysToSkip
                         fromStartDate:(NSDate *)startDate;

@end

@interface DatesForValues : XCTestCase

@end

@implementation DatesForValues

- (void)testDatesForValues {
    
    //Postive tests
    [self positiveAssertions];
    
    //Negative tests
    [self negativeAssertions];
}

- (void)positiveAssertions
{
    //4 - 0
    [self assertDatesForValues:4 daysToSkip:0];
    
    //4 - 1
    [self assertDatesForValues:4 daysToSkip:1];
    
    //4 - 2
    [self assertDatesForValues:4 daysToSkip:2];
}

- (void)negativeAssertions
{
    //4 - -1
    [self assertDatesForValues:4 daysToSkip:-1];
    
    //-1 - 1
    [self assertDatesForValues:-1 daysToSkip:1];
    
    //-1 - -1
    [self assertDatesForValues:-1 daysToSkip:-1];
    
    //0 - 0
    [self assertDatesForValues:0 daysToSkip:0];
    
    //0 - -1
    [self assertDatesForValues:0 daysToSkip:-1];
    
    //-1 - 0
    [self assertDatesForValues:-1 daysToSkip:0];
    
    //-1 - -1
    [self assertDatesForValues:-1 daysToSkip:-1];
}

- (void)assertDatesForValues:(NSInteger)numberOfDays daysToSkip:(NSInteger)daysToSkip
{
    NSDate *startDate = [NSDate new];
    
    NSArray <NSDate *> * dates = [Utils datesForValues:numberOfDays
                                            daysToSkip:daysToSkip
                                         fromStartDate:startDate];
    [self assertDates:dates
         numberOfDays:numberOfDays
           daysToSkip:daysToSkip
            startDate:startDate];
}

- (void)assertDates:(NSArray <NSDate *> *)dates
       numberOfDays:(NSInteger)numberOfDays
         daysToSkip:(NSInteger)daysToSkip
          startDate:(NSDate *)startDate
{
    NSTimeInterval oneDay = 24 * 60 * 60;
    
    if (numberOfDays >= 0 && daysToSkip >= 0) {
        //Assert number of days
        XCTAssertEqual([dates count], numberOfDays, @"numberOfDays assertion failed!");
        
        NSDate *currentDate = startDate;
        
        //Assert the difference between each day to match daysToSkip
        for (NSDate *date in dates) {
            NSTimeInterval difference = [date timeIntervalSinceDate:currentDate];
            XCTAssertEqual(difference, daysToSkip * oneDay, @"Days to skip assertion failed!");
            currentDate = date;
        }
    }else {
        XCTAssertEqual([dates count], 0, @"numberOfDays should be 0!");
    }
}

@end

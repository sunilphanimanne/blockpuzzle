//
//  PhysicsDelegate.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 19/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "BlockShape.h"

typedef void (^ContactsEndedBlock)(void);

@interface PhysicsDelegate : NSObject <SKPhysicsContactDelegate>

- (void)setupPhysicsForScene:(SKScene *)scene;

- (void)setupPhysicsForScene:(SKScene *)scene
            allContactsEnded:(ContactsEndedBlock)block;

- (void)setupPhysicsForBlock:(BlockShape *)block;

@end

//
//  AnimatingSpriteNode.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 18/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface AnimatingSpriteNode : SKSpriteNode

@property (nonatomic, assign) CGFloat zoomInScale;
@property (nonatomic, assign) CGFloat zoomOutScale;
@property (nonatomic, assign) NSTimeInterval zoomInDuration;
@property (nonatomic, assign) NSTimeInterval zoomOutDuration;
@property (nonatomic, assign) NSTimeInterval zoomToNormalDuration;
@property (nonatomic, assign) NSTimeInterval waitDuration;

- (void)startAnimation;
- (void)stopAnimation;
- (void)applyAnimationMomentarily;

@end

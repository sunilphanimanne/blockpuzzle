//
//  GameAlert.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 14/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameAlert : SKShapeNode

typedef void(^AlertActionBlock)(GameAlert *sender, BOOL isCancelled);

@property (nonatomic, copy) NSString *message;
@property (nonatomic, strong) SKColor *fontColor;
@property (nonatomic, strong) AlertActionBlock actionBlock;

- (instancetype)initWithMessage:(NSString *)message
                       fontSize:(CGFloat)fontSize
                      fontColor:(SKColor *)fontColor;

- (instancetype)initForInfoDisplayWithMessage:(NSString *)message
                                     fontSize:(CGFloat)fontSize
                                    fontColor:(SKColor *)fontColor;

- (void)showInScene:(SKScene *)scene;
- (void)showInScene:(SKScene *)scene completion:(void (^)())block;
- (void)dismiss;
- (void)dismissWithCompletion:(void (^)())block;

@end

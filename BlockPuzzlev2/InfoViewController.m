//
//  InfoViewController.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 22/02/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import "InfoViewController.h"

#import "GameConstants.h"
#import "OpenSourceCreditsViewController.h"

@implementation InfoViewController

- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(id)sender
{
    if ([segue.identifier isEqualToString:ShowOpenSourceCreditsSegue]) {
        OpenSourceCreditsViewController *openSourceCreditsViewController = (OpenSourceCreditsViewController *)segue.destinationViewController;
        openSourceCreditsViewController.openSourceCreditsHTMLPage = OpenSourceCreditsHTMLPage;
    }
}

- (IBAction)openSourceCreditsAction:(id)sender
{
    [self performSegueWithIdentifier:ShowOpenSourceCreditsSegue
                              sender:self];
}

- (IBAction)dismissAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end

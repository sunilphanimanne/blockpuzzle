//
//  AnimatingLabelNode.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 01/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "AnimatingLabelNode.h"


static const NSInteger AnimatingLabelThresholdDeltaValue = 20;

@implementation AnimatingLabelNode

- (void)setText:(NSString *)text
{
    if (![self.text isEqualToString:text]) {
        [self setTextWithAnimation:text];
    }
}

- (void)setText:(NSString *)text animated:(BOOL)animated
{
    if (animated) {
        [self setText:text];
    }else {
        [super setText:text];
    }
}

//Assumption: no negative values...
- (void)setTextWithAnimation:(NSString *)text
{
    DDLogDebug(@"AnimatingLabel: setTextWithAnimation:%@", text);
    
    NSUInteger startValue = [self.text integerValue];
    NSUInteger deltaValue = [text integerValue] - startValue;
    NSInteger loopValue = deltaValue;
    
    DDLogDebug(@"AnimatingLabel: Before loopValue if:%ld", (long)loopValue);
    
    if (loopValue > 0 && !self.hidden) {
        DDLogDebug(@"AnimatingLabel: Outside Loop:");
        
        if (loopValue >= AnimatingLabelThresholdDeltaValue) {
            loopValue = AnimatingLabelThresholdDeltaValue;
            startValue = ([text integerValue] - AnimatingLabelThresholdDeltaValue);
        }
        
        for (NSInteger i=1; i <= loopValue; i++) {
            SKAction *action = [SKAction waitForDuration:i * AnimatingLabelAnimationMultiplier];
            
            DDLogDebug(@"AnimatingLabel: Before running action:");
            
            [self runAction:action completion:^{
                DDLogDebug(@"AnimatingLabel: Setting text:");
                super.text = [NSString stringWithFormat:@"%lu", (unsigned long)startValue + i];
            }];
        }
    } else {
        DDLogDebug(@"Entered else");
        super.text = text;
    }
}

@end

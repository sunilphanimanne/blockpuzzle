//
//  PlayerManager.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 03/05/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "PlayerManager.h"

@import GameKit;

#import "GameConstants.h"
#import "GameKitHelper.h"
#import "PersistanceManager.h"

NSString *const PlayerManagerPersistanceKey = @"Persistence.PlayerManagerKey";

@implementation PlayerManager

Player *_currentPlayer;

+ (instancetype)sharedInstance
{
    static PlayerManager *sharedInstance = nil;
    static dispatch_once_t playerManagerToken;
    dispatch_once(&playerManagerToken, ^{
        sharedInstance = [[self class] loadInstance];
    });
    
    return sharedInstance;
}

+ (instancetype)sharedManager
{
    return [self sharedInstance];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [self init]) {
        _currentPlayer = [aDecoder decodeObjectForKey:PlayerManagerCurrentPlayerDataKey];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_currentPlayer forKey:PlayerManagerCurrentPlayerDataKey];
}

+ (instancetype)loadInstance
{
    PlayerManager *playerManger = [PersistanceManager loadInstanceForClass:self
                                                                    andKey:PlayerManagerPersistanceKey];
    
    [playerManger loadCurrentPlayer];
    
    return playerManger;
}

- (void)save
{
    [PersistanceManager saveObject:self forKey:PlayerManagerPersistanceKey];
}

- (void)loadCurrentPlayer
{
    Player *nativePlayer = _currentPlayer;
    
    if (!nativePlayer) {
        //Create a new player...
        nativePlayer = [self createANewNativePlayer];
    }
    
    //Implicit contract callback
    [self playerLoaded:nativePlayer];
}

- (void)playerLoaded:(Player *)player
{
    if (![_currentPlayer isEqual:player]) {
        _currentPlayer = player;
        [self save];
    }
}

- (Player *)createANewNativePlayer
{
    Player *player = [[Player alloc] init];
    return player;
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end

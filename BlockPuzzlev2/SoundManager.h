//
//  SoundManager.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 24/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundManager : NSObject <NSSecureCoding>

@property (nonatomic, assign) BOOL isSFXEnabled;
@property (nonatomic, assign) BOOL isMusicEnabled;

+ (instancetype)sharedManager;

//Sound related
- (void)playSound:(NSString *)sound;
- (void)playSound:(NSString *)sound withKey:(NSString *)key;
- (void)stopSoundWithKey:(NSString *)key;
- (BOOL)isSoundPlayingWithKey:(NSString *)key;

//SFX related, depends on the isSFXEnabled flag whether to play or not
- (void)playSFX:(NSString *)sfx;

//Music related, depends on the isMusicEnabled flag whether to play or not
- (void)playMusic:(NSString *)music;
- (void)stopMusic;
- (void)pauseMusic;
- (void)resumeMusic;

@end

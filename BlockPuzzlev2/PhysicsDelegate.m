//
//  PhysicsDelegate.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 19/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "PhysicsDelegate.h"

#import "GameUtils.h"
#import "GameScene.h"
#import "MinuteBlock.h"
#import "SoundManager.h"
#import "GameConstants.h"

//Physics constants
static const uint32_t BlockCategory  = 0x1 << 0;
static const uint32_t WorldCategory  = 0x1 << 1;
static const CGFloat BlockFriction = 0.0f;
static const CGFloat BlockRestitution = 0.5f;
static const CGFloat BlockFallThresholdHeightPercentage = 38.38f;

#define WORLD_GRAVITY CGVectorMake(0.0, -25)

@implementation PhysicsDelegate

NSMutableArray*_blocksFalling;
BOOL _didCallEndCollisions;
ContactsEndedBlock _contactsEndedBlock;

- (void)setupPhysicsForBlock:(BlockShape *)block
{
    //Physics Body
    block.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:block.size];
    block.physicsBody.friction = BlockFriction;
    block.physicsBody.restitution = BlockRestitution;
    block.physicsBody.usesPreciseCollisionDetection = YES;
    block.physicsBody.categoryBitMask = BlockCategory;
    block.physicsBody.contactTestBitMask = BlockCategory | WorldCategory;
}

- (void)setupPhysicsForScene:(SKScene *)scene
{
    [self setupPhysicsForScene:scene allContactsEnded:nil];
}

- (void)setupPhysicsForScene:(SKScene *)scene
            allContactsEnded:(ContactsEndedBlock)block
{
    //Physcis World
    CGRect rect = GetActualPuzzleBoardRect();
    DDLogDebug(@"Physics World is confined to:%@", NSStringFromCGRect(rect));
    
    scene.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:rect];
    scene.physicsWorld.gravity = WORLD_GRAVITY;
    scene.physicsBody.categoryBitMask = WorldCategory;
    
    scene.physicsWorld.contactDelegate = self;
    
    _blocksFalling = [NSMutableArray arrayWithCapacity:3];
    _didCallEndCollisions = NO;
    
    [[SoundManager sharedManager] playSFX:BlocksFallingSFXName];
    
    if (block) {
        _contactsEndedBlock = block;
        
        //If the game did not start yet, then the physics delegate wont get called, manually call them...
        if (!SCENE.isGameStarted) {
            _contactsEndedBlock();
        }
    }
}

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    [self handleBeginContactForNode:contact.bodyA.node];
    [self handleBeginContactForNode:contact.bodyB.node];
}

- (void)didEndContact:(SKPhysicsContact *)contact
{
    [self handleEndContactForNode:contact.bodyA.node];
    [self handleEndContactForNode:contact.bodyB.node];
}

- (BOOL)didTheNodeReachTheGround:(SKNode *)node
{
    BOOL didReach = NO;
    if (node) {
        if (node.position.y <= [PhysicsDelegate absoluteThresholdHeight]) {
            didReach = YES;
        }
    }
    
    return didReach;
}

- (void)runDisappearAnimationOn:(SKNode *)node
{
    SKAction *fadeOutAction = [SKAction fadeOutWithDuration:0.2];
    SKAction *removeAction = [SKAction removeFromParent];
    SKAction *sequence = [SKAction sequence:@[fadeOutAction, removeAction]];
    
    [node runAction:sequence];
}

- (void)handleBeginContactForNode:(SKNode *)node
{
    if ([GameUtils isAMinuteBlock:node]) {
        DDLogDebug(@"BeginContact:");
        if(!((MinuteBlock *)node).didFallDownToGround) {
            DDLogDebug(@"Body Position:%@", NSStringFromCGPoint(node.position));
            [_blocksFalling addObject:node];
        }
    }
}

- (void)handleEndContactForNode:(SKNode *)node
{
    if ([GameUtils isAMinuteBlock:node]) {
        DDLogDebug(@"EndContact:");
        DDLogDebug(@"Body Position:%@", NSStringFromCGPoint(node.position));
        
        if ([self didTheNodeReachTheGround:node]) {
            ((MinuteBlock *)node).didFallDownToGround = YES;
            [self runDisappearAnimationOn:node];
            
            [_blocksFalling removeObject:node];
        }
    }
    
    if ([_blocksFalling count] <= 0 && !_didCallEndCollisions) {
        _didCallEndCollisions = YES;
        DDLogDebug(@"Before calling contactsEndedBlock!");
        
        if (_contactsEndedBlock) {
            _contactsEndedBlock();
        }
    }
}

+ (CGFloat)absoluteThresholdHeight
{
    CGFloat thresholdHeight = GetScreenSize().height * BlockFallThresholdHeightPercentage/100.;
    return thresholdHeight;
}

@end

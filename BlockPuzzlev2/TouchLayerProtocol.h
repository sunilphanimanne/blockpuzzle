//
//  TouchLayerProtocol.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, TouchTrend) {
    TouchTrendUnknown = 0,
    TouchTrendXDecreasing = 1 << 0,
    TouchTrendXIncreasing = 1 << 1,
    TouchTrendYDecreasing = 1 << 2,
    TouchTrendYIncreasing = 1 << 3
};

@protocol TouchLayerProtocol <NSObject>

@required
@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) CGFloat zPosition;
@property (nonatomic, assign) BOOL isMutuallyExclusive;
@property (nonatomic, assign) BOOL touchesEnabled;

@optional

//Basic touch methods

- (void)preTouchesHandlerForTouchesAtPoint:(CGPoint)point;

- (void)postTouchesHandlerForTouchesAtPoint:(CGPoint)point;

- (void)touchesBeganAtPoint:(CGPoint)point;

- (void)touchesMovedAtPoint:(CGPoint)point;

- (void)touchesEndedAtPoint:(CGPoint)point;

- (void)touchesCancelledAtPoint:(CGPoint)point;

- (void)touchesOutOfRangeAtPoint:(CGPoint)point;

//Trend touch methods

- (void)preTouchesHandlerForTouchesAtPoint:(CGPoint)point withTouchTrend:(TouchTrend)trend;

- (void)postTouchesHandlerForTouchesAtPoint:(CGPoint)point withTouchTrend:(TouchTrend)trend;

- (void)touchesBeganAtPoint:(CGPoint)point withTouchTrend:(TouchTrend)trend;

- (void)touchesMovedAtPoint:(CGPoint)point withTouchTrend:(TouchTrend)trend;

- (void)touchesEndedAtPoint:(CGPoint)point withTouchTrend:(TouchTrend)trend;

- (void)touchesCancelledAtPoint:(CGPoint)point withTouchTrend:(TouchTrend)trend;

@end

//
//  ScoreListener.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 06/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameConstants.h"

@protocol ScoreListener <NSObject>

@required
- (void)scoreType:(ScoreKeeperScoreType)scoreType
         newValue:(int64_t)newValue
         oldValue:(int64_t)oldValue
          context:(NSDictionary *)context;

@end

//
//  Player+Private.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 03/05/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

@interface Player ()

@property (nonatomic, strong) NSString *playerID;

@end

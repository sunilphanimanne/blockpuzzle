//
//  ScoreKeeper+Private.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 02/05/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

@interface ScoreKeeper ()

@property (nonatomic, assign) int64_t currentScore;
@property (nonatomic, assign) int64_t highScore;

@end

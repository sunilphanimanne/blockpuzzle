//
//  BlockShape.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 18/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "YCMatrix.h"

/*!
 Enumeration indicating the different Block Shape States.
 */
typedef NS_ENUM(NSUInteger, BlockShapeState) {
    BlockShapeStateBuffered,
    BlockShapeStateMoving,
    BlockShapeStateMomentarilyPlaced,
    BlockShapeStatePlaced,
    BlockShapeStateMerged,
    BlockShapeStateNotMerged,
    BlockShapeStateDisabled,
    BlockShapeStateDisabledMoving
};

/*!
 Enumeration indicating the different Block Shapes.
 */
typedef NS_ENUM(NSUInteger, BlockShapeType) {
    //Small blocks
    BlockShapeTypeDot,
    BlockShapeTypeDoubleDotHorizontal,
    BlockShapeTypeDoubleDotVertical,
    BlockShapeTypeSmallI,
    BlockShapeTypeBigI,
    BlockShapeTypeSmallL,
    BlockShapeTypeSmallSquare,
    
    //Big blocks
    BlockShapeTypeBigL,
    BlockShapeTypeExtendedI,
    BlockShapeTypeExtendedL,
    BlockShapeTypeBigSquare,
    BlockShapeTypeT,
    BlockShapeTypeZ
};

@class BlockShape;
typedef void (^BlockShapePlacedAction) (BlockShape* shape);

@interface BlockShape : SKSpriteNode

@property (nonatomic, assign) CGFloat scale;
@property (nonatomic, assign) NSInteger offset;
@property (nonatomic, strong) SKColor *shapeColor;
@property (nonatomic, assign) BlockShapeState state;
@property (nonatomic, readonly) BlockShapeType type;
@property (nonatomic, readonly) Matrix *shapeMatrix;
@property (nonatomic, assign) CGFloat blockPadding;
@property (nonatomic, assign) CGPoint originalPosition;
@property (nonatomic, strong) BlockShapePlacedAction placedAction;

+ (instancetype)blockShapeOfType:(BlockShapeType)type;
- (instancetype)initWithMatrix:(Matrix *)matrix andColor:(SKColor *)color;

- (void)prepareShape;
- (CGSize)computeSize;
- (void)startColorAnimation;
- (void)stopColorAnimation;

- (void)addShapeBlocksToNode:(SKNode *)node;
- (void)addShapeBlocksToNode:(SKNode *)node atOffset:(CGPoint)offset;

@end

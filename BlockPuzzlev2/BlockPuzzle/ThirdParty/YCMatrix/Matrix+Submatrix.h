//
//  Matrix+Submatrix.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 17/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "Matrix.h"

@interface Matrix (Submatrix)

- (BOOL)canAccomodate:(Matrix *)otherMatrix;
- (Matrix *)submatrixWithRowRange:(NSRange)rowRange
                   andColumnRange:(NSRange)columnRange;

- (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row;

- (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row
              withPadding:(CGFloat)padding;

- (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row
              withPadding:(CGFloat)padding
               atPosition:(CGPoint)position;

- (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row
              withPadding:(CGFloat)padding
               atPosition:(CGPoint)position
           forBlockLength:(CGFloat)blockLength;

- (void)setMatrix:(Matrix *)submatrix atColumn:(NSUInteger)column andRow:(NSUInteger)row;
- (void)maskMatrix:(Matrix *)submatrix atColumn:(NSUInteger)column andRow:(NSUInteger)row;

- (void)setZerosToRow:(NSInteger)row;
- (void)setZerosToColumn:(NSInteger)column;

- (BOOL)isAllColumnsOnesForRow:(NSInteger)row;
- (BOOL)isAllRowsOnesForColumn:(NSInteger)column;
- (BOOL)isAllZeros;

@end

//
//  Matrix+Submatrix.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 17/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "Matrix+Submatrix.h"

#import "YCMatrix.h"
#import "GameConstants.h"

#define XOR !=

@implementation Matrix (Submatrix)

//TODO: Move the canAccomdate: and canAccomodateMatrixOfSameRank: into Someother class...
- (BOOL)canAccomodate:(Matrix *)otherMatrix
{
    BOOL result = NO;
    
    int rowLength = otherMatrix.rows;
    int columnLength = otherMatrix.columns;
    
    if (self.rows >= rowLength && self.columns >= columnLength) {
        for (int i=0; i <= (self.rows - rowLength); i++) {
            NSRange rowRange = NSMakeRange(i, rowLength);
            
            for (int j=0; j <= (self.columns - columnLength); j++) {
                NSRange columnRange = NSMakeRange(j, columnLength);
                
                Matrix *subMatrix = [self submatrixWithRowRange:rowRange andColumnRange:columnRange];
                result = result || [subMatrix canAccomodateMatrixOfSameRank:otherMatrix];
                
                if (result) {
                    break;
                }
            }
            
            if (result) {
                break;
            }
        }
    }
    
    return result;
}

- (BOOL)canAccomodateMatrixOfSameRank:(Matrix *)otherMatrix
{
    return [[self XORWith:otherMatrix] logicalAndValue];
}

- (Matrix *)submatrixWithRowRange:(NSRange)rowRange
                   andColumnRange:(NSRange)columnRange
{
    Matrix *submatrix = nil;
    
    if (MatrixRangeValid(self.rows, rowRange) &&
        MatrixRangeValid(self.columns, columnRange)) {
        if (rowRange.location == 0 && rowRange.length == self.rows && columnRange.location == 0 && columnRange.length == self.columns) {
            submatrix = [self copy];
        }else {
            submatrix = [[self matrixWithColumnsInRange:columnRange] matrixWithRowsInRange:rowRange];
        }
    }
    
    return submatrix;
}

NS_INLINE BOOL MatrixRangeValid(NSUInteger rowsOrColumns, NSRange range) {
    return (range.location < rowsOrColumns && range.location + range.length <= rowsOrColumns)?YES:NO;
}


//TODO: VERY Crude way of handling this. Figure of the best PRIVATE way of doing this...

- (Matrix *)XORWith:(Matrix *)aMatrix
{
    Matrix *result = [Matrix matrixOfRows:self.rows
                                  columns:self.columns
                                    value:0];
    
    for (int i=0; i<self.rows; i++) {
        for (int j=0; j<self.columns; j++) {
            int a = (int)[self i:i j:j];
            int b = (int)[aMatrix i:i j:j];
            int c = 0;
            
            /* 
                Handle don't care:
                ~~~~~~ ~~~~~ ~~~~
                if value of b is zero, it means it is DONT CARE,
                then the result c is always 1, not matter whether a is 0 or 1
             */
            if (!b) {
                c = 1;
            }else {
                c = (a XOR b);
            }
            
            [result setValue:c row:i column:j];
        }
    }
    
    return result;
}

- (BOOL)logicalAndValue
{
    BOOL result = YES;
    
    for (int i=0; i<self.rows; i++) {
        for (int j=0; j<self.columns; j++) {
            result = result && [self i:i j:j];
            
            if (!result) {
                break;
            }
        }
        if (!result) {
            break;
        }
    }
    
    return result;
}

- (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row
              withPadding:(CGFloat)padding
               atPosition:(CGPoint)position
{
    return [self pointForColumn:column row:row
                    withPadding:padding atPosition:position
                 forBlockLength:GetPuzzleBlockLength()];
}

- (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row
              withPadding:(CGFloat)padding
               atPosition:(CGPoint)position
           forBlockLength:(CGFloat)blockLength
{
    DDLogDebug(@"Padding:%f blockLength:%f", padding, blockLength);
    
    NSInteger newRow = (self.rows - 1) - row;
    
    CGFloat x = ( column * blockLength + padding * column);
    CGFloat y = ( newRow * blockLength + padding * newRow);
    
    return CGPointMake(x + position.x, y + position.y);
}

- (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row
              withPadding:(CGFloat)padding
{
    return [self pointForColumn:column row:row withPadding:padding atPosition:CGPointZero];
}

- (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row
{
    return [self pointForColumn:column row:row withPadding:PuzzleBlockPadding];
}

- (void)setMatrix:(Matrix *)submatrix atColumn:(NSUInteger)column andRow:(NSUInteger)row
{
    if (column + submatrix.columns <= self.columns && row + submatrix.rows <= self.rows) {
        for (int i=0; i<submatrix.rows; i++) {
            for (int j=0; j<submatrix.columns; j++) {
                [self setValue:[submatrix i:i j:j]
                           row:(int)(row + i)
                        column:(int)(column + j)];
            }
        }
    }
}

- (void)maskMatrix:(Matrix *)submatrix atColumn:(NSUInteger)column andRow:(NSUInteger)row
{
    if (column + submatrix.columns <= self.columns && row + submatrix.rows <= self.rows) {
        for (int i=0; i<submatrix.rows; i++) {
            for (int j=0; j<submatrix.columns; j++) {
                int rowInMasterMatrix = (int)(row + i);
                int colInMasterMatrix = (int)(column + j);
                
                CGFloat valueInMasterMatrix = [self i:rowInMasterMatrix j:colInMasterMatrix];
                CGFloat valueInSubmatrix = [submatrix i:i j:j];
                
                if (! valueInMasterMatrix && valueInSubmatrix) {
                    [self setValue:valueInSubmatrix
                               row:rowInMasterMatrix
                            column:colInMasterMatrix];
                }
            }
        }
    }
}

- (void)setZerosToColumn:(NSInteger)column
{
    for (int i=0; i<self.rows; i++) {
        [self setValue:0 row:i column:(int)column];
    }
}

- (void)setZerosToRow:(NSInteger)row
{
    for (int i=0; i<self.columns; i++) {
        [self setValue:0 row:(int)row column:i];
    }
}

- (BOOL)isAllColumnsOnesForRow:(NSInteger)row
{
    [self checkBoundsForRow:(int)row column:0];
    
    BOOL result = YES;
    BOOL isValid = NO;

    for (int j=0; j<self.columns && result; j++) {
        isValid = YES;
        result = (result && (BOOL)[self i:(int)row j:j]);
    }
    
    return result && isValid;
}

- (BOOL)isAllRowsOnesForColumn:(NSInteger)column
{
    [self checkBoundsForRow:0 column:(int)column];
    
    BOOL result = YES;
    BOOL isValid = NO;
    
    for (int i=0; i<self.rows && result; i++) {
        isValid = YES;
        result = (result && (BOOL)[self i:i j:(int)column]);
    }
    
    return result && isValid;
}

- (BOOL)isAllZeros
{
    BOOL result = YES;
    BOOL isValid = NO;
    
    for (int i=0; i<self.rows; i++) {
        for (int j=0; j<self.columns; j++) {
            isValid = YES;
            result = (result && !((BOOL)[self i:i j:j]));
        }
    }
    
    return result && isValid;
}

@end

//
//  GameUtils.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 18/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "GameUtils.h"

#import "Utils.h"
#import "GameScene.h"
#import "ScoreKeeper.h"
#import "MinuteBlock.h"
#import "SoundManager.h"
#import "GameConstants.h"
#import "Matrix+Submatrix.h"

#import <AVFoundation/AVFoundation.h>

@implementation GameUtils

+ (void)initialize
{
    time_t t;
    
    //Intialize the random number generator
    srand((unsigned) time(&t));
}

+ (SKSpriteNode *)getPuzzleBoardBlockWithColor:(SKColor *)color ofLength:(CGFloat)length
{
    SKSpriteNode *block = [MinuteBlock minuteBlockWithColor:color ofLength:length];
    
    block.name = PuzzleBackgroundBlockName;
    block.zPosition = PuzzleBackgroundBlockZPosition;
    
    return block;
}

+ (SKSpriteNode *)getPlayButton
{
    SKSpriteNode *playButton = [SKSpriteNode spriteNodeWithImageNamed:PlayButtonName];
    return playButton;
}

+ (CGPoint)pointForColumn:(NSInteger)column row:(NSInteger)row
{
    return [self pointForColumn:column row:row andPadding:PuzzleBlockPadding];
}

+ (CGPoint)pointForColumn:(NSInteger)column
                      row:(NSInteger)row
               andPadding:(NSUInteger)padding
{
    DDLogDebug(@"Column = %ld Row = %ld Padding = %ld", (long)column, (long)row, (long)padding);
    
    CGFloat x = column * (GetPuzzleBlockLength() + padding) + GetPuzzleBlockLength()/2;
    CGFloat y = row * (GetPuzzleBlockLength() + padding) + GetPuzzleBlockLength()/2;
    
    DDLogDebug(@"X:%lf Y:%lf", x, y);
    
    return CGPointMake(x, y);
}

+ (CGPoint)puzzleBoardPointForColumn:(NSInteger)column row:(NSInteger)row
{
    NSInteger newRow = PuzzleRows - 1 - row;
    return [self pointForColumn:column
                            row:newRow
                     withOffset:GetPuzzleBoardOffset()
                andBorderLength:GetMasterBorderLength()];
}

+ (CGPoint)pointForColumn:(NSInteger)column
                      row:(NSInteger)row
               withOffset:(CGPoint)offset
          andBorderLength:(CGFloat)borderLength
{
    CGPoint returnPoint;
    
    CGPoint originalPoint = [self pointForColumn:column row:row];
    
    DDLogDebug(@"Original Point:%@", NSStringFromCGPoint(originalPoint));
    returnPoint = CGPointMake(originalPoint.x + offset.x + borderLength,
                              originalPoint.y + offset.y + borderLength);
    
    DDLogDebug(@"Return Point:%@", NSStringFromCGPoint(returnPoint));
    
    return returnPoint;
}

+ (BOOL)convertPoint:(CGPoint)point toColumn:(NSInteger *)column row:(NSInteger *)row
{
    return [self convertPoint:point toColumn:column row:row beyoundBounds:NO];
}

+ (BOOL)convertPoint:(CGPoint)point toColumn:(NSInteger *)column row:(NSInteger *)row beyoundBounds:(BOOL)beyond
{
    
    BOOL isValidPoint = NO;
    
    if (beyond || (point.x >= GetMasterBorderLength() &&
                   point.x <= GetMasterBorderLength() + [self actualPuzzleBoardWidth] + GetPuzzleBoardOffset().x &&
                   point.y >= GetMasterBorderLength() &&
                   point.y <= GetMasterBorderLength() + [self actualPuzzleBoardHeight] + GetPuzzleBoardOffset().y)) {
        
        *column = roundf((point.x - GetPuzzleBoardOffset().x - GetMasterBorderLength() - GetPuzzleBlockLength()/2) / (GetPuzzleBlockLength() + PuzzleBlockPadding));
        *row = roundf((point.y - GetPuzzleBoardOffset().y - GetMasterBorderLength() - GetPuzzleBlockLength()/2) / (GetPuzzleBlockLength() + PuzzleBlockPadding));
        *row = PuzzleRows - *row - 1;
        
        isValidPoint = YES;
    } else {
        *column = NSNotFound;  // invalid point
        *row = NSNotFound;
    }
    
    DDLogDebug(@"Col>>%ld Row>>%ld", (long)*column, (long)*row);
    
    return isValidPoint;
}

+ (BOOL)convertAndCorrectPoint:(CGPoint)point toColumn:(NSInteger *)column row:(NSInteger *)row
{
    BOOL isValidPoint = NO;
    
    isValidPoint = [self convertPoint:point toColumn:column row:row beyoundBounds:YES];
    
    //Correction logic...
    [self correctRow:row andColumn:column];
    
    return isValidPoint;
}

+ (void)correctRow:(NSInteger *)row andColumn:(NSInteger *)column
{
    [self correctRow:row];
    [self correctColumn:column];
}

+ (void)correctRow:(NSInteger*)row
{
    if (*row < 0) {
        *row = 0;
    }
    
    if (*row >= PuzzleRows) {
        (*row)--;
    }
}

+ (void)correctColumn:(NSInteger*)column
{
    if (*column < 0) {
        *column = 0;
    }
    
    if (*column >= PuzzleColumns) {
        (*column)--;
    }
}


+ (BOOL)findPuzzleBlockColumn:(NSInteger *)column
                          row:(NSInteger *)row
                   atPosition:(CGPoint)position
                     forShape:(BlockShape *)shape
{
    BOOL isSuccess = NO;
    
    if ([self convertAndCorrectPoint:position toColumn:column row:row]) {
        isSuccess = YES;
    }
    return isSuccess;
}

+ (CGFloat)puzzleBoardWidth
{
    return GetScreenSize().width;
}

+ (CGFloat)actualPuzzleBoardWidth
{
    CGFloat width = PuzzleColumns * GetPuzzleBlockLength() +
                    (PuzzleColumns - 1) * PuzzleBlockPadding;
    return width;
}

+ (CGFloat)puzzleBoardHeight
{
    return [self actualPuzzleBoardHeight] + (GetMasterBorderSize().height * 2);
}


+ (CGFloat)actualPuzzleBoardHeight
{
    CGFloat height = PuzzleRows * GetPuzzleBlockLength() +
                      (PuzzleRows - 1) * PuzzleBlockPadding;
    return height;
}

+ (NSUInteger)random:(NSUInteger)max
{
    NSUInteger ret = rand() % (max + 1);
    return ret;
}

+ (BlockShapeType)getRandomBlockUntil:(BlockShapeType)blockShape
{
    return [self random:blockShape];
}

+ (CGPoint)point:(CGPoint)point withOffset:(CGPoint)offset
{
    //Create an offset so that the block is visible while moving the finger...
    return CGPointMake(point.x + offset.x, point.y + offset.y);
}

+ (CGPoint)effectiveOffsetForShape:(BlockShape *)shape
{
    CGPoint offset = GetBufferBlockOffset();
    return CGPointMake((shape.shapeMatrix.columns - 1) * GetPuzzleBlockLength()/2 * offset.x, offset.y);
}

+ (BOOL)canTheBlock:(BlockShape *)block
        fitAtColumn:(NSUInteger)column
             andRow:(NSUInteger)row
            inScene:(id)scene
{
    BOOL canFit = NO;
    
    NSUInteger effectiveRow = row - block.shapeMatrix.rows + 1;
    
    NSRange rowRange = NSMakeRange(effectiveRow, block.shapeMatrix.rows);
    NSRange columnRange = NSMakeRange(column, block.shapeMatrix.columns);
    
    DDLogDebug(@"Row Range:%@ Col Range:%@", NSStringFromRange(rowRange), NSStringFromRange(columnRange));
    
    Matrix *subMatrix = [((GameScene *)scene).puzzleMatrix submatrixWithRowRange:rowRange
                                                   andColumnRange:columnRange];
    
    DDLogDebug(@"Submatrix:%@", subMatrix);
    
    canFit = [subMatrix canAccomodate:block.shapeMatrix];
    
    return canFit;
}

+ (void)markTheShape:(BlockShape *)shape
            atColumn:(NSUInteger)column
              andRow:(NSUInteger)row
            forScene:(id)scene
{
    NSUInteger effectiveRow = row - shape.shapeMatrix.rows + 1;
    
    DDLogDebug(@"Marking the shape at Row:%lu Col:%lu", (unsigned long)effectiveRow, (unsigned long)column);
    
    DDLogDebug(@"Shape matrix:%@", shape.shapeMatrix);
    
    [((GameScene *)scene).puzzleMatrix maskMatrix:shape.shapeMatrix atColumn:column andRow:effectiveRow];
    
    DDLogDebug(@"Matrix after masking:%@", ((GameScene *)scene).puzzleMatrix);
}

+ (void)rows:(NSArray **)rows andColumns:(NSArray **)columns affectedByBlock:(BlockShape *)shape atRow:(NSInteger)row andColumn:(NSInteger)column
{
    DDLogDebug(@"Row:%ld Col:%ld", (long)row, (long)column);
    
    NSMutableArray *rowsToBeChecked = [[NSMutableArray alloc] initWithCapacity:3];
    NSMutableArray *colsToBeChecked = [[NSMutableArray alloc] initWithCapacity:3];
    
    NSUInteger effectiveRow = row - shape.shapeMatrix.rows + 1;
    
    
    for (int i=0; i<shape.shapeMatrix.rows; i++) {
        [rowsToBeChecked addObject:@(effectiveRow + i)];
    }

    for (int j=0; j<shape.shapeMatrix.columns; j++) {
        [colsToBeChecked addObject:@(column + j)];
    }
    
    *rows = [NSArray arrayWithArray:rowsToBeChecked];
    *columns = [NSArray arrayWithArray:colsToBeChecked];
}

+ (void)handleLineCompletionAtRows:(NSArray*)rows andColumns:(NSArray *)columns forScene:(id)scene
{
    GameScene *gameScene = (GameScene *)scene;
    NSMutableArray *rowsArray = [NSMutableArray array];
    NSMutableArray *columnsArray = [NSMutableArray array];
    
    for (NSNumber* aRow in rows) {
        NSInteger aRowIntValue = [aRow intValue];
        
        if ([gameScene.puzzleMatrix isAllColumnsOnesForRow:aRowIntValue]) {
            DDLogDebug(@"Line finished at Row:%ld", (long)aRowIntValue);
            [self handleLineCompletionAtRow:aRowIntValue forScene:scene];
            [rowsArray addObject:aRow];
        }else {
            DDLogDebug(@"Line NOT finished at Row:%ld", (long)aRowIntValue);
        }
    }
    
    for (NSNumber* aColumn in columns) {
        NSInteger aColumnIntValue = [aColumn intValue];
        
        if ([gameScene.puzzleMatrix isAllRowsOnesForColumn:aColumnIntValue]) {
            DDLogDebug(@"Line finished at Column:%ld", (long)aColumnIntValue);
            [self handleLineCompletionAtColumn:aColumnIntValue forScene:scene];
            [columnsArray addObject:aColumn];
        }else {
            DDLogDebug(@"Line NOT finished at Column:%ld", (long)aColumnIntValue);
        }
    }

    NSUInteger lineCount = [columnsArray count] + [rowsArray count];
    
    if (lineCount) {
        BlockShape *currentBlock = gameScene.currentBufferedBlock;
        [gameScene.scoreKeeper incrementScoreForFinishingLines:lineCount
                                                      forBlock:currentBlock];
        [[SoundManager sharedManager] playSFX:BlocksLineClearSFXName];
        currentBlock.state = BlockShapeStateMerged;
    }
    
    //Clear the matrix...
    for (NSNumber *aRow in rowsArray) {
        [gameScene.puzzleMatrix setZerosToRow:[aRow intValue]];
    }
    
    for (NSNumber *aColumn in columnsArray) {
        [gameScene.puzzleMatrix setZerosToColumn:[aColumn intValue]];
    }
    
    [gameScene handleAllBoardClear];
}

+ (void)handleLineCompletionAtRow:(NSInteger)row forScene:(id)scene
{
    GameScene *gameScene = (GameScene *)scene;
    
    for (int i=0; i<gameScene.puzzleMatrix.columns; i++) {
        [self playAnimationAtRow:row andColumn:i inScene:scene];
    }
}

+ (void)handleLineCompletionAtColumn:(NSInteger)column forScene:(id)scene
{
    GameScene *gameScene = (GameScene *)scene;
    
    for (int i=0; i<gameScene.puzzleMatrix.rows; i++) {
        [self playAnimationAtRow:i andColumn:column inScene:scene];
    }
}

+ (NSUInteger)numberOfMinuteBlocksInShape:(BlockShape *)block
{
    return [block.shapeMatrix sum];
}

+ (void)playAnimationAtRow:(NSInteger)row andColumn:(NSInteger)column inScene:(id)scene
{
    SKSpriteNode *minuteBlock = [self minuteBlockAtRow:row andColumn:column inScene:scene];
    
    DDLogDebug(@"minuteBlock:%@", minuteBlock);
    [minuteBlock runAction:[self actionForLineCompletion]];
}

+ (SKSpriteNode *)minuteBlockAtRow:(NSInteger)row andColumn:(NSInteger)column inScene:(id)scene
{
    SKSpriteNode *minuteBlock = nil;
    GameScene *gameScene = (GameScene *)scene;
    CGPoint position = [self puzzleBoardPointForColumn:column row:row];
    
    NSArray *nodesFound = [gameScene nodesAtPoint:position];
    
    for (SKNode *aNode in nodesFound) {
        DDLogDebug(@"Node name:%@", aNode.name);
        
        if ([aNode.name isEqualToString:MinuteBlockName]) {
            minuteBlock = (SKSpriteNode *)aNode;
            break;
        }
    }
    
    return minuteBlock;
}

+ (SKAction *)actionForLineCompletion
{
    SKAction *sequence = [SKAction sequence:@[[SKAction scaleTo:LineFinishedScale
                                                       duration:LineFinishedAnimationDuration],
                                              [SKAction removeFromParent]]];
    
    sequence.timingMode = LineFinishedAnimationTiming;
    
    return sequence;
}

+ (CIFilter *)blurFilter
{
    CIFilter *filter = [CIFilter filterWithName:@"CIBoxBlur"];
    [filter setDefaults];
    [filter setValue:[NSNumber numberWithFloat:20] forKey:@"inputRadius"];
    return filter;
}

+ (NSString*)getGameDataFilePath
{
    static NSString* filePath = nil;
    if (!filePath) {
        filePath =
        [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                              NSUserDomainMask,
                                              YES) firstObject]
         stringByAppendingPathComponent:GameDataFileName];
    }
    
    return filePath;
}

CGRect rectSubtract(CGRect r1, CGRect r2, CGRectEdge edge) {
    // Find how much r1 overlaps r2
    CGRect intersection = CGRectIntersection(r1, r2);
    
    // If they don't intersect, just return r1. No subtraction to be done
    if (CGRectIsNull(intersection)) {
        return r1;
    }
    
    // Figure out how much we chop off r1
    float chopAmount = (edge == CGRectMinXEdge || edge == CGRectMaxXEdge)
    ? intersection.size.width
    : intersection.size.height;
    
    CGRect r3, throwaway;
    // Chop
    CGRectDivide(r1, &throwaway, &r3, chopAmount, edge);
    return r3;
}

+ (void)sendBlockBackToBufferBoardForScene:(id)scene
{
    GameScene *gameScene = ((GameScene *)scene);
    
    [self setBlockStateWhileSendingBackForBlock:gameScene.currentBufferedBlock];
    
    gameScene.currentBufferedBlock.position = gameScene.currentBufferedBlock.originalPosition;
    gameScene.currentBufferedBlock = nil;
}

+ (void)setBlockStateWhileSendingBackForBlock:(BlockShape *)block
{
    if (block.state == BlockShapeStateDisabledMoving) {
        block.state = BlockShapeStateDisabled;
    }else {
        block.state = BlockShapeStateBuffered;
    }
}

+ (void)sendBlockBackToBufferBoardWithAnimationForScene:(id)scene
{
    GameScene *gameScene = ((GameScene *)scene);
    SKAction *moveAction = [SKAction moveTo:gameScene.currentBufferedBlock.originalPosition
                                   duration:BlockRestorePositionAnimationDuration];
    moveAction.timingMode = SKActionTimingEaseInEaseOut;
    
    //Save the block's reference into local variable...
    BlockShape *currentBlock = gameScene.currentBufferedBlock;
    
    //dereference the current block...
    gameScene.currentBufferedBlock = nil;
    
    [currentBlock runAction:moveAction completion:^{
        [self setBlockStateWhileSendingBackForBlock:currentBlock];
    }];
}

+ (UIImage *)takeScreenshotForView:(UIView *)view
{
    return [self takeScreenshotForView:view isOpaque:YES];
}

+ (UIImage *)takeScreenshotForView:(UIView *)view isOpaque:(BOOL)isOpaque
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, isOpaque, 1);
    
    BOOL isSuccess = [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    
    DDLogDebug(@"GameUtils: takeScreenshotForView: Success:%@", BoolToString(isSuccess));
    
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return  viewImage;
}

+ (UIColor *)getVariant:(NSUInteger)index
               forColor:(SKColor *)color
                andComponent:(NSUInteger)component
{
    UIColor *variantColor = color;
    if (color) {
        CGFloat incrementValue = 100/255.;
        CGFloat red = 0.;
        CGFloat green = 0.;
        CGFloat blue = 0.;
        CGFloat alpha = 0.;
        
        [color getRed:&red green:&green blue:&blue alpha:&alpha];
        
        switch (component) {
            case 0:
                red += incrementValue;
                
                if (red > 1.) {
                    red = 1.;
                }
                
                break;
            
            case 1:
                green += incrementValue;
                
                if (green > 1.) {
                    green = 1.;
                }
                
                break;
                
            case 2:
                blue += incrementValue;
                
                if (blue > 1.) {
                    blue = 1.;
                }
                
                break;
                
            case 3:
                alpha += incrementValue;
                
                if (alpha > 1.) {
                    alpha = 1.;
                }
                break;
        }
        
        variantColor = getColor(red, green, blue, alpha);
    }
    
    return variantColor;
}

+ (BOOL)isAMinuteBlock:(SKNode *)node
{
    return [node.name isEqualToString:MinuteBlockName];
}

+ (int64_t)getCurrentScore
{
    return SCENE.scoreKeeper.currentScore;
}

+ (int64_t)getHighScore
{
    return SCENE.scoreKeeper.highScore;
}

+ (NSUInteger)getCurrentLevel
{
    return SCENE.levelManager.currentLevel;
}

+ (CGFloat)getCurrentLevelPercentage
{
    return SCENE.levelManager.currentLevelPercentage;
}

+ (CGFloat)getAbsoluteWidthForPercentage:(CGFloat)widthPercent
{
    return GetScreenSize().width * widthPercent/100.;
}

+ (CGFloat)getAbsoluteHeightForPercentage:(CGFloat)heightPercent
{
    return GetScreenSize().height * heightPercent/100.;
}

CGFloat widthInPercent(CGFloat widthPercent)
{
    return [GameUtils getAbsoluteWidthForPercentage:widthPercent];
}

CGFloat heightInPercent(CGFloat heightPercent)
{
    return [GameUtils getAbsoluteHeightForPercentage:heightPercent];
}

SKColor *getColor(CGFloat red, CGFloat green, CGFloat blue, CGFloat alpha)
{
    SKColor *color = [SKColor colorWithRed:red/255.f
                                     green:green/255.f
                                      blue:blue/255.f
                                     alpha:alpha];
    return color;
}

+ (BOOL)isPuzzleBoardEmpty
{
    return [SCENE.puzzleMatrix isAllZeros];
}

+ (NSString *)stringContentsOfFile:(NSString *)filePath
                             error:(NSError **)error
{
    NSString *fullPath = [[NSBundle mainBundle] pathForResource:filePath ofType:nil];
    
    return [NSString stringWithContentsOfFile:fullPath
                                     encoding:NSUTF8StringEncoding
                                        error:error];
}

+ (void)showUserAppRatingsAlertIfRequired
{
    GameContext *gameContext = [GameContext sharedContext];
    if (gameContext.didUserFinishSignificantEvent) {
        [Appirater userDidSignificantEvent:YES];
        gameContext.didUserFinishSignificantEvent = NO;
    }
    
    [Utils configureAppiraterAndShow];
}

+ (void)showUserAppRatingsAlertForcefully
{
    [Appirater forceShowPrompt:YES];
}

@end

//
//  GameScene.h
//  BlockPuzzle
//

//  Copyright (c) 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

#import "YCMatrix.h"
#import "BlockShape.h"
#import "BlockFence.h"
#import "ScorePanel.h"
#import "ScoreKeeper.h"
#import "LevelManager.h"
#import "ThemeManager.h"
#import "PlayerManager.h"
#import "TouchController.h"
#import "GameoverOverlay.h"
#import "MasterTouchLayer.h"
#import "SidebarController.h"
#import "BlockMagneticLayer.h"
#import "ScoreboardTouchLayer.h"
#import "BufferBlockTouchLayer.h"

@interface GameScene : SKScene

//Puzzle matrix
@property (nonatomic, readonly) Matrix *puzzleMatrix;

//BufferedBoard related properties
@property (nonatomic, readonly) SKSpriteNode *bufferBoard;
@property (nonatomic, assign) NSUInteger numberOfFreeBlocks;
@property (nonatomic, weak) BlockShape *currentBufferedBlock;
@property (nonatomic, readonly) SKSpriteNode *puzzleBoardPanel;
@property (nonatomic, assign) NSInteger currentBufferedBlockIndex;
@property (nonatomic, readonly) NSArray<BufferBlockTouchLayer *> *bufferBlockTouchLayers;

//Touch and fence related properties
@property (nonatomic, readonly) BlockFence *blockFence;
@property (nonatomic, readonly) TouchController *touchController;
@property (nonatomic, readonly) MasterTouchLayer *masterTouchLayer;
@property (nonatomic, readonly) BlockMagneticLayer *blockMagneticLayer;
@property (nonatomic, readonly) ScoreboardTouchLayer *scoreboardTouchLayer;

//Score-related properties
@property (nonatomic, readonly) ScorePanel *scorePanel;
@property (nonatomic, readonly) ScoreKeeper *scoreKeeper;

//Gameover overlay screen
@property (nonatomic, strong) GameoverOverlay *gameoverOverlayScreen;

//Level related properties
@property (nonatomic, readonly) LevelManager *levelManager;

//TODO: Remove this. This is obselete. Better way is to access using levelManager.
@property (nonatomic, readonly) NSUInteger currentLevel;

//Theme related properties
@property (nonatomic, readonly) ThemeManager *themeManager;

//Sidebar related  properties
@property (nonatomic, readonly) SidebarController *sidebarController;

//PlayerManager related  properties
@property (nonatomic, readonly) PlayerManager *playerManager;

@property (nonatomic, readonly, getter=isGameStarted) BOOL gameStarted;

//methods
- (void)flipTheme;
- (BOOL)isGameOver;
- (void)restartGame;
- (void)handleGameOver;
- (void)initializeGame;
- (void)generateBlocksInBuffer;
- (void)generateBlockInBufferAtIndex:(NSUInteger)index;
- (void)disableTheBufferBlocks;
- (void)handleAllBoardClear;

@end

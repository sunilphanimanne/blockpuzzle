//
//  BlockShape.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 18/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "BlockShape.h"

#import "GameUtils.h"
#import "MinuteBlock.h"
#import "SoundManager.h"
#import "GameConstants.h"

NSArray *_allShapeMatrices;
NSArray *_allShapeRanks;
NSArray *_allShapeColors;

@interface BlockShape ()
{
    @private
    SKSpriteNode *_aBlock;
    BOOL _isColorAnimationRunning;
}
@end

@implementation BlockShape

+ (void)initialize
{
    [self initShapeMetaInfo];
}

- (instancetype)initWithMatrix:(Matrix *)matrix andColor:(SKColor *)color
{
    if (self = [super init]) {
        _shapeMatrix = matrix;
        _shapeColor = color;
        _aBlock = [MinuteBlock minuteBlockWithColor:color];
        _aBlock.name = MinuteBlockName;
        _blockPadding = PuzzleBlockPadding;
        _state = BlockShapeStateBuffered; //Default State
        _offset = BlockShapeStateBufferedOffset;
        
        _isColorAnimationRunning = NO;
        [self setScale:BlockShapeStateBufferedScale];
        [self setName:PuzzleBlockShapeName];
        self.zPosition = BlockShapeDefaultZPosition;
        [self prepareShape];
        
        //Update the size
        self.size = [self computeSize];
    }
    
    return self;
}

+ (void)initShapeMetaInfo
{
    if(!_allShapeMatrices) {
        _allShapeMatrices = @[@[@1],                    //BlockShapeTypeDot
                              
                              @[@1, @1],          //BlockShapeTypeDoubleDotHorizontal
                              
                              @[@1,               //BlockShapeTypeDoubleDotVertical
                                @1],
                              
                              @[@1,
                                @1,
                                @1],                    //BlockShapeTypeSmallI
                              
                              @[@1,
                                @1,
                                @1,
                                @1],                    //BlockShapeTypeBigI
                              
                              @[@1, @1,                 //BlockShapeTypeSmallL
                                @1, @0],
                              
                              @[@1, @1,                 //BlockShapeTypeSmallSquare
                                @1, @1],
                              
                              @[@1, @0, @0,             //BlockShapeTypeBigL
                                @1, @1, @1],
                              
                              @[@1, @1, @1, @1, @1],    //BlockShapeTypeExtendedI
                              
                              @[@1, @1, @1,             //BlockShapeTypeExtendedL
                                @0, @0, @1,
                                @0, @0, @1],
                            
                              @[@1, @1, @1,             //BlockShapeTypeBigSquare
                                @1, @1, @1,
                                @1, @1, @1],
                            
                              @[@0, @1, @0,             //BlockShapeTypeT
                                @1, @1, @1],
                              
                              @[@0, @1, @1,             //BlockShapeTypeZ
                                @1, @1, @0]];
        
        _allShapeRanks = @[@[@1,@1],    //BlockShapeTypeDot
                           @[@1,@2],    //BlockShapeTypeDoubleDotHorizontal
                           @[@2,@1],    //BlockShapeTypeDoubleDotVertical
                           @[@3,@1],    //BlockShapeTypeSmallI
                           @[@4,@1],    //BlockShapeTypeBigI
                           @[@2,@2],    //BlockShapeTypeSmallL
                           @[@2,@2],    //BlockShapeTypeSmallSquare
                           @[@2,@3],    //BlockShapeTypeBigL
                           @[@1,@5],    //BlockShapeTypeExtendedI
                           @[@3,@3],    //BlockShapeTypeExtendedL
                           @[@3,@3],    //BlockShapeTypeBigSquare
                           @[@2,@3],    //BlockShapeTypeT
                           @[@2,@3]];   //BlockShapeTypeZ
        
        SKColor *blueColor1 = getColor(37, 146, 227, 1);
        
        SKColor *redColor1 = getColor(243, 29, 41, 1);//getColor(221, 56, 44, 1);
    
        SKColor *redColor2 = getColor(253, 40, 28, 1);
        
        SKColor *greenColor1 = getColor(171, 251, 50, 1);
        
        SKColor *aquaBlueColor = getColor(33, 193, 253, 1);//getColor(107, 96, 198, 1);
    
        SKColor *lavenderColor = getColor(160, 77, 219, 1);

        SKColor *orangeColor = getColor(253, 111, 36, 1);
        
        _allShapeColors = @[aquaBlueColor,
                            [SKColor greenColor],
                            [SKColor greenColor],
                            lavenderColor,
                            redColor1,
                            orangeColor,
                            [SKColor magentaColor],
                            blueColor1,
                            [SKColor cyanColor],
                            [SKColor yellowColor],
                            redColor2,
                            greenColor1,
                            [SKColor blueColor]];
    }
}

+ (instancetype)blockShapeOfType:(BlockShapeType)type
{
    DDLogDebug(@"BlockShape:blockShapeOfType:%@", @(type));
    BlockShape *shape = nil;
    NSArray *shapeMatrixArray = nil;
    NSArray *shapeMatrixRank = nil;
    SKColor *shapeColor = nil;
    
    if ([_allShapeMatrices count] >= type ) {
        shapeMatrixArray = _allShapeMatrices[type];
    }
    
    if ([_allShapeRanks count] >= type ) {
        shapeMatrixRank = _allShapeRanks[type];
    }
    
    if ([_allShapeColors count] >= type) {
        shapeColor = _allShapeColors[type];
    }
    
    if (shapeMatrixArray &&
        shapeMatrixRank && [shapeMatrixRank count] == 2 &&
        shapeColor) {
        Matrix *shapeMatrix = [Matrix matrixFromNSArray:shapeMatrixArray
                                                   rows:[shapeMatrixRank[0] intValue]
                                                columns:[shapeMatrixRank[1] intValue]];
        
        shape = [[self alloc] initWithMatrix:shapeMatrix
                                    andColor:shapeColor];
        shape->_type = type;
    }
    
    return shape;
}

- (void)setBlockPadding:(CGFloat)blockPadding
{
    if (_blockPadding != blockPadding) {
        _blockPadding = blockPadding;
    }
}

- (void)setShapeColor:(SKColor *)shapeColor
{
    if (![_shapeColor isEqual:shapeColor]) {
        _shapeColor = shapeColor;
        _aBlock = [MinuteBlock minuteBlockWithColor:_shapeColor];
    }
}

- (void)prepareShape
{
    [self addShapeBlocksToNode:self];

#if 0
    [self setPath:CGPathCreateWithRoundedRect(CGRectMake(0, 0, shapeSize.width, shapeSize.height),
                                              4, 4, nil)];
    
    self.strokeColor = self.fillColor = getColor(0, 128, 255, 1);
#endif
}


//TODO: This method CAN be HIGHLY optimized
- (void)addShapeBlocksToNode:(SKNode *)node
{
    [self addShapeBlocksToNode:node atOffset:CGPointZero];
}

- (void)addShapeBlocksToNode:(SKNode *)node atOffset:(CGPoint)offset
{
    [self removeAllChildren];
    
    for (int i=_shapeMatrix.rows - 1; i>=0; i--) {
        for (int j=_shapeMatrix.columns - 1; j>=0; j--) {
            if ([_shapeMatrix i:i j:j]) {
                SKNode *aNode = [_aBlock copy];
                
                DDLogDebug(@"Block's size for Scale: %f %@", aNode.xScale, NSStringFromCGSize(aNode.calculateAccumulatedFrame.size));
                aNode.xScale = aNode.yScale = self.scale;
                DDLogDebug(@"Block's size for Scale: %f %@", aNode.xScale, NSStringFromCGSize(aNode.calculateAccumulatedFrame.size));
                
                DDLogDebug(@"Block State:%ld Padding:%f", (unsigned long)self.state, self.blockPadding);
                
                aNode.position = [_shapeMatrix pointForColumn:j row:i
                                                  withPadding:self.blockPadding
                                                   atPosition:offset
                                               forBlockLength:(GetPuzzleBlockLength() * _scale)];
                [node addChild:aNode];
            }
        }
    }
}

- (CGSize)computeSize
{
    CGFloat height = _shapeMatrix.rows * _aBlock.size.height * _scale + (_shapeMatrix.rows - 1) * self.blockPadding;
    CGFloat width = _shapeMatrix.columns * _aBlock.size.width * _scale + (_shapeMatrix.columns - 1) * self.blockPadding;
    
    return CGSizeMake(width, height);
}

- (void)setState:(BlockShapeState)state
{
    DDLogDebug(@"State:%ld", (unsigned long)state);
    
    if (_state != state) {
        BlockShapeState fromState = _state;
        _state = state;
        
        switch (state) {
            case BlockShapeStateBuffered:
                self.blockPadding = PuzzleBlockPadding;
                self.scale = BlockShapeStateBufferedScale;
                self.offset = BlockShapeStateBufferedOffset;
                self.zPosition = BlockShapeDefaultZPosition;
                break;
                
            case BlockShapeStateMoving:
            case BlockShapeStateMomentarilyPlaced:
                self.blockPadding = GetMovingPuzzleBlockPadding();
                self.scale = BlockShapeStateMovingScale;
                self.offset = BlockShapeStateMovingOffset;
                self.zPosition = BlockShapeMovingZPosition;
                break;
                
            case BlockShapeStatePlaced:
                self.blockPadding = PuzzleBlockPadding;
                self.scale = BlockShapeStatePlacedScale;
                self.offset = BlockShapeStatePlacedOffset;
                self.zPosition = BlockShapePlacedZPosition;
                break;
                
            case BlockShapeStateDisabled:
                self.blockPadding = PuzzleBlockPadding;
                self.scale = BlockShapeStateDisabledScale;
                self.offset = BlockShapeStateDisabledOffset;
                self.shapeColor = PuzzleBlockDisabledColor;
                self.zPosition = BlockShapeDisabledZPosition;
                break;
                
            case BlockShapeStateDisabledMoving:
                self.blockPadding = GetMovingPuzzleBlockPadding();
                self.scale = BlockShapeStateMovingScale;
                self.offset = BlockShapeStateDisabledOffset;
                self.shapeColor = PuzzleBlockDisabledColor;
                self.zPosition = BlockShapeDisabledZPosition;
                break;
            
            case BlockShapeStateMerged:
            case BlockShapeStateNotMerged:
                //Do nothing. These states are just attribute kind of states...
                break;
        }
        
        DDLogDebug(@"Block Padding:%f", self.blockPadding);
        DDLogDebug(@"Scale:%f", self.scale);
        
        if (state != BlockShapeStateMerged &&
            state != BlockShapeStateNotMerged) {
            [self prepareShape];
        }
        
        [self stateChangedFrom:fromState to:state];
    }
}

- (void)stateChangedFrom:(BlockShapeState)fromState to:(BlockShapeState)toState
{
    if (toState == BlockShapeStateNotMerged) {
        if (self.placedAction) {
            [[SoundManager sharedManager] playSFX:BlocksClickonSFXName];
            self.placedAction(self);
        }
    }
}

- (void)startColorAnimation
{
    if (!_isColorAnimationRunning) {
        NSMutableArray *actions = [NSMutableArray arrayWithCapacity:3];
        UIColor *variantColor = nil;
        
        for (NSUInteger i=0; i<3; i++) {
            variantColor = [GameUtils getVariant:i
                                        forColor:self.shapeColor
                                    andComponent:[GameUtils random:2]];
            
            SKAction *colorizeAction = [SKAction colorizeWithColor:variantColor
                                                  colorBlendFactor:1.0
                                                          duration:0.5];
            
            SKAction *waitAciton = [SKAction waitForDuration:0.5];
            [actions addObject:colorizeAction];
            [actions addObject:waitAciton];
        }
        
        SKAction *sequence = [SKAction sequence:actions];
        sequence = [SKAction repeatActionForever:sequence];
        
        [self applyColorActionOnMinuteBlocks:sequence];
        
        _isColorAnimationRunning = YES;
    }
}

- (void)stopColorAnimation
{
    if (_isColorAnimationRunning) {
        [self removeColorActionOnMinuteBlocks];
        _isColorAnimationRunning = NO;
    }
}

- (void)applyColorActionOnMinuteBlocks:(SKAction *)colorizeAction
{
    [self enumerateChildNodesWithName:MinuteBlockName
                           usingBlock:^(SKNode * _Nonnull node, BOOL * _Nonnull stop) {
                               [node runAction:colorizeAction withKey:@"colorizeAction"];
                           }
    ];
}

- (void)removeColorActionOnMinuteBlocks
{
    [self enumerateChildNodesWithName:MinuteBlockName usingBlock:^(SKNode * _Nonnull node, BOOL * _Nonnull stop) {
        [node removeActionForKey:@"colorizeAction"];
    }];
}

@end

//
//  AppDelegate.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 12/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "AppDelegate.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "Utils.h"
#import "AdUtils.h"
#import "GameContext.h"
#import "SoundManager.h"
#import "GameConstants.h"
#import "AnalyticsLogUtils.h"
#import "GameViewController.h"

@import Firebase;
@import FirebaseMessaging;
@import FirebaseInstanceID;

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate>
@end
#endif

#define SHOW_ALERT_FOR_REMOTE_NOTIFICATION 1

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Utils configureRemoteNotifications:self];
    
    [Utils configureLogging];
    
    [Utils configureGoogleAdmob];
    
    [Utils configureFirebase];
    
    [Utils configureRemoteConfig];
    
    [Utils configureLocalNotifications];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [AnalyticsLogUtils logAppStartDate:[NSDate new]];
    
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [((GameViewController *)[AdContext sharedContext].viewController) reconfigureAds];
    
    [AnalyticsLogUtils logOpenAppEvent];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [GameContext sharedContext].appDidEnterBackground = YES;
    
    [AdUtils incrementInterstitialThresholdCount];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [[SoundManager sharedManager] pauseMusic];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[SoundManager sharedManager] resumeMusic];
    
    [FBSDKAppEvents activateApp];
    
    GameContext *gameContext = [GameContext sharedContext];
    AdContext *adContext = [AdContext sharedContext];
    
    if (gameContext.appDidEnterBackground) {
        if (!adContext.isBannerAdClicked) {
            [AdUtils showIntersititalAd];
        }else {
            adContext.isBannerAdClicked = NO;
        }
        
        //Clear the flag off
        [GameContext sharedContext].appDidEnterBackground = NO;
    }
    
    [Appirater appEnteredForeground:YES];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    
    [self handleRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
 
    [self handleRemoteNotification:userInfo];
}

- (void)handleRemoteNotification:(NSDictionary *)userInfo
{
#if SHOW_ALERT_FOR_REMOTE_NOTIFICATION
    [Utils showAlertForRemoteNotificationUserInfo:userInfo
                                 inViewController:self.window.rootViewController];
#endif
    
    // Print message ID.
    if (userInfo[RemoteNotificationMessageIDKey]) {
        DDLogInfo(@"Message ID: %@", userInfo[RemoteNotificationMessageIDKey]);
    }
    
    // Print full message.
    DDLogInfo(@"%@", userInfo);
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    DDLogInfo(@"Received local notification:%@", notification);
    DDLogInfo(@"Alert:%@", notification.alertBody);
}

@end

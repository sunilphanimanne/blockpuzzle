//
//  AppDelegate.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 12/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  GameViewController.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 12/02/16.
//  Copyright (c) 2016 Sunil Phani Manne. All rights reserved.
//

#import "GameViewController.h"

#import "Utils.h"
#import "AdUtils.h"
#import "GameScene.h"
#import "GameContext.h"
#import "Reachability.h"
#import "GameKitHelper.h"
#import "PlayerManager.h"
#import "AVVideoDemoController.h"

#define SHOW_DEBUG_INFO 0

@interface GameViewController()
{
@private
    UIView *_bottomBannerAdView;
    UIView *_topBannerAdView;
}

@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AVVideoDemoController *videDemoController = [[AVVideoDemoController alloc] initWithVideoName:DemoVideoFileName
                                                                         presentOnViewController:self];
    [videDemoController playDemoIfRequired:^{
       [self initAndShowGameScene];
    }];
}

- (void)initAndShowGameScene
{
    [self setupAds];
    
    [self configureGameView];
    
    [self configureGameViewController];
    
    GameScene *gameScene = [self createAndPresentGameScene];
    
    [gameScene initializeGame];
    
    [self initializeGameKit];
}

- (void)configureGameView
{
    SKView * gameView = (SKView *)self.view;
    
#if SHOW_DEBUG_INFO
    gameView.showsFPS = YES;
    gameView.showsNodeCount = YES;
#endif
    
    gameView.multipleTouchEnabled = NO;
}

- (void)configureGameViewController
{
    [GameContext sharedContext].viewController = self;
}

- (GameScene *)createAndPresentGameScene
{
    SKView * gameView = (SKView *)self.view;
    
    // Create and configure the scene.
    GameScene *scene = [GameScene sceneWithSize:gameView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [gameView presentScene:scene];
    
    return scene;
}

- (void)initializeGameKit
{
    GameKitHelper *gameKitHelper = [GameKitHelper sharedHelper];
    [gameKitHelper authenticateLocalPlayerWithCompletionHandler:^(NSError *error) {
        if (error) {
            DDLogError(@"Error:Authenticating:%@", error.localizedDescription);
        }else {
            //Report the last high score
            [gameKitHelper reportHighScoreMissedDueToInternetConnectivityIssues];
        }
    }];
    
    [self setupReachabilityCallbacks];
}

- (void)setupAds
{
    [Utils fetchRemoteConfigValuesWithExpirationDuration:0
    completion:^(FIRRemoteConfigFetchStatus status, NSError * _Nullable error) {
        //bottom banner
        [self setupBottomBannerAd];
        
        //top banner
        [self setupTopBannerAd];
        
        //interstitial
        [self setupInterstitialAd];
        
        //native
        [self setupNativeAd];
    }];
}

- (void)reconfigureAds
{
    if ([AdUtils isAdsEnabled]) {
        [Utils fetchRemoteConfigValuesWithExpirationDuration:0 completion:^(FIRRemoteConfigFetchStatus status, NSError * _Nullable error) {
            if ([AdUtils shouldShowTopBanner]) {
                if (!_topBannerAdView) {
                    [self setupTopBannerAd];
                }
            }else {
                if (_topBannerAdView) {
                    [_topBannerAdView removeFromSuperview];
                    _topBannerAdView = nil;
                }
            }
        }];
    }
}

- (void)setupBottomBannerAd
{
    _bottomBannerAdView = [AdUtils getBottomBannerAdViewWithRootViewController:self];
    
    if (_bottomBannerAdView) {
        [self.view addSubview:_bottomBannerAdView];
    }
}

- (void)setupTopBannerAd
{
    _topBannerAdView = [AdUtils getTopBannerAdViewWithRootViewController:self];
    
    if (_topBannerAdView) {
        [self.view addSubview:_topBannerAdView];
    }
}

- (void)setupInterstitialAd
{
    [AdUtils prepareInterstitialAd];
}

- (void)setupNativeAd
{
#if 0
    [AdUtils prepareNativeAd];
#endif
}

- (void)setupReachabilityCallbacks
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    
    reachability.reachableBlock = ^(Reachability *reachability)
    {
        DDLogInfo(@"GameViewController: Internet reachable!");
        
        [AdUtils refreshBannerAdView:_bottomBannerAdView];
        
        [AdUtils refreshBannerAdView:_topBannerAdView];
        
        [AdUtils prepareInterstitialAd];
    };
    
    reachability.unreachableBlock = ^(Reachability *reachability)
    {
        DDLogError(@"GameViewController: Internet Unreachable!");
    };
    
    [reachability startNotifier];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    DDLogError(@"Received Low Memory Warning!");
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end

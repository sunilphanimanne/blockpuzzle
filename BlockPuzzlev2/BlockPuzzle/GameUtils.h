//
//  GameUtils.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 18/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <Foundation/Foundation.h>

#import "BlockShape.h"
#import "GameConstants.h"
#import "AnimatingLabelNode.h"

@interface GameUtils : NSObject

//PuzzleBlock methods
+ (SKSpriteNode *)getPuzzleBoardBlockWithColor:(SKColor *)color
                                      ofLength:(CGFloat)length;

//Point & column/row methods
+ (CGPoint)pointForColumn:(NSInteger)column
                      row:(NSInteger)row;

+ (CGPoint)pointForColumn:(NSInteger)column
                      row:(NSInteger)row
               andPadding:(NSUInteger)padding;

+ (CGPoint)pointForColumn:(NSInteger)column
                      row:(NSInteger)row
               withOffset:(CGPoint)offset
          andBorderLength:(CGFloat)borderLength;

+ (CGPoint)puzzleBoardPointForColumn:(NSInteger)column
                                 row:(NSInteger)row;

+ (BOOL)convertPoint:(CGPoint)point
            toColumn:(NSInteger *)column
                 row:(NSInteger *)row;

+ (BOOL)findPuzzleBlockColumn:(NSInteger *)column
                          row:(NSInteger *)row
                   atPosition:(CGPoint)position
                     forShape:(BlockShape *)shape;

//Miscellaneous
+ (CGFloat)puzzleBoardWidth;
+ (CGFloat)puzzleBoardHeight;
+ (CGFloat)actualPuzzleBoardWidth;
+ (CGFloat)actualPuzzleBoardHeight;
+ (NSUInteger)random:(NSUInteger)max;
+ (CGPoint)point:(CGPoint)point withOffset:(CGPoint)offset;
+ (CGPoint)effectiveOffsetForShape:(BlockShape *)shape;
+ (BOOL)canTheBlock:(BlockShape *)block
        fitAtColumn:(NSUInteger)column
             andRow:(NSUInteger)row
            inScene:(id)scene;
+ (void)markTheShape:(BlockShape *)shape
            atColumn:(NSUInteger)column
              andRow:(NSUInteger)row
            forScene:(id)scene;

+ (void)rows:(NSArray **)rows andColumns:(NSArray **)columns affectedByBlock:(BlockShape *)shape atRow:(NSInteger)row andColumn:(NSInteger)column;

+ (void)handleLineCompletionAtRows:(NSArray*)rows andColumns:(NSArray *)columns forScene:(id)scene;

+ (SKSpriteNode *)getPlayButton;

+ (CIFilter *)blurFilter;

+ (NSString*)getGameDataFilePath;

+ (BlockShapeType)getRandomBlockUntil:(BlockShapeType)blockShape;

+ (void)sendBlockBackToBufferBoardForScene:(id)scene;
+ (void)sendBlockBackToBufferBoardWithAnimationForScene:(id)scene;

CGRect rectSubtract(CGRect r1, CGRect r2, CGRectEdge edge);

+ (UIImage *)takeScreenshotForView:(UIView *)view;
+ (UIImage *)takeScreenshotForView:(UIView *)view isOpaque:(BOOL)isOpaque;

+ (UIColor *)getVariant:(NSUInteger)index
               forColor:(SKColor *)color
           andComponent:(NSUInteger)component;

+ (BOOL)isAMinuteBlock:(SKNode *)node;
+ (NSUInteger)numberOfMinuteBlocksInShape:(BlockShape *)block;

+ (int64_t)getCurrentScore;
+ (int64_t)getHighScore;
+ (NSUInteger)getCurrentLevel;
+ (CGFloat)getCurrentLevelPercentage;

+ (CGFloat)getAbsoluteWidthForPercentage:(CGFloat)widthPercent;
+ (CGFloat)getAbsoluteHeightForPercentage:(CGFloat)heightPercent;

+ (BOOL)isPuzzleBoardEmpty;

+ (NSString *)stringContentsOfFile:(NSString *)filePath
                             error:(NSError **)error;

+ (void)showUserAppRatingsAlertIfRequired;
+ (void)showUserAppRatingsAlertForcefully;

CGFloat widthInPercent(CGFloat widthPercent);
CGFloat heightInPercent(CGFloat heightPercent);
SKColor *getColor(CGFloat red, CGFloat green, CGFloat blue, CGFloat alpha);

#define BoolToString(value) value?@"YES":@"NO"

#define CREATE_LABEL_NODE(node, labelText, labelFontName, labelFontSize, labelFontColor, labelPosition, labelZPosition) \
[SKLabelNode labelNodeWithFontNamed:labelFontName];\
node.text = labelText;\
node.fontSize = labelFontSize;\
node.fontColor = labelFontColor;\
node.position = labelPosition;\
node.zPosition = labelZPosition;

#define CREATE_ANIMATING_LABEL_NODE(node, labelText, labelFontName, labelFontSize, labelFontColor, labelPosition, labelZPosition) \
[AnimatingLabelNode labelNodeWithFontNamed:labelFontName];\
node.text = labelText;\
node.fontSize = labelFontSize;\
node.fontColor = labelFontColor;\
node.position = labelPosition;\
node.zPosition = labelZPosition;

#define CREATE_SCORE_LABEL(node, labelText, labelFontName, labelFontSize, labelFontColor, labelPosition, labelVerticalAlignment, labelZPosition) \
CREATE_LABEL_NODE(node, labelText, labelFontName, labelFontSize, labelFontColor, labelPosition, labelZPosition);\
node.verticalAlignmentMode = labelVerticalAlignment;

#define CREATE_ANIMATING_SCORE_LABEL(node, labelText, labelFontName, labelFontSize, labelFontColor, labelPosition, labelVerticalAlignment, labelZPosition) \
CREATE_ANIMATING_LABEL_NODE(node, labelText, labelFontName, labelFontSize, labelFontColor, labelPosition, labelZPosition);\
node.verticalAlignmentMode = labelVerticalAlignment;

@end

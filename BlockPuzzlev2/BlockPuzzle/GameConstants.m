//
//  GameConstants.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 19/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "GameConstants.h"

//Fonts
NSString *const GameSceneScoreLiveValueFontName = @"Helvetica";
NSString *const GameSceneHighScoreLiveValueFontName = @"Helvetica";
NSString *const GameSceneHiScoreIndicatorFontName = @"AmericanTypewriter-Bold";
NSString *const GameSceneScoreIndicatorFontName = @"AmericanTypewriter-Bold";

NSString *const GameoverScoreLabelFontName = @"Helvetica";
NSString *const GameoverGameoverLabelFontName = @"Helvetica";
NSString *const GameoverHighScoreLabelFontName = @"Helvetica";
NSString *const GameoverLiveValueLabelFontName = @"Helvetica";
NSString *const GameResetAlertTextFontName = @"AmericanTypewriter";
NSString *const InlineScoreAnimationFontName = @"Helvetica-Bold";

//Strings
NSString *const GameoverLabelString = @"Game Over";
NSString *const ScoreLabelString = @"Score";
NSString *const HighScoreLabelString = @"High";

//Info and Opensource credits constants
NSString *const ShowInfoPageSegue = @"ShowInfoPageSegue";
NSString *const ShowOpenSourceCreditsSegue = @"ShowOpenSourceCreditsSegue";
NSString *const OpenSourceCreditsHTMLPage = @"opensourcecredits.html";

//Other Constants
NSString *const MinuteBlockName = @"block";
NSString *const PuzzleBlockShapeName = @"PuzzleBlockShape";
NSString *const PuzzleBackgroundBlockName = @"puzzleBoardBlock";
NSString *const PlayButtonName = @"play";

NSString *const GameDataFileName = @"gamedata";
NSString *const PlayerDeltaScoreDataKey = @"Player.deltaScore";
NSString *const PlayerCurrentScoreDataKey = @"Player.currentScore";
NSString *const PlayerHighScoreDataKey = @"Player.highScore";
NSString *const PlayerPlayerIDDataKey = @"Player.playerID";
NSString *const PlayerDemoStatusKey = @"Player.demoStatus";
NSString *const PlayerManagerAllPlayersDataKey = @"PlayerManager.allPlayers";
NSString *const PlayerManagerCurrentPlayerDataKey = @"PlayerManager.CurrentPlayer";
NSString *const ScoreKeeperHighScoreDataKey = @"ScoreKeeper.highScore";
NSString *const ThemeManagerCurrentThemeDataKey = @"ThemeManager.currentTheme";

NSString *const SoundManagerisSFXEnabledKey = @"SoundManager.isSFXEnabled";
NSString *const SoundManagerisMusicEnabledKey = @"SoundManager.isMusicEnabled";
NSString *const SoundManagerMusicFileKey = @"SoundManager.musicFile";

NSString *const MenuDarkImageName = @"menudark";
NSString *const MenuLightImageName = @"menulight";

NSString *const InlineScoreAnimationLabelPrefix = @"+";

//SFX & Music filenames and keys
NSString *const GameMusicFileName = @"gonefishing.caf";
NSString *const BlocksFallingSFXName = @"Bal in";
NSString *const BlocksClickonSFXName = @"clickon";
NSString *const BlocksLineClearSFXName = @"whoosh type3 04";

NSString *const DemoVideoFileName = @"Blocks Demo Video.mp4";

NSString *const AppleApplicationID = @"1195429337";

//Leaderboard IDs
NSString *const HighScoreLeaderBoardID = @"blocks.highscore";
NSString *const TotalScoreLeaderBoardID = @"blocks.totalscore";

//Score constants
NSString *const AllClearScore = @"AllClearScore";
NSString *const DeltaScore = @"DeltaScore";
NSString *const OvertookHighScore = @"OvertookHighScore";

//Remote Config String Constants
NSString *const RemoteConfigAdsShowTopBannerKey = @"ads_showTopBanner";
NSString *const RemoteConfigAdsInterstitialFrequencyKey = @"ads_interstitialFrequency";

NSString *const RemoteConfigLocalNotificationsScheduleStringKey = @"localNotifications_scheduleString";
NSString *const RemoteConfigLocalNotificationsAlertBodyKey = @"localNotifications_alertBody";
NSString *const RemoteConfigLocalNotificationsAlertTitleKey = @"localNotifications_alertTitle";
NSString *const RemoteConfigLocalNotificationsAlertActionKey = @"localNotifications_alertAction";

//Remote Config Namespaces
NSString *const RemoteConfigAdsNameSpace = @"nameSpace.ads";

//LocalNotification Constants
NSString *const LocalNotificationsFireDate = @"fireDate";
NSString *const LocalNotificationsAlertBody = @"alertBody";
NSString *const LocalNotificationsAlertTitle = @"alertTitle";
NSString *const LocalNotificationsAlertAction = @"alertAction";

//Remote Config Default Values
NSString *const RemoteConfigLocalNotificationsScheduleStringDefaultValue = @"4-1, 4-2, 4-3";
NSString *const RemoteConfigLocalNotificationsAlertTitleDefaultValue = @"Hello";
NSString *const RemoteConfigLocalNotificationsAlertBodyDefaultValue = @"Hello, World!";
NSString *const RemoteConfigLocalNotificationsAlertActionDefaultValue = @"Play";

//Appirater choices
NSString *const AppiraterChoiceRate = @"Rate";
NSString *const AppiraterChoiceDecline = @"Decline";
NSString *const AppiraterChoiceLater = @"Later";

//Themes
NSString *const DarkTheme = @"dark.theme";
NSString *const WhiteTheme = @"white.theme";

//Analytics Constants
NSString *const AppStartDate = @"app_start_date";
NSString *const TimeTakenForLevel = @"level_time_taken";
NSString *const SidebarAction = @"side_bar_action";
NSString *const NumberOfConsecutivePlays = @"number_of_consecutive_plays";
NSString *const AppRatingChoice = @"app_rating_choice";

//Remote Notification Alert constants
NSString *const RemoteNotificationMessageIDKey = @"gcm.message_id";
NSString *const RemoteNotificationTitleKey = @"google.c.a.c_l";
NSString *const RemoteNotificationDefaultTitle = @"Notification";
NSString *const RemoteNotificationMessageKey = @"alert";
NSString *const RemoteNotificationAPSKey = @"aps";
NSString *const RemoteNotificationAlertDefaultActionTitle = @"OK";


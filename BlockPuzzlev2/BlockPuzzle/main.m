//
//  main.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 12/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

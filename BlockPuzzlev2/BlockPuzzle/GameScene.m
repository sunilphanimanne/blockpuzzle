//
//  GameScene.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 12/02/16.
//  Copyright (c) 2016 Sunil Phani Manne. All rights reserved.
//

#import "GameScene.h"

#import "Utils.h"
#import "YCMatrix.h"
#import "GameUtils.h"
#import "BlockShape.h"
#import "ScorePanel.h"
#import "ScoreKeeper.h"
#import "SoundManager.h"
#import "ThemeListener.h"
#import "GameConstants.h"
#import "LevelListener.h"
#import "GameoverOverlay.h"
#import "PhysicsDelegate.h"
#import "AnalyticsLogUtils.h"

#define DEBUG_MODE 0
#define SHOULD_END_GAME_ABRUPTLY 0

static const CGFloat BufferBlockZoomInValue = 1.5;
static const CGFloat BufferBlockZoomOutValue = 1.0;
static const CGFloat BufferBlockZoomInAnimationDuration = 0.1;
static const CGFloat BufferBlockZoomOutAnimationDuration = 0.1;

@interface GameScene () <ScoreListener,
                         LevelListener,
                         ThemeListener>
{
    BOOL _isDarkTheme;
    BOOL _isPuzzleBoardDrawn;
    BOOL _isSceneInitialized;
    NSUInteger _currentLevel;
    BlockShapeType _currentLevelShape;
    PhysicsDelegate *_physicsDelegate;
    TouchController *_touchController;
    SidebarController *_sidebarController;
    NSUInteger _numberOfConsecutivePlays;
}

@property (nonatomic, readwrite) NSUInteger currentLevel;

@end

@implementation GameScene

//Autosynthesize the touchmethods :)
SYNTHESIZE_TOUCH_METHODS_FOR_TOUCH_CONTROLLER(_touchController);

- (id)initWithSize:(CGSize)size {
    if ((self = [super initWithSize:size])) {
        //restrict to single touch...
        self.view.multipleTouchEnabled = NO;
        self.view.exclusiveTouch = YES;
        
        //TODO: Find out why this is happening in the first place.
        [self dummyRenderLabelForScoreAnimation];
    }
    return self;
}

- (void)dummyRenderLabelForScoreAnimation
{
    //Dummy run... Not sure why this is happening, for the first time it is applying heavy load on the game and the FPS is going down. So running a dummy run.
    [self prepareLabelForScore:0 display:NO];
}

- (void)initializeGame
{
    //Some initial housekeeping activities...
    [self performHouseKeepingActivities];
    
    [self setupGame];
}

- (void)setupGame
{
    DDLogDebug(@"GameScene: setupGame:");
    _gameStarted = NO;
    _isSceneInitialized = NO;
    
    //Setup the player
    [self setupPlayer];
    
    //Setup the levels
    [self setupLevels];
    
    //Setup the puzzle matrix
    [self setupPuzzleMatrix];
    
    //Setup the blocks bufferboard UI
    [self setupBlockBufferboard];
    
    //Setup the puzzleboard UI
    [self setupPuzzleBoardUI];
    
    //Setup the scorePanel UI
    [self setupScoreKeeperAndScorePanel];
    
    //Setup themes
    [self setupThemes];
    
    //Setup sidebar
    [self setupSidebarController];
    
    //Setup music
    [self setupGameMusic];
    
    _isSceneInitialized = YES;
}

- (void)setupGameMusic
{
    [self restartMusic];
}

- (void)restartMusic
{
    [[SoundManager sharedManager] playMusic:GameMusicFileName];
}

- (void)setupThemes
{
    if (_themeManager) {
        [_themeManager reset];
    }else  {
        _themeManager = [ThemeManager loadInstance];
    }
    
    [_themeManager subscribeForThemeUpdates:self];
    [_themeManager subscribeForThemeUpdates:_scorePanel];
    
    if (_themeManager.currentTheme) {
        [_themeManager applyTheme:_themeManager.currentTheme
                       completion:^(BOOL isSuccesful) {
                           if (isSuccesful) {
                               _isDarkTheme = _themeManager.currentTheme.isDark;
                           }
                       }
         ];
    }else {
        [self applyLightTheme];
    }
}

- (void)setupSidebarController
{
    _sidebarController = [[SidebarController alloc] init];
}

- (void)setupLevels
{
    DDLogDebug(@"GameScene: setupLevels:");
    _levelManager = [[LevelManager alloc] init];
    [_levelManager subscribeForLevelUpdates:self];
    
    _currentLevel = 0;
    _currentLevelShape = [_levelManager shapeForLevel:_currentLevel];
    DDLogDebug(@"currentLevelShape:%ld", (unsigned long)_currentLevelShape);
}

- (void)setupPuzzleMatrix
{
    DDLogDebug(@"GameScene: setupPuzzleMatrix:");
    //Create an empty matrix
    _puzzleMatrix = [Matrix matrixOfRows:PuzzleRows
                                 columns:PuzzleColumns
                                   value:0];
}

- (void)performHouseKeepingActivities
{
    DDLogDebug(@"GameScene: performHouseKeepingActivities:");
    //Place all the content to the center of the scene
    self.anchorPoint = CGPointMake(0., 0.);
    
    //Create a brand new gestureHandler...
    _touchController = [[TouchController alloc] initWithNode:self];
    
    [self configureTouchLayers];
    
    [self configureTheFence];
    
    [self configureTheMagneticLayer];
    
    _physicsDelegate = [PhysicsDelegate new];
    
    _numberOfFreeBlocks = PuzzleBufferNumberOfBlocks;
    
    _numberOfConsecutivePlays = 0;
}

- (void)setupPlayer
{
    _playerManager = [PlayerManager sharedManager];
}

- (void)setupScoreKeeperAndScorePanel
{
    //create
    _scoreKeeper = [[ScoreKeeper alloc] initWithPlayer:_playerManager.currentPlayer];
    _scorePanel = [[ScorePanel alloc] init];
    
    //initialise ScorePanel
    [_scorePanel setCurrentScore:_scoreKeeper.currentScore
                        animated:NO];
    [_scorePanel setHighScore:_scoreKeeper.highScore animated:NO];
    
    //and Subscribe for updates...
    [_scoreKeeper subscribeForScoreUpdates:self];
    [_scoreKeeper subscribeForScoreUpdates:_scorePanel];
    [_scoreKeeper subscribeForScoreUpdates:_levelManager];
    
    //display
    [self addChild:_scorePanel];
}

- (void)setupPuzzleBoardUI
{
    _puzzleBoardPanel = [SKSpriteNode node];
    _puzzleBoardPanel.anchorPoint = CGPointMake(0, 0);
    _puzzleBoardPanel.size = GetPuzzleBoardSize();
    _puzzleBoardPanel.position = GetPuzzleBoardPosition();
    _puzzleBoardPanel.zPosition = PuzzleBlockBoardZPosition;
    
    [self addChild:_puzzleBoardPanel];

    //drawPuzzleBoardWithBlockColor: is called as a part of applying the theme...
}

- (void)drawPuzzleBoardWithBlockColor:(SKColor *)blockColor
{
    if (!_isPuzzleBoardDrawn) {
        for (NSInteger i=0; i<PuzzleRows; i++) {
            for (NSInteger j=0; j<PuzzleColumns; j++) {
                SKSpriteNode *puzzleBlock = [GameUtils getPuzzleBoardBlockWithColor:blockColor
                                                   ofLength:GetPuzzleBlockLength()];
                puzzleBlock.position = [GameUtils puzzleBoardPointForColumn:j row:i];
                [self addChild:puzzleBlock];
            }
        }
        
        _isPuzzleBoardDrawn = YES;
    }
}

- (void)clearPuzzleBoard
{
    if (_isPuzzleBoardDrawn) {
        for (SKSpriteNode *aBlock in [self children]) {
            if ([aBlock.name isEqualToString:@"puzzleBoardBlock"]) {
                [aBlock removeFromParent];
            }
        }
        
        _isPuzzleBoardDrawn = NO;
    }
}

- (CGPoint)getBlockPositionInBufferBoardAtIndex:(NSUInteger)index forBlock:(BlockShape *)block
{
    CGFloat totalWidth = GetPuzzleBlockBoardSize().width;
    
    CGFloat eachBlockWidth = totalWidth/PuzzleBufferNumberOfBlocks;
    
    CGFloat xOffset = index * eachBlockWidth + (eachBlockWidth - block.size.width)/2 +
                      (GetPuzzleBlockLength() * BlockShapeStateBufferedScale)/2;
    CGFloat yOffset = GetPuzzleBlockBoardSize().height/2 + GetPuzzleBufferBlockYOffset();
    
    return CGPointMake(xOffset, yOffset);
}

- (CGRect)getFrameForBufferBlockTouchLayerAtIndex:(NSUInteger)index
{
    CGFloat totalWidth = GetPuzzleBlockBoardSize().width;
    
    CGFloat eachLayerWidth = totalWidth/PuzzleBufferNumberOfBlocks;
    CGFloat eachLayerHeight = GetPuzzleBlockBoardSize().height +
                              GetMasterBorderSize().height;
    
    CGFloat xOffset = index * eachLayerWidth;
    CGFloat yOffset = GetBottomAdFrame().size.height;
    
    return CGRectMake(xOffset, yOffset, eachLayerWidth, eachLayerHeight);
}

- (void)setupBlockBufferboard
{
    _bufferBoard = [SKSpriteNode node];
    _bufferBoard.anchorPoint = CGPointMake(0, 0);
    _bufferBoard.size = GetPuzzleBlockBoardSize();
    _bufferBoard.position = GetPuzzleBlockBoardPosition();
    
    [self addChild:_bufferBoard];
    
    [self generateBlocksInBuffer];
    
}

- (void)generateBlocksInBuffer
{
    for (NSUInteger i=0; i < PuzzleBufferNumberOfBlocks; i++) {
        [self generateBlockInBufferAtIndex:i];
    }
    
    _numberOfFreeBlocks = PuzzleBufferNumberOfBlocks;
}

- (void)generateBlockInBufferAtIndex:(NSUInteger)index
{
    if (index <= _bufferBlockTouchLayers.count - 1) {
        BlockShape *shape = [BlockShape blockShapeOfType:[GameUtils getRandomBlockUntil:_currentLevelShape]];
        
        DDLogDebug(@"Current Level:%@", @(_currentLevel));
        DDLogDebug(@"Current Level Shape:%@", @(_currentLevelShape));
        DDLogDebug(@"Shape generated:%@", @(shape.type));
        
        shape.originalPosition = [self getBlockPositionInBufferBoardAtIndex:index
                                                                   forBlock:shape];
        shape.position = shape.originalPosition;
        
        //init the action when placed...
        shape.placedAction = ^(BlockShape* shape) {
            _gameStarted = YES;
            [self.scoreKeeper incrementScoreForPlacingBlock:shape];
        };
        
        _bufferBlockTouchLayers[index].shape = shape;
        _bufferBlockTouchLayers[index].index = index;
        [self addChild:shape];
        
        [self animateBlock:shape];
    }
}

- (void)handleAllBoardClear
{
    BOOL isPuzzleBoardEmpty = [GameUtils isPuzzleBoardEmpty];
    DDLogDebug(@"GameScene:isPuzzleBoardEmpty:%@", BoolToString(isPuzzleBoardEmpty));
    
    if (isPuzzleBoardEmpty) {
        [self playAnimationForAllBoardClear];
        [self.scoreKeeper incrementScoreForClearingAllBlocks];
    }
}

- (void)animateBlock:(BlockShape *)blockShape
{
    if (blockShape) {
        SKAction *zoomInAction = [SKAction scaleTo:BufferBlockZoomInValue
                                          duration:BufferBlockZoomInAnimationDuration];
        
        SKAction *zoomOutAction = [SKAction scaleTo:BufferBlockZoomOutValue
                                           duration:BufferBlockZoomOutAnimationDuration];
        
        SKAction *sequence = [SKAction sequence:@[zoomInAction, zoomOutAction]];
        
        [blockShape runAction:sequence];
    }
}

- (void)configureTouchLayers
{
    //buffer block touch layers...
    _bufferBlockTouchLayers = @[[BufferBlockTouchLayer new], [BufferBlockTouchLayer new], [BufferBlockTouchLayer new]];
    
    for (NSUInteger i=0; i < [_bufferBlockTouchLayers count]; i++) {
        _bufferBlockTouchLayers[i].index = i;
        _bufferBlockTouchLayers[i].frame = [self getFrameForBufferBlockTouchLayerAtIndex:i];
        [_touchController subscribeTouchLayer:_bufferBlockTouchLayers[i]];
    }

#if DEBUG_MODE
    [self attachDebugNodesForBufferBlockTouchLayers];
#endif
    
    //master touch layer...
    _masterTouchLayer = [MasterTouchLayer new];
    //TODO: Modify to remove the scorepanel area...
    _masterTouchLayer.frame =  GetScreenRect();
    [_touchController subscribeTouchLayer:_masterTouchLayer];
    
    //scoreboard touch layer...
    _scoreboardTouchLayer = [[ScoreboardTouchLayer alloc] init];
    _scoreboardTouchLayer.frame = GetScoreboardRect();
    [_touchController subscribeTouchLayer:_scoreboardTouchLayer];
}


- (void)attachDebugNodesForBufferBlockTouchLayers
{
    NSArray <SKSpriteNode*> *debugNodes = @[[SKSpriteNode new],
                                            [SKSpriteNode new],
                                            [SKSpriteNode new]];
    NSArray *debugColors = @[[SKColor redColor],
                             [SKColor greenColor],
                             [SKColor blueColor]];
    
    for (NSUInteger i=0; i < [_bufferBlockTouchLayers count]; i++) {
        debugNodes[i].anchorPoint = CGPointMake(0, 0);
        debugNodes[i].color = debugColors[i];
        debugNodes[i].position = _bufferBlockTouchLayers[i].frame.origin;
        debugNodes[i].size = _bufferBlockTouchLayers[i].frame.size;
        debugNodes[i].zPosition = 1;
        [self addChild:debugNodes[i]];
    }
}

- (void)configureTheFence
{
    _blockFence = [[BlockFence alloc] initWithFrame:GetActualPuzzleBoardRect()];
}


- (void)configureTheMagneticLayer
{
    _blockMagneticLayer = [[BlockMagneticLayer alloc] initWithFrame:GetActualPuzzleBoardRect()];
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

- (void)didMoveToView:(SKView *)view
{
    GameContext *gameContext = [GameContext sharedContext];
    
    //Make the scene as currentScene!
    gameContext.currentScene = self;
}

- (BOOL)isGameOver
{
    BOOL isGameOver = NO;
#if SHOULD_END_GAME_ABRUPTLY
    return YES;
#endif
    
    BOOL canAccomodate = NO;
    
    for (BufferBlockTouchLayer *layer in self.bufferBlockTouchLayers) {
        if (layer.shape) {
            canAccomodate = canAccomodate || [self.puzzleMatrix canAccomodate:layer.shape.shapeMatrix];
        }
    }
    
    DDLogDebug(@"GameScene: isGameOver:%@", BoolToString(!canAccomodate));
    
    isGameOver = !canAccomodate;
    
    return isGameOver;
}

- (void)showOverlayScreen:(GameoverOverlay *)gameoverOverlayScreen
{
    if (gameoverOverlayScreen) {
        _gameoverOverlayScreen = gameoverOverlayScreen;
        [self addChild:_gameoverOverlayScreen];
        
        //fade in
        [_gameoverOverlayScreen fadein];
    }
}

- (void)dismissOverlayScreen
{
    [self dismissOverlayScreenWithCompletion:nil];
}


- (void)dismissOverlayScreenWithCompletion:(void (^)())block
{
    DDLogDebug(@"GameScene:dismissOverlayScreenWithCompletion:");
    if (_gameoverOverlayScreen) {
        //fadeout
        [_gameoverOverlayScreen fadeoutWithCompletion:^{
            [_gameoverOverlayScreen removeFromParent];
            _gameoverOverlayScreen = nil;
            if (block) {
                block();
            }
        }];
    }else {
        if (block) {
            block();
        }
    }
}

- (void)disableTheBufferBlocks
{
    for (BufferBlockTouchLayer *blockLayer in [self bufferBlockTouchLayers]) {
        if (blockLayer.shape) {
            blockLayer.shape.state = BlockShapeStateDisabled;
        }
    }
}

- (void)handleGameOver
{
    //check for gameOver...
    if ([self isGameOver]) {
        //Pause the music...
        [[SoundManager sharedManager] pauseMusic];
        
        //Disable the blocks first...
        [self disableTheBufferBlocks];
        
        //Show the overlay screen
        [self showGameoverOverlayAfter:GameoverResetTimeDelayInSeconds];
        
        //Display the AD
        [AdUtils showIntersititalAdAfterDelay:InterstitialAdDisplayDelay
                                        force:YES];
        
        //Display the User App Rating alert
        [GameUtils showUserAppRatingsAlertIfRequired];
    }
}

- (void)showGameoverOverlay
{
    if (!self.gameoverOverlayScreen) {
        GameoverOverlay *gameoverOverlayScreen =
        [[GameoverOverlay alloc] init];
        [self showOverlayScreen:gameoverOverlayScreen];
    }
}

- (void)showGameoverOverlayAfter:(NSTimeInterval)seconds
{
    SKAction *waitAction = [SKAction waitForDuration:seconds];
    [self runAction:waitAction completion:^{
        [self showGameoverOverlay];
    }];
}

- (void)resetGameWithCompletion:(void (^)())block
{
    [self resetGameUIWithCompletion:block];
}

- (void)resetGameUIWithCompletion:(void (^)())block
{
    [self makeTheBlocksFallAndHideWithCompletion:^{
        [self removeAllChildrenExceptOverlayScreenAndBlocks];
        [self removeAllActions];
        
        if (block) {
            block();
        }
    }];
}

- (void)removeAllChildrenExceptOverlayScreenAndBlocks
{
    for (SKNode *node in [self children]) {
        if (![node isEqual:_gameoverOverlayScreen] &&
                ![node.name isEqualToString:MinuteBlockName]) {
            SKAction *removeAction = [SKAction removeFromParent];
            [node runAction:removeAction];
        }
    }
}

- (void)makeTheBlocksFallAndHideWithCompletion:(void (^)())block
{
    [_physicsDelegate setupPhysicsForScene:self allContactsEnded:^{
        DDLogDebug(@"End contacts called...");
        
        if (block) {
            block();
        }
    }];
    
    for (BlockShape *aBlock in [self children]) {
        if ([aBlock.name isEqualToString:MinuteBlockName]) {
            [_physicsDelegate setupPhysicsForBlock:aBlock];
        }
    }
}

- (void)removeAllMinuteBlocks
{
    for (SKNode *node in [self children]) {
        if ([node.name isEqualToString:MinuteBlockName]) {
            SKAction *removeAction = [SKAction removeFromParent];
            [node runAction:removeAction];
        }
    }
}

- (void)restartGame
{
    DDLogInfo(@"*** Game restarted ***");
    DDLogDebug(@"GameScene: restartGame:");
    
    SCENE.scoreboardTouchLayer.touchesEnabled = NO;
    
    [self dismissOverlayScreenWithCompletion:^{
       [self resetGameWithCompletion:^{
           DDLogDebug(@"*** Setting up new Game ***");
           
           //setup a new game...
           [self setupGame];
           
           SCENE.scoreboardTouchLayer.touchesEnabled = YES;
           
           _numberOfConsecutivePlays++;
           
           [AnalyticsLogUtils logNumberOfTimesConsecutivelyPlayed:_numberOfConsecutivePlays];
       }];
    }];
}

#pragma mark - Level related methods

- (void)gameAdvancedToNewLevel:(NSUInteger)newLevel
                      oldLevel:(NSUInteger)oldLevel
                       context:(NSDictionary *)context
{
    self.currentLevel = newLevel;
    
    if (newLevel >= AppiraterLevelUntilPrompt) {
        DDLogInfo(@"GameScene:User has done a Appirater Significant Event!");
        [GameContext sharedContext].didUserFinishSignificantEvent = YES;
    }
    
    [self logTimeTakenForPreviousLevel:oldLevel
                               context:context];
    
    [AnalyticsLogUtils logUserLeveledUp:newLevel];
}

- (void)logTimeTakenForPreviousLevel:(NSUInteger)oldLevel context:(NSDictionary *)context
{
    id timeTakenObject = context[@"timeTakenForPreviousLevel"];
    
    if (timeTakenObject) {
        NSTimeInterval timeTakenForPreviousLevel = [timeTakenObject doubleValue];
        
        [AnalyticsLogUtils logTimeTakenForLevel:timeTakenForPreviousLevel
                                          level:oldLevel];
    }
}

- (void)setCurrentLevel:(NSUInteger)currentLevel
{
    if (_currentLevel != currentLevel) {
        NSUInteger previousLevel = _currentLevel;
        _currentLevel = currentLevel;
        
        //Assumption: Game will ONLY advance...
        if (previousLevel < _currentLevel) {
            [self handleNewLevelUnlocked];
        }
    }
}

- (void)handleNewLevelUnlocked
{
    DDLogInfo(@"***New Level Unlocked:%lu***", (unsigned long)_currentLevel);
    [self unlockGameForNewLevel];
}

- (void)unlockGameForNewLevel
{
    _currentLevelShape = [_levelManager shapeForLevel:_currentLevel];
}

- (void)flipTheme
{
    DDLogInfo(@"***Theme flipped***");
    if (_isDarkTheme) {
        [self applyLightTheme];
    }else {
        [self applyDarkTheme];
    }
    
    [AnalyticsLogUtils logCurrentTheme:_themeManager.currentTheme.name];
}

- (void)applyDarkTheme
{
    [_themeManager applyDarkThemeWithCompletion:^(BOOL isSuccesful) {
        if (isSuccesful) {
            _isDarkTheme = YES;
        }else {
            DDLogError(@"Error: Unable to apply Dark Theme!");
        }
    }];
}

- (void)applyLightTheme
{
    [_themeManager applyLightThemeWithCompletion:^(BOOL isSuccesful) {
        if (isSuccesful) {
            _isDarkTheme = NO;
        }else {
            DDLogError(@"Error: Unable to apply Light Theme!");
        }
    }];
}

-(void)themeApplied:(Theme *)newTheme from:(Theme *)oldTheme
{
    NSTimeInterval duration = ThemeChangeAnimationDuration;
    
    if (!_isSceneInitialized) {
        duration = 0.;
    }
    
    SKAction *fadeOutAction = [SKAction fadeAlphaTo:0 duration:duration];
    SKAction *fadeInAction = [SKAction fadeAlphaTo:1 duration:duration];
    
    [self runAction:fadeOutAction completion:^{
        [self applyColorsOfTheme:newTheme fromOldTheme:oldTheme];
        
        [self runAction:fadeInAction];
    }];
}

- (void)applyColorsOfTheme:(Theme *)newTheme fromOldTheme:(Theme *)oldTheme
{
    NSTimeInterval duration = ThemeChangeAnimationDuration;
    
    if (!_isSceneInitialized) {
        duration = 0.;
    }
    
    self.scorePanel.color = newTheme.scorePanelColor;
    self.puzzleBoardPanel.color = newTheme.puzzleBoardColor;
    self.bufferBoard.color = newTheme.bufferBoardColor;
    
    SKAction *colorizeAction = [SKAction colorizeWithColor:newTheme.masterBackgroundColor
                                          colorBlendFactor:1 duration:duration];
    [self runAction:colorizeAction];
    
    //clear...
    [self clearPuzzleBoard];
    
    //... and redraw!
    [self drawPuzzleBoardWithBlockColor:newTheme.puzzleBoardBlockColor];
    
    if (_isSceneInitialized) {
        [self flipMenu];
    }
}

#pragma mark - Menu related methods

- (void)flipMenu
{
    if (!_isDarkTheme) {
        [self displayDarkMenu];
    }else {
        [self displayLightMenu];
    }
}

- (void)displayLightMenu
{
    [self remove:MenuDarkImageName];
    [self display:MenuLightImageName withAlpha:0.7 andZposition:ThemeLightScoreIconZPosition];
    [self adjustFramesAfterDisplayingMenu];
}

- (void)displayDarkMenu
{
    [self remove:MenuLightImageName];
    [self display:MenuDarkImageName withAlpha:0.7 andZposition:ThemeLightScoreIconZPosition];
    [self adjustFramesAfterDisplayingMenu];
}

- (void)adjustFramesAfterDisplayingMenu
{
    _scoreboardTouchLayer.sidebarMenuFrame = GetSidebarMenuFrame();

#if DEBUG_MODE
    [self attachDebugNodesForMenu];
#endif
    
    DDLogDebug(@"Theme Frame:%@", NSStringFromCGRect(_scoreboardTouchLayer.sidebarMenuFrame));
}

- (void)attachDebugNodesForMenu
{
    NSArray <SKSpriteNode*> *debugNodes = @[[SKSpriteNode new]];
    NSArray *debugColors = @[[SKColor redColor],
                             [SKColor greenColor],
                             [SKColor blueColor]];
    
    for (NSUInteger i=0; i < [debugNodes count]; i++) {
        debugNodes[i].anchorPoint = CGPointMake(0, 0);
        debugNodes[i].color = debugColors[i];
        debugNodes[i].position = _scoreboardTouchLayer.sidebarMenuFrame.origin;
        debugNodes[i].size = _scoreboardTouchLayer.sidebarMenuFrame.size;
        debugNodes[i].zPosition = 1;
        [self addChild:debugNodes[i]];
    }
}

- (void)display:(NSString *)name withAlpha:(CGFloat)alpha andZposition:(CGFloat)zPosition
{
    SKSpriteNode *image = [SKSpriteNode spriteNodeWithImageNamed:name];
    image.name = name;
    CGPoint position = MENU_POSITION;
    position.y -= image.size.height;
    image.position = position;
    
    DDLogDebug(@"Menu Position:%@", NSStringFromCGPoint(position));
    
    image.zPosition = zPosition;
    image.alpha = alpha;
    [self addChild:image];
}

- (void)remove:(NSString *)name
{
    [[self childNodeWithName:name] removeFromParent];
}

- (void)scoreType:(ScoreKeeperScoreType)scoreType
         newValue:(int64_t)newValue oldValue:(int64_t)oldValue
          context:(NSDictionary *)context
{
    if (scoreType == ScoreKeeperScoreTypeScore) {
        if (![self isAllClearScore:context]) {
            [self playPuzzleboardScoreAnimationForScore:newValue
                                               oldScore:oldValue];
        }
        
        [AnalyticsLogUtils logScore:newValue
                              level:_levelManager.currentLevel];
    }
}

- (BOOL)isAllClearScore:(NSDictionary *)context
{
    BOOL isAllClear = NO;
    if (context) {
        isAllClear = [context[AllClearScore] boolValue];
    }
    
    return isAllClear;
}

- (void)playPuzzleboardScoreAnimationForScore:(int64_t)newScore
                                     oldScore:(int64_t)oldScore
{
    int64_t scoreDiff = newScore - oldScore;
    DDLogDebug(@"Play animation for score:%ld", (long)scoreDiff);
    
    SKLabelNode *scoreLabel = [self prepareLabelForScore:scoreDiff];
    
    for (SKAction *action in [self actionsForScoreAnimation]) {
        [scoreLabel runAction:action];
    }
}

- (NSArray *)actionsForScoreAnimation
{
    SKAction *moveAction = [SKAction moveBy:InlineScoreAnimationOffsetVector
                                   duration:InlineScoreAnimationMoveDuration];
    moveAction.timingMode = SKActionTimingEaseInEaseOut;
    
    SKAction *fadeOutAction = [SKAction fadeOutWithDuration:InlineScoreAnimationFadeOutDuration];
    SKAction *removeAction = [SKAction removeFromParent];
    
    SKAction *sequence = [SKAction sequence:@[fadeOutAction,
                                              removeAction]];
    
    return @[moveAction, sequence];
}

- (SKLabelNode *)prepareLabelForScore:(int64_t)score
{
    return [self prepareLabelForScore:score display:YES];
}

- (void)playAnimationForAllBoardClear
{
    CGPoint puzzleBoardPositionCenteredX = GetPuzzleBoardPosition();
    
    puzzleBoardPositionCenteredX.x = GetPuzzleBoardSize().width/2;
    
    SKLabelNode *label = [self prepareLabelForText:@"All clear +50!"
                                        atPosition:puzzleBoardPositionCenteredX
                                           display:YES];
    
    for (SKAction *action in [self actionsForScoreAnimation]) {
        [label runAction:action];
    }
}

- (SKLabelNode *)prepareLabelForText:(NSString *)text atPosition:(CGPoint)position
                             display:(BOOL)display
{
    SKLabelNode *label = CREATE_LABEL_NODE(label,
                                           text,
                                           InlineScoreAnimationFontName,
                                           InlineScoreAnimationFontSize,
                                           _themeManager.currentTheme.gameScoreColor,
                                           position,
                                           InlineScoreAnimationLabelZPosition);
    
    DDLogDebug(@"ScoreLabel Position:%@", NSStringFromCGPoint(label.position));
    
    if (display) {
        [self addChild:label];
    }else {
        label.text = @"";
        [self addChild:label];
        [label removeFromParent];
    }
    
    return label;
}

- (SKLabelNode *)prepareLabelForScore:(int64_t)score display:(BOOL)display
{
    NSString *text = [InlineScoreAnimationLabelPrefix stringByAppendingString:
                      [@(score) description]];
    
    SKLabelNode *scoreLabel = [self prepareLabelForText:text
                                             atPosition:CURRENT_BUFF_BLOCK.position
                                                display:display];
    
    return scoreLabel;
}

@end

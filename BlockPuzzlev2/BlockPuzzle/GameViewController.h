//
//  GameViewController.h
//  BlockPuzzle
//

//  Copyright (c) 2016 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController

- (void)reconfigureAds;

@end

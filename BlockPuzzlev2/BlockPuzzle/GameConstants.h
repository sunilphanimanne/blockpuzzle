//
//  GameConstants.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 19/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

#import "GameUtils.h"
#import "GameContext.h"
#import "AdConstants.h"
#import "DynamicConstants.h"

#ifndef GameConstants_h
#define GameConstants_h

//
//  GameConstants.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 18/02/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#pragma mark - Color Macros

#define PuzzleMainBackgroundColor [SKColor whiteColor]
#define PuzzleBoardColor [SKColor whiteColor]
#define PuzzleBlockColor getColor(220., 220., 220., 0.5)
#define BufferBoardColor [SKColor whiteColor]

#define RestartAlertTextColor [SKColor whiteColor]
#define GameCenterErrorAlertTextColor [SKColor redColor]

//ScorePanel colors
#define ScorePanelColor [SKColor whiteColor]

//Gameover Overlay colors
#define GameoverOverlayScoreColor [SKColor orangeColor]
#define GameoverOverlayHighScoreColor [SKColor yellowColor]
#define GameoverOverlayGameoverLabelColor [SKColor redColor]
#define GameoverOverlayBackgroundColor [SKColor blackColor]
#define GameoverOverlayLiveValueColor [SKColor whiteColor]
#define PuzzleBlockDisabledColor [SKColor blackColor]

//GameScene score colors
#define GameSceneScoreLiveValueColor [SKColor purpleColor]
#define GameSceneScoreIndicatorColor [SKColor purpleColor]
#define GameSceneHighScoreLiveValueColor [SKColor orangeColor]
#define GameSceneHiScoreIndicatorColor [SKColor orangeColor]

#pragma mark - Other Macros

//BlockBoard Macros
#define GetPuzzleBlockBoardSize() [DynamicConstants puzzleBlockBoardSize]
#define GetPuzzleBlockBoardOffset() CGPointMake(0., 0.)
#define GetPuzzleBlockBoardPosition() CGPointMake(GetPuzzleBlockBoardOffset().x , GetPuzzleBlockBoardOffset().y + GetBottomAdFrame().size.height)

//PuzzleBoard Macros
#define GetPuzzleBoardRect() CGRectMake(GetPuzzleBoardOffset().x, GetPuzzleBoardOffset().y, GetPuzzleBoardSize().width, GetPuzzleBoardSize().height)
#define GetActualPuzzleBoardRect() CGRectMake(GetPuzzleBoardPosition().x + GetMasterBorderLength(), GetPuzzleBoardPosition().y + GetMasterBorderLength(), [GameUtils actualPuzzleBoardWidth], [GameUtils actualPuzzleBoardHeight])
#define GetPuzzleBoardSize() CGSizeMake([GameUtils puzzleBoardWidth], [GameUtils puzzleBoardHeight])
#define GetPuzzleBoardOffset() CGPointMake(0, GetPuzzleBlockBoardPosition().y + GetPuzzleBlockBoardSize().height)
#define GetPuzzleBoardPosition() CGPointMake(GetPuzzleBoardOffset().x, GetPuzzleBoardOffset().y)

//Generic Macros
#define GetScreenSize() [[UIScreen mainScreen] bounds].size
#define GetMasterBorderLength() [DynamicConstants masterBoarderLength]

#define GetMasterBorderSize() CGSizeMake(GetMasterBorderLength(), GetMasterBorderLength())
#define GetBufferBlockOffset() CGPointMake(-1, 50)
#define GetScreenRect() CGRectMake(0, 0, GetScreenSize().width, GetScreenSize().height)
#define CENTERX CGRectGetMidX(GetScreenRect())
#define CENTERY CGRectGetMidY(GetScreenRect())

#define DummyPoint CGPointMake(NSNotFound, NSNotFound)

#define SCENE ((GameScene *)[GameContext sharedContext].currentScene)
#define CURRENT_BUFF_BLOCK SCENE.currentBufferedBlock
#define CURRENT_BUFF_BLOCK_INDEX SCENE.currentBufferedBlockIndex

//Scoreboard Macros
#define GetScoreboardOffset() CGPointMake(0, GetPuzzleBoardPosition().y + GetPuzzleBoardSize().height)
#define GetScoreboardSize() CGSizeMake(GetScreenSize().width, GetScreenSize().height - (GetPuzzleBoardSize().height + GetPuzzleBlockBoardSize().height))
#define GetScoreboardPosition() CGPointMake(GetScoreboardOffset().x, GetScoreboardOffset().y)
#define GetScoreboardRect() CGRectMake(GetScoreboardPosition().x, GetScoreboardPosition().y, GetScoreboardSize().width, GetScoreboardSize().height)
#define SCORE_LABEL_POSITION CGPointMake(CGRectGetMidX(GetScoreboardRect()), CGRectGetMidY(GetScoreboardRect()))
#define HISCORE_LABEL_POSITION CGPointMake(CGRectGetMidX(GetScoreboardRect()), CGRectGetMidY(GetScoreboardRect()))
#define MENU_POSITION [DynamicConstants sidebarMenuPosition]

#define GetSidebarMenuSize() [DynamicConstants sidebarMenuSize]
#define GetSidebarMenuFrame() CGRectMake(0, GetScreenSize().height - GetSidebarMenuSize().height, GetSidebarMenuSize().width , GetSidebarMenuSize().height)

#define InlineScoreAnimationOffsetVector CGVectorMake(0, 150)

#define GetPuzzleBlockLength() [DynamicConstants puzzleBlockLength]

#define LOCALPLAYER [GKLocalPlayer localPlayer]

#define GetMovingPuzzleBlockPadding() [DynamicConstants movingPuzzleBlockPadding]

#define GetScoreIndicatorFontSize() [DynamicConstants scoreIndicatorFontSize]
#define GetScoreLiveValueFontSize() [DynamicConstants scoreLiveValueFontSize]
#define GetHiScoreLiveValueFontSize() [DynamicConstants hiScoreLiveValueFontSize]
#define GetGameRestartAlertFontSize() [DynamicConstants gameRestartAlertFontSize]

#define GetPuzzleBufferBlockYOffset() [DynamicConstants puzzleBufferBlockYOffset]

#define GetGameMusicVolume() [DynamicConstants gameMusicVolume]

#define GetSidebarItemSize() [DynamicConstants itemSizeForSidebar]

#define GetScoreLabelTopPadding() [DynamicConstants scoreLabelTopPadding]

#define GetHiScoreLabelBottomPadding() [DynamicConstants hiScoreLabelBottomPadding]

#define GetScoreIndicatorTopPadding() [DynamicConstants scoreIndicatorTopPadding]

#pragma mark - Constants

static const NSInteger PuzzleColumns = 10;
static const NSInteger PuzzleRows = 10;
static const CGFloat PuzzleBlockPadding = 2.0;
static const NSInteger PuzzleBufferNumberOfBlocks = 3;

static const CGFloat BlockShapeStateBufferedScale = 0.7;
static const CGFloat BlockShapeStateDisabledScale = BlockShapeStateBufferedScale;
static const CGFloat BlockShapeStateMovingScale = 0.9;
static const CGFloat BlockShapeStatePlacedScale = 1.0;
static const CGFloat BlockShapeTransientScale = 1.2;

static const CGFloat BlockShapeTransientScaleAnimateDuration = 0.1;
static const CGFloat BlockShapeReturnTransientScaleAnimateDuration = 0.1;
static const CGFloat BlockRestorePositionAnimationDuration = 0.5;

static const CGFloat LineFinishedAnimationDuration = 0.25;
static const CGFloat LineFinishedScale = 0;
static const SKActionTimingMode LineFinishedAnimationTiming = SKActionTimingEaseInEaseOut;

static const CGFloat BlockShapeStateBufferedOffset = 0;
static const CGFloat BlockShapeStateDisabledOffset = 0;
static const CGFloat BlockShapeStatePlacedOffset = 0;
static const CGFloat BlockShapeStateMovingOffset = 0;

static const CGFloat GameoverOverlayScoreFontSize = 25;
static const CGFloat GameoverOverlayHighScoreFontSize = 25;
static const CGFloat GameoverOverlayLiveValueFontSize = 25;
static const CGFloat GameoverOverlayGameoverFontSize = 60;

static const CGFloat GameRestartAlertFontSize = 40;
static const CGFloat GameRestartAlertFontSize4S = 30;

static const CGFloat LeaderBoardAlertFontSize = 19;
static const CGFloat LeaderBoardAlertFontSize4S = 17;

static const CGFloat GameResetAlertAlpha = 0.8;
static const CGFloat GameoverOverlayInitalAlpha = 0.7;
static const CGFloat GameoverOverlayFinalAlpha = 0.9;
static const CGFloat GameoverOverlayDismissAlpha = 0.0;
static const CGFloat GameoverOverlayAlphaFadeAnimationDuration = 0.5;

static const CGFloat GameSceneScoreLiveValueFontSize = 55;
static const CGFloat GameSceneHighScoreLiveValueFontSize = 35;
static const CGFloat GameSceneScoreIndicatorFontSize = 30;
static const CGFloat InlineScoreAnimationFontSize = 35;

static const CGFloat GameSceneScoreLiveValueFontSize4S = 45;
static const CGFloat GameSceneHighScoreLiveValueFontSize4S = 25;
static const CGFloat GameSceneScoreIndicatorFontSize4S = 20;

static const CGFloat GameSceneScoreboardZPosition = -3;
static const CGFloat GameSceneScoreLabelZPosition = 2;
static const CGFloat GameSceneHiScoreIndicatorZPosition = 1;
static const CGFloat GameSceneScoreIndicatorZPosition = 1;
static const CGFloat GameoverOverlayZPosition = 0;
static const CGFloat PuzzleBoardZPosition = 2;
static const CGFloat PuzzleBlockBoardZPosition = -3;
static const CGFloat BlockShapeDefaultZPosition = 3;
static const CGFloat BlockShapeMovingZPosition = 4;
static const CGFloat BlockShapePlacedZPosition = 1;
static const CGFloat BlockShapeDisabledZPosition = 0;
static const CGFloat PuzzleBackgroundBlockZPosition = -2;
static const CGFloat InlineScoreAnimationLabelZPosition = 5;

static const CGFloat BufferBlockTouchLayerZPosition = 2;
static const CGFloat PlayButtonTouchLayerZPosition = 2;
static const CGFloat ScoreBoardTouchLayerZPosition = 3;

static const CGFloat ThemeLightScoreIconZPosition = 2;

static const NSInteger GameoverResetTimeDelayInSeconds = 1;

static const CGFloat ThemeChangeAnimationDuration = 0.2;

static const CGFloat InlineScoreAnimationMoveDuration = 0.5;
static const CGFloat InlineScoreAnimationFadeOutDuration = 0.5;

static const NSInteger AppiraterDaysUntilPrompt = 1;
static const NSInteger AppiraterUsesUntilPrompt = 1;
static const NSInteger AppiraterLevelUntilPrompt = 2;
static const NSInteger AppiraterSignificantEventsUntilPrompt = -1;
static const NSInteger AppiraterTimeBeforeRemindingInDays = 1;

//Remote Config Default Values
static const BOOL RemoteConfigAdsShowTopBannerDefaultValue = YES;
static const NSInteger RemoteConfigAdsInterstitialFrequencyDefaultValue = 4;

FOUNDATION_EXPORT NSString *const RemoteConfigLocalNotificationsScheduleStringDefaultValue;
FOUNDATION_EXPORT NSString *const RemoteConfigLocalNotificationsAlertBodyDefaultValue;
FOUNDATION_EXPORT NSString *const RemoteConfigLocalNotificationsAlertTitleDefaultValue;
FOUNDATION_EXPORT NSString *const RemoteConfigLocalNotificationsAlertActionDefaultValue;

//Strings

//Remote Config String Constants
FOUNDATION_EXPORT NSString *const RemoteConfigAdsShowTopBannerKey;
FOUNDATION_EXPORT NSString *const RemoteConfigAdsInterstitialFrequencyKey;
FOUNDATION_EXPORT NSString *const RemoteConfigLocalNotificationsScheduleStringKey;
FOUNDATION_EXPORT NSString *const RemoteConfigLocalNotificationsAlertBodyKey;
FOUNDATION_EXPORT NSString *const RemoteConfigLocalNotificationsAlertTitleKey;
FOUNDATION_EXPORT NSString *const RemoteConfigLocalNotificationsAlertActionKey;

FOUNDATION_EXPORT NSString *const ScoreLabelString;
FOUNDATION_EXPORT NSString *const GameoverLabelString;
FOUNDATION_EXPORT NSString *const HighScoreLabelString;

//LocalNotification Constants
FOUNDATION_EXPORT NSString *const LocalNotificationsFireDate;
FOUNDATION_EXPORT NSString *const LocalNotificationsAlertBody;
FOUNDATION_EXPORT NSString *const LocalNotificationsAlertTitle;
FOUNDATION_EXPORT NSString *const LocalNotificationsAlertAction;

//Font Names
FOUNDATION_EXPORT NSString *const GameSceneScoreLiveValueFontName;
FOUNDATION_EXPORT NSString *const GameSceneScoreIndicatorFontName;
FOUNDATION_EXPORT NSString *const GameSceneHighScoreLiveValueFontName;
FOUNDATION_EXPORT NSString *const GameSceneHiScoreIndicatorFontName;

FOUNDATION_EXPORT NSString *const GameResetAlertTextFontName;
FOUNDATION_EXPORT NSString *const GameoverScoreLabelFontName;
FOUNDATION_EXPORT NSString *const GameoverGameoverLabelFontName;
FOUNDATION_EXPORT NSString *const GameoverHighScoreLabelFontName;
FOUNDATION_EXPORT NSString *const GameoverLiveValueLabelFontName;

//Opensource credits constants
FOUNDATION_EXPORT NSString *const ShowInfoPageSegue;
FOUNDATION_EXPORT NSString *const ShowOpenSourceCreditsSegue;
FOUNDATION_EXPORT NSString *const OpenSourceCreditsHTMLPage;

//Other constants
FOUNDATION_EXPORT NSString *const PlayButtonName;
FOUNDATION_EXPORT NSString *const MinuteBlockName;
FOUNDATION_EXPORT NSString *const PuzzleBlockShapeName;
FOUNDATION_EXPORT NSString *const PuzzleBackgroundBlockName;

FOUNDATION_EXPORT NSString *const GameDataFileName;
FOUNDATION_EXPORT NSString *const PlayerPlayerIDDataKey;
FOUNDATION_EXPORT NSString *const PlayerDeltaScoreDataKey;
FOUNDATION_EXPORT NSString *const PlayerCurrentScoreDataKey;
FOUNDATION_EXPORT NSString *const PlayerHighScoreDataKey;
FOUNDATION_EXPORT NSString *const PlayerDemoStatusKey;

FOUNDATION_EXPORT NSString *const ScoreKeeperHighScoreDataKey;
FOUNDATION_EXPORT NSString *const ThemeManagerCurrentThemeDataKey;
FOUNDATION_EXPORT NSString *const PlayerManagerAllPlayersDataKey;
FOUNDATION_EXPORT NSString *const PlayerManagerCurrentPlayerDataKey;

FOUNDATION_EXPORT NSString *const SoundManagerisSFXEnabledKey;
FOUNDATION_EXPORT NSString *const SoundManagerisMusicEnabledKey;
FOUNDATION_EXPORT NSString *const SoundManagerMusicFileKey;

FOUNDATION_EXPORT NSString *const MenuDarkImageName;
FOUNDATION_EXPORT NSString *const MenuLightImageName;

FOUNDATION_EXPORT NSString *const InlineScoreAnimationFontName;
FOUNDATION_EXPORT NSString *const InlineScoreAnimationLabelPrefix;

FOUNDATION_EXPORT NSString *const GameMusicFileName;
FOUNDATION_EXPORT NSString *const BlocksLineClearSFXName;
FOUNDATION_EXPORT NSString *const BlocksFallingSFXName;
FOUNDATION_EXPORT NSString *const BlocksClickonSFXName;

FOUNDATION_EXPORT NSString *const DemoVideoFileName;

FOUNDATION_EXPORT NSString *const AppleApplicationID;

//Leaderboard IDs
FOUNDATION_EXPORT NSString *const HighScoreLeaderBoardID;
FOUNDATION_EXPORT NSString *const TotalScoreLeaderBoardID;

//Score Constants
FOUNDATION_EXPORT NSString *const AllClearScore;
FOUNDATION_EXPORT NSString *const DeltaScore;
FOUNDATION_EXPORT NSString *const OvertookHighScore;

//Remote Config Namespaces
FOUNDATION_EXPORT NSString *const RemoteConfigAdsNameSpace;

//Appirater choices
FOUNDATION_EXPORT NSString *const AppiraterChoiceRate;
FOUNDATION_EXPORT NSString *const AppiraterChoiceDecline;
FOUNDATION_EXPORT NSString *const AppiraterChoiceLater;

//Themes
FOUNDATION_EXPORT NSString *const DarkTheme;
FOUNDATION_EXPORT NSString *const WhiteTheme;

//Analytics Log events
FOUNDATION_EXPORT NSString *const AppStartDate;
FOUNDATION_EXPORT NSString *const TimeTakenForLevel;
FOUNDATION_EXPORT NSString *const SidebarAction;
FOUNDATION_EXPORT NSString *const NumberOfConsecutivePlays;
FOUNDATION_EXPORT NSString *const AppRatingChoice;

//Remote Notification Alert constants
FOUNDATION_EXPORT NSString *const RemoteNotificationMessageIDKey;
FOUNDATION_EXPORT NSString *const RemoteNotificationTitleKey;
FOUNDATION_EXPORT NSString *const RemoteNotificationDefaultTitle;
FOUNDATION_EXPORT NSString *const RemoteNotificationMessageKey;
FOUNDATION_EXPORT NSString *const RemoteNotificationAPSKey;
FOUNDATION_EXPORT NSString *const RemoteNotificationAlertDefaultActionTitle;

#pragma mark -

//ScoreKeeperScoreType
typedef NS_ENUM(NSUInteger, ScoreKeeperScoreType) {
    ScoreKeeperScoreTypeScore,
    ScoreKeeperScoreTypeHighScore
};

//GameCenterDisplayType
typedef NS_ENUM(NSUInteger, GameCenterDisplayType) {
    GameCenterDisplayTypeLeaderBoard,
    GameCenterDisplayTypeAchievements
};

#endif /* GameConstants_h */

//
//  BlockFence.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 08/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "BlockFence.h"

#import "GameUtils.h"
#import "GameConstants.h"

@implementation BlockFence

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super init]) {
        _frame = frame;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andScene:(id)scene
{
    self = [self initWithFrame:frame];
    
    return self;
}

- (SKShapeNode *)shapeNodeWithRect:(CGRect)frame
{
    SKShapeNode *shape = [SKShapeNode shapeNodeWithRect:frame];
    CGPathRef pathRef = CGPathCreateWithRoundedRect(CGRectMake(frame.origin.x,
                                                               frame.origin.y,
                                                               frame.size.width,
                                                               frame.size.height),
                                                    4, 4, nil);
    
    [shape setPath:pathRef];
    CGPathRelease(pathRef);
    
    shape.lineWidth = 4.;
    shape.strokeColor = getColor(0, 128, 255, 1);
    return shape;
}

- (BOOL)isPointInsideTheFence:(CGPoint)point
{
    BOOL isInside = NO;
    
    isInside = CGRectContainsPoint(self.frame, point);
    
    return isInside;
}

- (BOOL)areAnyPointsInsideTheFence:(CGPoint)corner1 corner2:(CGPoint)corner2 corner3:(CGPoint)corner3 corner4:(CGPoint)corner4
{
    BOOL areInside = NO;
    
    areInside = [self isPointInsideTheFence:corner1] || [self isPointInsideTheFence:corner2] ||
                [self isPointInsideTheFence:corner3] || [self isPointInsideTheFence:corner4];
    
    return areInside;
}

- (BOOL)isPointInsideTheFence:(CGPoint)point forShape:(BlockShape *)shape
{
    BOOL isInside = NO;
    
    if (point.x >= self.frame.origin.x - GetPuzzleBlockLength()/2 &&
        point.x <= self.frame.origin.x + self.frame.size.width - shape.size.width + GetPuzzleBlockLength()/2 &&
        point.y >= self.frame.origin.y - GetPuzzleBlockLength()/2 &&
        point.y <= self.frame.origin.y + self.frame.size.height - shape.size.height + GetPuzzleBlockLength()/2) {
        isInside = YES;
        DDLogDebug(@"Inside:%@", BoolToString(isInside));
    }
    
    return isInside;
}

@end

//
//  MasterTouchLayer.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 08/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "MasterTouchLayer.h"

#import "math.h"
#import "Utils.h"
#import "GameUtils.h"
#import "GameScene.h"
#import "BlockFence.h"
#import "ScoreKeeper.h"
#import "GameConstants.h"
#import "GameoverOverlay.h"

#define GENERATE_ALL_AT_ONCE 0

@implementation MasterTouchLayer

NSInteger _currentAttractedColumn;
NSInteger _currentAttractedRow;
CGPoint _attractedPoint;

- (instancetype)init
{
    if (self = [super init]) {
        _currentAttractedColumn = NSNotFound;
        _currentAttractedRow = NSNotFound;
        _attractedPoint = DummyPoint;
    }
    
    return self;
}

- (void)preTouchesHandlerForTouchesAtPoint:(CGPoint)point
{
    CGPoint effectivePoint = [GameUtils point:point
                                   withOffset:[GameUtils effectiveOffsetForShape:CURRENT_BUFF_BLOCK]];
    
    BOOL isInsideTheFence = [SCENE.blockFence isPointInsideTheFence:effectivePoint forShape:CURRENT_BUFF_BLOCK];
    
    if (isInsideTheFence) {
        SCENE.blockMagneticLayer.isMagneticEffectOn = YES;
    }else {
        SCENE.blockMagneticLayer.isMagneticEffectOn = NO;
    }
    
    DDLogDebug(@"Magnetic Effect:%@", BoolToString(SCENE.blockMagneticLayer.isMagneticEffectOn));
}

- (void)touchesMovedAtPoint:(CGPoint)point
{
    DDLogDebug(@"Entered Moved method...");
    
    if (!CURRENT_BUFF_BLOCK || CURRENT_BUFF_BLOCK.state == BlockShapeStatePlaced) {
        DDLogDebug(@"Returning...");
        return;
    }
    
    CGPoint currentPoint = [GameUtils point:point
                                 withOffset:[GameUtils effectiveOffsetForShape:CURRENT_BUFF_BLOCK]];

    if (CURRENT_BUFF_BLOCK.state == BlockShapeStateDisabled ||
        CURRENT_BUFF_BLOCK.state == BlockShapeStateDisabledMoving) {
        CURRENT_BUFF_BLOCK.state = BlockShapeStateDisabledMoving;
        CURRENT_BUFF_BLOCK.position = currentPoint;
        return;
    }
    
    if ([SCENE.blockMagneticLayer findAttractingBlockPosition:&_attractedPoint
                                                       column:&_currentAttractedColumn
                                                       andRow:&_currentAttractedRow
                                                     forPoint:currentPoint]) {
        
        if (!CGPointEqualToPoint(_attractedPoint, DummyPoint)) {
            BOOL canFit = [GameUtils canTheBlock:CURRENT_BUFF_BLOCK
                                     fitAtColumn:_currentAttractedColumn
                                          andRow:_currentAttractedRow
                                         inScene:SCENE];
            DDLogDebug(@"Canfit:%@", BoolToString(canFit));
            
            if (canFit) {
                DDLogDebug(@"Mom. placing...");
                CURRENT_BUFF_BLOCK.state = BlockShapeStateMomentarilyPlaced;
                CURRENT_BUFF_BLOCK.position = CGPointMake(_attractedPoint.x - CURRENT_BUFF_BLOCK.offset,
                                                          _attractedPoint.y - CURRENT_BUFF_BLOCK.offset);
            }else {
                DDLogDebug(@"Moving...");
                CURRENT_BUFF_BLOCK.state = BlockShapeStateMoving;
                CURRENT_BUFF_BLOCK.position = currentPoint;
            }
        }else {
            //If no proper attracted point was found...
            CURRENT_BUFF_BLOCK.position = currentPoint;
        }
    }else {
        DDLogDebug(@"Elseeeeeee part3");
        CURRENT_BUFF_BLOCK.position = currentPoint;
        _attractedPoint = DummyPoint;
        _currentAttractedRow = NSNotFound;
        _currentAttractedColumn = NSNotFound;
    }
}

- (void)touchesEndedAtPoint:(CGPoint)point
{
    if (!CURRENT_BUFF_BLOCK) {
        return;
    }
    
    BOOL shouldSendBlockToBufferBoard = NO;
    
    if (SCENE.blockMagneticLayer.isMagneticEffectOn) {
        if (CURRENT_BUFF_BLOCK.state == BlockShapeStateMomentarilyPlaced) {
            DDLogDebug(@"Placing...");
            
            if (!CGPointEqualToPoint(_attractedPoint, DummyPoint)) {
                BOOL canFit = [GameUtils canTheBlock:CURRENT_BUFF_BLOCK
                                         fitAtColumn:_currentAttractedColumn
                                              andRow:_currentAttractedRow
                                             inScene:SCENE];
                if (canFit) {
                    
                    CURRENT_BUFF_BLOCK.state = BlockShapeStatePlaced;
                    
                    NSArray *rows = nil;
                    NSArray *cols = nil;
                    
                    [GameUtils markTheShape:CURRENT_BUFF_BLOCK
                                   atColumn:_currentAttractedColumn
                                     andRow:_currentAttractedRow
                                   forScene:SCENE];
                    
                    [GameUtils rows:&rows
                         andColumns:&cols
                    affectedByBlock:CURRENT_BUFF_BLOCK
                              atRow:_currentAttractedRow
                          andColumn:_currentAttractedColumn];
                    
                    DDLogDebug(@"Rows affected:%@", rows);
                    DDLogDebug(@"Cols affected:%@", cols);
                    
                    [CURRENT_BUFF_BLOCK addShapeBlocksToNode:SCENE atOffset:CGPointMake(_attractedPoint.x - CURRENT_BUFF_BLOCK.offset,
                         _attractedPoint.y - CURRENT_BUFF_BLOCK.offset)];
                    
                    [GameUtils handleLineCompletionAtRows:rows
                                               andColumns:cols
                                                 forScene:SCENE];
                    
                    if (CURRENT_BUFF_BLOCK.state == BlockShapeStatePlaced) {
                        CURRENT_BUFF_BLOCK.state = BlockShapeStateNotMerged;
                    }
                    
                    CURRENT_BUFF_BLOCK = nil;
                    SCENE.bufferBlockTouchLayers[CURRENT_BUFF_BLOCK_INDEX].shape = nil;
                    SCENE.bufferBlockTouchLayers[CURRENT_BUFF_BLOCK_INDEX].index = -1;
                    
                    SCENE.numberOfFreeBlocks--;

#if GENERATE_ALL_AT_ONCE
                    if (!SCENE.numberOfFreeBlocks) {
                        [SCENE generateBlocksInBuffer];
                    }
#else
                    [SCENE generateBlockInBufferAtIndex:CURRENT_BUFF_BLOCK_INDEX];
#endif
                    
                    CURRENT_BUFF_BLOCK_INDEX = -1;
                    
                    [SCENE handleGameOver];
                }else {
                    shouldSendBlockToBufferBoard = YES;
                }
            }else {
                shouldSendBlockToBufferBoard = YES;
            }
        }else if (CURRENT_BUFF_BLOCK.state == BlockShapeStateMoving){
            shouldSendBlockToBufferBoard = YES;
        }else if (CURRENT_BUFF_BLOCK.state == BlockShapeStateDisabledMoving) {
            shouldSendBlockToBufferBoard = YES;
        }else if (CURRENT_BUFF_BLOCK.state == BlockShapeStatePlaced){
            DDLogError(@"UNHANDLED ERROR: BlockShapeStatePlaced");
        }else if (CURRENT_BUFF_BLOCK.state == BlockShapeStateBuffered) {
            DDLogError(@"UNHANDLED ERROR: BlockShapeStateBuffered");
        }
    }else {
        shouldSendBlockToBufferBoard = YES;
    }
    
    if (shouldSendBlockToBufferBoard) {
        [GameUtils sendBlockBackToBufferBoardWithAnimationForScene:SCENE];
    }
}

- (void)touchesOutOfRangeAtPoint:(CGPoint)point
{
    [GameUtils sendBlockBackToBufferBoardWithAnimationForScene:SCENE];
}

@end

//
//  AnalyticsLogUtils.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 04/04/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnalyticsLogUtils : NSObject

+ (void)logOpenAppEvent;
+ (void)logCurrentTheme:(NSString *)themeName;
+ (void)logAppStartDate:(NSDate *)date;
+ (void)logUserLeveledUp:(NSUInteger)level;
+ (void)logTimeTakenForLevel:(NSTimeInterval)time level:(NSUInteger)level;
+ (void)logSidebarAction:(NSString *)action;
+ (void)logScore:(int64_t)score level:(int64_t)level;
+ (void)logNumberOfTimesConsecutivelyPlayed:(NSUInteger)noOfTimes;
+ (void)logAppiraterChoice:(NSString *)choiceString;

@end

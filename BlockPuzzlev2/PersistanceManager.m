//
//  PersistanceManager.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 08/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "PersistanceManager.h"

#import "ScoreKeeper.h"

@implementation PersistanceManager

+ (void)saveObject:(id)object forKey:(NSString *)key
{
    NSData* encodedData = [NSKeyedArchiver archivedDataWithRootObject:object];
    [[NSUserDefaults standardUserDefaults] setObject:encodedData forKey:key];
}

+ (id)retrieveObjectForKey:(NSString *)key
{
    id object = nil;
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    if (data) {
        object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    return object;
}

+ (id)loadInstanceForClass:(Class)class andKey:(NSString *)key
{
    //Try to retrieve it from the persistence store...
    id object = [PersistanceManager retrieveObjectForKey:key];
    
    //...if not, create new one!
    if (!object) {
        object = [[class alloc] init];
    }
    
    return object;
}

@end

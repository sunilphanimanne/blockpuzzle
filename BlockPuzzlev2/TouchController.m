//
//  TouchController.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "TouchController.h"

#import "GameConstants.h"
#import "TouchLayerProtocol.h"

@interface TouchController ()
{
    SKNode *_node;
    NSMutableArray *_frames;
    NSMutableArray *_touchLayers;
    NSArray *_zOrderedTouchLayers;
    NSSet<UITouch *> *_previousTouches;
    NSMutableArray *_activeTouchLayers;
}

@end

@implementation TouchController

#define BYPASS_TOUCHES_IFREQUIRED() \
if (self.shouldBypassTouches) { \
    DDLogDebug(@"Bypassing..."); \
    return; \
} \

#define COMPUTE_POINT_AND_TREND() \
CGPoint pointInNode = [self pointInNodeForTouches:touches];\
TouchTrend trend = [self analyseTouchTrend:touches];\

//Handle mutually exclusive touch
#define HANDLE_MUTUAL_EXCLUSIVITY(touchLayer)\
    if (touchLayer.isMutuallyExclusive) {\
        DDLogDebug(@"Breaking for Mutual Exclusivity...");\
        break;\
    }\

// Handle PreTouches Macro
#define HANDLE_PRETOUCHES(touchLayer, point, trend) \
\
if ([touchLayer respondsToSelector:@selector(preTouchesHandlerForTouchesAtPoint:)]) {\
    [touchLayer preTouchesHandlerForTouchesAtPoint:point];\
}\
\
if ([touchLayer respondsToSelector:@selector(preTouchesHandlerForTouchesAtPoint:withTouchTrend:)]) {\
    [touchLayer preTouchesHandlerForTouchesAtPoint:point withTouchTrend:trend];\
}\
//End of PreTouches

// Handle PostTouches Macro
#define HANDLE_POSTTOUCHES(touchLayer, point, trend) \
\
if ([touchLayer respondsToSelector:@selector(postTouchesHandlerForTouchesAtPoint:)]) {\
[touchLayer postTouchesHandlerForTouchesAtPoint:point];\
}\
\
if ([touchLayer respondsToSelector:@selector(postTouchesHandlerForTouchesAtPoint:withTouchTrend:)]) {\
[touchLayer postTouchesHandlerForTouchesAtPoint:point withTouchTrend:trend];\
}\
//End of PostTouches

//Touches Began Macro
#define FORWARD_TOUCHESBEGAN_TOUCHPOINT_AND_TREND(touchLayer, point, trend) \
if (currentTouchLayer.touchesEnabled) {\
    HANDLE_PRETOUCHES(touchLayer, point, trend)\
\
if ([touchLayer respondsToSelector:@selector(touchesBeganAtPoint:)]) {\
    [touchLayer touchesBeganAtPoint:point];\
    HANDLE_MUTUAL_EXCLUSIVITY(touchLayer);\
}\
\
if ([touchLayer respondsToSelector:@selector(touchesBeganAtPoint:withTouchTrend:)]) {\
    [touchLayer touchesBeganAtPoint:point withTouchTrend:trend];\
    HANDLE_MUTUAL_EXCLUSIVITY(touchLayer);\
}\
\
    HANDLE_POSTTOUCHES(touchLayer, point, trend)\
}\
//End of Touches Began Macro


//Touches Moved Macro
#define FORWARD_TOUCHESMOVED_TOUCHPOINT_AND_TREND(touchLayer, point, trend) \
if (currentTouchLayer.touchesEnabled) {\
    HANDLE_PRETOUCHES(touchLayer, point, trend)\
\
if ([touchLayer respondsToSelector:@selector(touchesMovedAtPoint:)]) {\
    [touchLayer touchesMovedAtPoint:point];\
    HANDLE_MUTUAL_EXCLUSIVITY(touchLayer);\
}\
\
if ([touchLayer respondsToSelector:@selector(touchesMovedAtPoint:withTouchTrend:)]) {\
    [touchLayer touchesMovedAtPoint:point withTouchTrend:trend];\
    HANDLE_MUTUAL_EXCLUSIVITY(touchLayer);\
}\
\
    HANDLE_POSTTOUCHES(touchLayer, point, trend)\
}\
//End of Touches Moved Macro


//Touches Ended Macro
#define FORWARD_TOUCHESENDED_TOUCHPOINT_AND_TREND(touchLayer, point, trend) \
if (currentTouchLayer.touchesEnabled) {\
    HANDLE_PRETOUCHES(touchLayer, point, trend)\
\
if ([touchLayer respondsToSelector:@selector(touchesEndedAtPoint:)]) {\
    [touchLayer touchesEndedAtPoint:point];\
    HANDLE_MUTUAL_EXCLUSIVITY(touchLayer);\
}\
\
if ([touchLayer respondsToSelector:@selector(touchesEndedAtPoint:withTouchTrend:)]) {\
    [touchLayer touchesEndedAtPoint:point withTouchTrend:trend];\
    HANDLE_MUTUAL_EXCLUSIVITY(touchLayer);\
}\
\
    HANDLE_POSTTOUCHES(touchLayer, point, trend)\
}\
//End of Touches Ended Macro

//Touches Cancelled Macro
#define FORWARD_TOUCHESCANCELLED_TOUCHPOINT_AND_TREND(touchLayer, point, trend) \
if (currentTouchLayer.touchesEnabled) {\
\
    HANDLE_PRETOUCHES(touchLayer, point, trend)\
\
if ([touchLayer respondsToSelector:@selector(touchesCancelledAtPoint:)]) {\
    [touchLayer touchesCancelledAtPoint:point];\
    HANDLE_MUTUAL_EXCLUSIVITY(touchLayer);\
}\
\
if ([touchLayer respondsToSelector:@selector(touchesCancelledAtPoint:withTouchTrend:)]) {\
    [touchLayer touchesCancelledAtPoint:point withTouchTrend:trend];\
    HANDLE_MUTUAL_EXCLUSIVITY(touchLayer);\
}\
\
    HANDLE_POSTTOUCHES(touchLayer, point, trend)\
}\
//End of Touches Cancelled Macro

#define HANDLE_TOUCHES_OUT_OF_RANGE(touchLayer, point) \
if ([_activeTouchLayers containsObject:touchLayer]) { \
    if ([currentTouchLayer respondsToSelector: \
         @selector(touchesOutOfRangeAtPoint:)]) { \
        [touchLayer touchesOutOfRangeAtPoint:point]; \
        [_activeTouchLayers removeObject:touchLayer]; \
    } \
} \

- (instancetype)initWithNode:(SKNode *)node
{
    if (self = [super init]) {
        _node = node;
        _touchLayers = [[NSMutableArray alloc] initWithCapacity:3];
        _activeTouchLayers = [[NSMutableArray alloc] initWithCapacity:3];
        _zOrderedTouchLayers = [[NSArray alloc] init];
        _previousTouches = nil;
    }
    
    return self;
}

#pragma mark - Public methods

- (BOOL)subscribeTouchLayer:(id <TouchLayerProtocol>)touchLayer
{
    BOOL isSuccess = NO;
    
    if (touchLayer && ![_touchLayers containsObject:touchLayer]) {
        [_touchLayers addObject:touchLayer];
        isSuccess = YES;
        
        [self sortTouchLayers];
    }
    
    return isSuccess;
}

- (BOOL)unsubscribeTouchLayer:(id <TouchLayerProtocol>)touchLayer
{
    BOOL isSuccess = NO;
    
    if (touchLayer && [_touchLayers containsObject:touchLayer]) {
        [_touchLayers removeObject:touchLayer];
        isSuccess = YES;
        
        [self sortTouchLayers];
    }
    
    return isSuccess;
}

- (BOOL)unsubscribeAllTouchLayers
{
    BOOL isSuccess = NO;
    
    if ([_touchLayers count]) {
        [_touchLayers removeAllObjects];
        isSuccess = YES;
        
        _zOrderedTouchLayers = [[NSArray alloc] init];
    }
    
    return isSuccess;
}

- (void)sortTouchLayers
{
    _zOrderedTouchLayers = [_touchLayers sortedArrayUsingComparator:
                            ^NSComparisonResult(id <TouchLayerProtocol> _Nonnull obj1,
                                                id <TouchLayerProtocol> _Nonnull obj2) {
                                if (obj1.zPosition > obj2.zPosition) {
                                    return NSOrderedAscending;
                                }else if (obj1.zPosition < obj2.zPosition) {
                                    return NSOrderedDescending;
                                }else {
                                    return NSOrderedSame;
                                }
                            }];
    
}

- (TouchTrend)analyseTouchTrend:(NSSet<UITouch *> *)touches
{
    TouchTrend trend = TouchTrendUnknown;
    
    CGPoint previousPoint = [self pointInNodeForTouches:_previousTouches];
    CGPoint currentPoint = [self pointInNodeForTouches:touches];
    
    if (!CGPointEqualToPoint(previousPoint, DummyPoint) && !CGPointEqualToPoint(currentPoint, DummyPoint)) {
        CGFloat deltaX = currentPoint.x - previousPoint.x;
        CGFloat deltaY = currentPoint.y - previousPoint.y;
        
        if (deltaX > 0) {
            trend = TouchTrendXIncreasing;
        }else if (deltaX < 0) {
            trend = TouchTrendXDecreasing;
        }
        
        if (deltaY > 0) {
            trend |= TouchTrendYIncreasing;
        }else if (deltaY < 0) {
            trend |= TouchTrendXDecreasing;
        }
    }
    
    return trend;
}

- (CGPoint)pointInNodeForTouches:(NSSet *)touches
{
    UITouch *touch = [touches anyObject];
    
    CGPoint pointInNode = DummyPoint;
    
    if (touch) {
        pointInNode = [touch locationInNode:_node];
    }

    return pointInNode;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    DDLogDebug(@"TouchController:TouchesBegan:");
    BYPASS_TOUCHES_IFREQUIRED();
    COMPUTE_POINT_AND_TREND();
    
    for (id <TouchLayerProtocol>currentTouchLayer in _zOrderedTouchLayers) {
        if (CGRectContainsPoint(currentTouchLayer.frame, pointInNode)) {
            DDLogDebug(@"CurrentTouchLayer:%@", currentTouchLayer);
            [_activeTouchLayers addObject:currentTouchLayer];
            
            FORWARD_TOUCHESBEGAN_TOUCHPOINT_AND_TREND(currentTouchLayer,
                                                      pointInNode,
                                                      trend);
        }
    }
    
    _previousTouches = touches;
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    DDLogDebug(@"TouchController:TouchesMoved:");
    BYPASS_TOUCHES_IFREQUIRED();
    COMPUTE_POINT_AND_TREND();
    
    for (id <TouchLayerProtocol>currentTouchLayer in _zOrderedTouchLayers) {
        if (CGRectContainsPoint(currentTouchLayer.frame, pointInNode)) {
            DDLogDebug(@"CurrentTouchLayer:%@", currentTouchLayer);
            
            FORWARD_TOUCHESMOVED_TOUCHPOINT_AND_TREND(currentTouchLayer,
                                                      pointInNode,
                                                      trend);
        }
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    DDLogDebug(@"TouchController:TouchesEnded:");
    BYPASS_TOUCHES_IFREQUIRED();
    COMPUTE_POINT_AND_TREND();
    
    for (id <TouchLayerProtocol>currentTouchLayer in _zOrderedTouchLayers) {
        if (CGRectContainsPoint(currentTouchLayer.frame, pointInNode)) {
            DDLogDebug(@"CurrentTouchLayer:%@", currentTouchLayer);
            
            FORWARD_TOUCHESENDED_TOUCHPOINT_AND_TREND(currentTouchLayer,
                                                      pointInNode,
                                                      trend);
        }else {
            HANDLE_TOUCHES_OUT_OF_RANGE(currentTouchLayer, pointInNode);
        }
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    DDLogDebug(@"TouchController:TouchesCancelled:");
    BYPASS_TOUCHES_IFREQUIRED();
    COMPUTE_POINT_AND_TREND();
    
    for (id <TouchLayerProtocol>currentTouchLayer in _zOrderedTouchLayers) {
        if (CGRectContainsPoint(currentTouchLayer.frame, pointInNode)) {
            DDLogDebug(@"CurrentTouchLayer:%@", currentTouchLayer);
            
            FORWARD_TOUCHESCANCELLED_TOUCHPOINT_AND_TREND(currentTouchLayer,
                                                          pointInNode,
                                                          trend);
        }else {
            HANDLE_TOUCHES_OUT_OF_RANGE(currentTouchLayer, pointInNode);
        }
    }
}

@end

//
//  InfoViewController.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 22/02/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewController : UIViewController

- (IBAction)openSourceCreditsAction:(id)sender;
- (IBAction)dismissAction:(id)sender;

@end

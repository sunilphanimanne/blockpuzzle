//
//  Theme.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "Theme.h"
#import "GameUtils.h"

@implementation Theme

- (instancetype)initWithName:(NSString *)name
{
    if ((self = [super init])) {
        _name = name;
    }
    
    return self;
}

#pragma mark - NSDecoder methods

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    _isDark = [aDecoder decodeBoolForKey:@"Theme.isDark"];
    
    if (_isDark) {
        self = [Theme getDarkTheme];
    }else {
        self = [Theme getLightTheme];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeBool:_isDark forKey:@"Theme.isDark"];
}

+ (Theme *)getDarkTheme
{
    Theme *darkTheme = [[Theme alloc] initWithName:DarkTheme];
    SKColor *darkGrayColor = getColor(34, 35, 36, 1);
    SKColor *lightGrayColor = getColor(69, 70, 71, 1);
    SKColor *lightBlueColor = getColor(1, 159, 254, 1);
    
    darkTheme->_gameScoreColor = lightBlueColor;
    darkTheme->_gameHiScoreColor = [SKColor orangeColor];
    darkTheme->_bufferBoardColor = darkGrayColor;
    darkTheme->_scorePanelColor = darkGrayColor;
    darkTheme->_masterBackgroundColor = darkGrayColor;
    darkTheme->_puzzleBoardColor = darkGrayColor;
    darkTheme->_puzzleBoardBlockColor = lightGrayColor;
    darkTheme->_isDark = YES;
    
    return darkTheme;
}

+ (Theme *)getLightTheme
{
    Theme *lightTheme = [[Theme alloc] initWithName:WhiteTheme];
    
    SKColor *lightGrayColor = getColor(220, 220, 220, 0.5);
    
    lightTheme->_gameScoreColor = [SKColor purpleColor];
    lightTheme->_gameHiScoreColor = [SKColor orangeColor];
    lightTheme->_bufferBoardColor = [SKColor whiteColor];
    lightTheme->_scorePanelColor = [SKColor whiteColor];
    lightTheme->_masterBackgroundColor = [SKColor whiteColor];
    lightTheme->_puzzleBoardColor = [SKColor whiteColor];
    lightTheme->_puzzleBoardBlockColor = lightGrayColor;
    lightTheme->_isDark = NO;
    
    return lightTheme;
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end

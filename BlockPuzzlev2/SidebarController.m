//
//  SidebarController.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 10/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "SidebarController.h"

#import "Theme.h"
#import "GameUtils.h"
#import "GameScene.h"
#import "GameAlert.h"
#import "ThemeManager.h"
#import "SoundManager.h"
#import "Reachability.h"
#import "GameKitHelper.h"
#import "GameConstants.h"
#import "RNFrostedSidebar.h"
#import "AnalyticsLogUtils.h"
#import "AVVideoDemoController.h"

#define ROOT_VIEW_CONTROLLER [[UIApplication sharedApplication] keyWindow].rootViewController

typedef NS_ENUM(NSUInteger, SidebarIndex) {
    SidebarIndexLightThemSelector = 0,
    SidebarIndexGameMusicOption,
    SidebarIndexGameSFXOption,
    SidebarIndexGameRestartOption,
    SidebarIndexUserRatingsOption,
    SidebarIndexGameDemoOption,
    SidebarIndexGameCenterOption,
    SidebarIndexGameInfoOption
};

static const CGFloat sidebarWidthInPercent = 40;

NSString *const GameRestartAlertMessage = @"Restart Game ?";

@interface SidebarController () <RNFrostedSidebarDelegate>
{
@private
    UIImageView *_screenShot;
    RNFrostedSidebar *_sidebar;
    GameAlert *_alert;
    NSArray *_actions;
}

@property (nonatomic, strong) NSMutableIndexSet *optionIndices;

@end

@implementation SidebarController

- (instancetype)init
{
    if (self = [super init]) {
        [self configureSidebar];
        [self configureAlert];
    }
    return self;
}

- (void)configureSidebar
{
    SKColor *sunColor = getColor(252, 173, 0, 1);
    SKColor *restartColor = getColor(147, 218, 98, 1);
    SKColor *musicColor = getColor(252, 40, 252, 1);
    SKColor *soundColor = getColor(44, 254, 241, 1);
    SKColor *userRatingsColor = [SKColor redColor];
    SKColor *demoColor = getColor(244, 232, 0, 1);
    SKColor *gameCenterColor = getColor(79, 180, 242, 1);
    SKColor *infoColor = getColor(255, 255, 255, 1);
    
    NSArray *images = @[
                        [UIImage imageNamed:@"sun"],
                        [UIImage imageNamed:@"music"],
                        [UIImage imageNamed:@"sound"],
                        [UIImage imageNamed:@"restart"],
                        [UIImage imageNamed:@"userratings"],
                        [UIImage imageNamed:@"demo"],
                        [UIImage imageNamed:@"gamecenter"],
                        [UIImage imageNamed:@"info"]
                       ];
    
    NSArray *colors = @[
                        sunColor,
                        musicColor,
                        soundColor,
                        restartColor,
                        userRatingsColor,
                        demoColor,
                        gameCenterColor,
                        infoColor
                       ];
    
    _actions = @[
                 @"Themes",
                 @"Music",
                 @"SFX",
                 @"RestartGame",
                 @"UserRatings",
                 @"Demo",
                 @"GameCenter",
                 @"Info"
                 ];
    
    self.optionIndices = [[NSMutableIndexSet alloc] init];
    
    [self syncGameStateToOptions];
    
    _sidebar = [[RNFrostedSidebar alloc] initWithImages:images
                                        selectedIndices:self.optionIndices
                                           borderColors:colors];
    
    _sidebar.width = widthInPercent(sidebarWidthInPercent);
    
    _sidebar.itemSize = GetSidebarItemSize();
    
    _sidebar.delegate = self;
    
    _screenShot = [[UIImageView alloc] init];
}

- (void)configureAlert
{
    _alert = [[GameAlert alloc] initWithMessage:GameRestartAlertMessage
                                       fontSize:GetGameRestartAlertFontSize()
                                      fontColor:RestartAlertTextColor];
    _alert.actionBlock = ^(GameAlert *sender, BOOL isCancelled) {
        if (isCancelled) {
            [[SoundManager sharedManager] resumeMusic];
        }else {
            [SCENE restartGame];
        }
    };
}

- (void)syncThemeStateToOptions:(BOOL)applyImmediately
{
    if (SCENE.themeManager) {
        if (!SCENE.themeManager.currentTheme.isDark) {
            [self.optionIndices addIndex:SidebarIndexLightThemSelector];
        }else {
            [self.optionIndices removeIndex:SidebarIndexLightThemSelector];
        }
        
        if (applyImmediately) {
            _sidebar.selectedIndices = self.optionIndices;
        }
    }
}

- (void)syncSoundStateToOptions:(BOOL)applyImmediately
{
    if ([SoundManager sharedManager].isMusicEnabled) {
        [self.optionIndices addIndex:SidebarIndexGameMusicOption];
    }else {
        [self.optionIndices removeIndex:SidebarIndexGameMusicOption];
    }
    
    if ([SoundManager sharedManager].isSFXEnabled) {
        [self.optionIndices addIndex:SidebarIndexGameSFXOption];
    }else {
        [self.optionIndices removeIndex:SidebarIndexGameSFXOption];
    }
    
    if (applyImmediately) {
        _sidebar.selectedIndices = self.optionIndices;
    }
}

- (void)syncGameStateToOptions
{
    [self syncThemeStateToOptions:NO];
    [self syncSoundStateToOptions:NO];
    
    _sidebar.selectedIndices = self.optionIndices;
}

- (void)flipSlidebar
{
    [self syncGameStateToOptions];
    [_sidebar show];
}

- (void)sidebar:(RNFrostedSidebar *)sidebar willShowOnScreenAnimated:(BOOL)animated
{
    SCENE.touchController.shouldBypassTouches = YES;
}

- (void)sidebar:(RNFrostedSidebar *)sidebar didDismissFromScreenAnimated:(BOOL)animated
{
    SCENE.touchController.shouldBypassTouches = NO;
}

- (void)sidebar:(RNFrostedSidebar *)sidebar didTapItemAtIndex:(NSUInteger)index
{
    switch (index) {
        case SidebarIndexLightThemSelector:
            [self dismissAndFlipTheme];
            break;
            
        case SidebarIndexGameRestartOption:
            [self dismissAndShowRestartAlert];
            break;
        
        case SidebarIndexGameMusicOption:
            [self dismissAndFlipMusic];
            break;
            
        case SidebarIndexGameSFXOption:
            [self dismissAndFlipSFX];
            break;
            
        case SidebarIndexGameCenterOption:
            [self showGameCenterOptions];
            [self justDismissSidebar];
            break;
            
        case SidebarIndexGameDemoOption:
            [self dismissAndShowDemo];
            break;
            
        case SidebarIndexUserRatingsOption:
            [self dismissAndShowUserRatingsAlert];
            break;
            
        case SidebarIndexGameInfoOption:
            [self dismissAndShowInfoPage];
            break;
    }
    
    if (index < [_actions count]) {
        [AnalyticsLogUtils logSidebarAction:_actions[index]];
    }
}

- (void)dismissAndShowUserRatingsAlert
{
    [_sidebar dismissAnimated:YES completion:^(BOOL finished) {
        [GameUtils showUserAppRatingsAlertForcefully];
    }];
}

- (void)showTheInfoPage
{
    [ROOT_VIEW_CONTROLLER performSegueWithIdentifier:ShowInfoPageSegue
                                              sender:self];
}

- (void)dismissAndShowInfoPage
{
    [_sidebar dismissAnimated:YES completion:^(BOOL finished) {
        [self showTheInfoPage];
    }];
}

- (void)dismissAndShowDemo
{
    [_sidebar dismissAnimated:YES completion:^(BOOL finished) {
        [self showGameDemo:[SoundManager sharedManager].isMusicEnabled];
        [self turnOffMusicTemporarily];
    }];
}


- (void)turnOffMusicTemporarily
{
    if ([SoundManager sharedManager].isMusicEnabled) {
        [SoundManager sharedManager].isMusicEnabled = NO;
    }
}

- (void)showGameDemo:(BOOL)isMusicEnabled
{
    AVVideoDemoController *demoController = [[AVVideoDemoController alloc] initWithVideoName:DemoVideoFileName
                                                                     presentOnViewController:ROOT_VIEW_CONTROLLER];
    [demoController playDemo:^{
        DDLogInfo(@"Video played...");
        [self restoreMusic:isMusicEnabled];
    }];
}

- (void)restoreMusic:(BOOL)originalState
{
    if (originalState) {
        [SoundManager sharedManager].isMusicEnabled = originalState;
    }
}

- (void)showRestartAlert
{
    [_alert showInScene:SCENE completion:^{
        [[SoundManager sharedManager] pauseMusic];
    }];
}

- (void)flipMusic
{
    if ([SoundManager sharedManager].isMusicEnabled) {
        [SoundManager sharedManager].isMusicEnabled = NO;
    }else {
        [SoundManager sharedManager].isMusicEnabled = YES;
    }
}

- (void)flipSFX
{
    if ([SoundManager sharedManager].isSFXEnabled) {
        [SoundManager sharedManager].isSFXEnabled = NO;
    }else {
        [SoundManager sharedManager].isSFXEnabled = YES;
    }
}

- (void)justDismissSidebar
{
    [_sidebar dismissAnimated:YES];
}

- (void)dismissAndShowRestartAlert
{
    [_sidebar dismissAnimated:YES completion:^(BOOL finished) {
        [self showRestartAlert];
    }];
}

- (void)dismissAndFlipMusic
{
    [_sidebar dismissAnimated:YES completion:^(BOOL finished) {
        [self flipMusic];
    }];
}

- (void)dismissAndFlipSFX
{
    [_sidebar dismissAnimated:YES completion:^(BOOL finished) {
        [self flipSFX];
    }];
}

- (void)dismissAndFlipTheme
{
    [_sidebar dismissAnimated:YES completion:^(BOOL finished) {
        [SCENE flipTheme];
    }];
}

- (void)showGameCenterOptions
{
    GameKitHelper *gameKitHelper = [GameKitHelper sharedHelper];
    
    if (!gameKitHelper.isAuthenticated) {
        DDLogError(@"SidebarController: Error: Requested Leaderboard without gamekit authentication!");
        [self showLeaderBoardErrorWithMessage:@"GC login failed, retry later!"];
    }else {
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        if ([reachability isReachable]) {
            [gameKitHelper reportHighScoreMissedDueToInternetConnectivityIssues];
            [self showLeaderBoard];
        }else {
            [self showLeaderBoardErrorWithMessage:@"No internet, please retry!"];
        }
    }
}

- (void)showLeaderBoard
{
    [[GameKitHelper sharedHelper] showLeaderboardAndAchievements:GameCenterDisplayTypeLeaderBoard];
}

- (void)showLeaderBoardErrorWithMessage:(NSString *)message
{
    GameAlert *errorAlert = [[GameAlert alloc] initForInfoDisplayWithMessage:message
                            fontSize:[DynamicConstants leaderBoardErrorAlertFontSize]
                           fontColor:GameCenterErrorAlertTextColor];
    [errorAlert showInScene:SCENE];
}

@end

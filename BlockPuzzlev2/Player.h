//
//  Player.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 03/05/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ScoreListener.h"

//PlayerDemoStatus
typedef NS_ENUM(NSUInteger, PlayerDemoStatus) {
    PlayerDemoStatusNotStarted = 0,
    PlayerDemoStatusStarted,
    PlayerDemoStatusEnded
};

@interface Player : NSObject <NSSecureCoding, ScoreListener>

@property (nonatomic, readonly) int64_t currentScore;
@property (nonatomic, readonly) int64_t highScore;
@property (nonatomic, readonly) NSString *playerID;
@property (nonatomic, assign) PlayerDemoStatus demoStatus;

+ (instancetype)loadInstance;
- (void)save;

@end

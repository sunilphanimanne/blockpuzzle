//
//  GameoverOverlay.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 27/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "GameoverOverlay.h"

#import "GameUtils.h"
#import "GameScene.h"
#import "GameConstants.h"
#import "PlayButtonTouchLayer.h"

@implementation GameoverOverlay

SKLabelNode *_scoreLabel;
SKLabelNode *_scoreValue;
SKLabelNode *_highScoreLabel;
SKLabelNode *_highScoreValue;
SKLabelNode *_gameoverLabel;
SKEmitterNode *_magicNode;
SKSpriteNode *_playButton;
PlayButtonTouchLayer *_playButtonTouchLayer;

#define GAMEOVERLAY_SCORE_LABEL_POSITION CGPointMake(CENTERX, CENTERY - 60)
#define GAMEOVERLAY_SCORE_VALUE_POSITION CGPointMake(CENTERX, CENTERY - 90)
#define GAMEOVERLAY_HI_SCORE_LABEL_POSITION CGPointMake(CENTERX, CENTERY)
#define GAMEOVERLAY_HI_SCORE_VALUE_POSITION CGPointMake(CENTERX, CENTERY - 30)
#define GAMEOVERLAY_GAME_OVER_LABEL_POSITION CGPointMake(CENTERX, CENTERY + 80)
#define GAMEOVERLAY_REPLAY_BUTTON_POSITION CGPointMake(CENTERX, CENTERY)

- (instancetype)init
{
    if (self = [super init]) {
        [self setupMagic];
        [self setupLabels];
        [self setupButtons];
        [self setupTouchLayers];
        [self doHouseKeepingActivities];
    }
    
    return self;
}

- (void)setupTouchLayers
{
    //setup touch logic...
    _playButtonTouchLayer = [[PlayButtonTouchLayer alloc] init];
    [SCENE.touchController subscribeTouchLayer:_playButtonTouchLayer];
    
    _playButtonTouchLayer.playButton = _playButton;
    _playButtonTouchLayer.frame = [_playButton calculateAccumulatedFrame];
}

- (void)disableTouchLayers
{
    [SCENE.touchController unsubscribeTouchLayer:_playButtonTouchLayer];
    _playButtonTouchLayer = nil;
}

- (void)setupButtons
{
    _playButton = [self createPlayButtonAtPosition:GAMEOVERLAY_REPLAY_BUTTON_POSITION];
    [self addChild:_playButton];
}

- (void)doHouseKeepingActivities
{
    self.color = GameoverOverlayBackgroundColor;
    self.colorBlendFactor = 1.0;
    self.anchorPoint = CGPointMake(0, 0);
    self.size = GetScreenRect().size;
    self.alpha = GameoverOverlayInitalAlpha;
    self.zPosition = 0;
}

- (void)setupLabels
{
    /* _score = score;
     _highScore = highScore;
     
     _scoreLabel = [self createScoreLabelAtPosition:GAMEOVERLAY_SCORE_LABEL_POSITION];
     _scoreValue = [self createLiveValueLabelForValue:[score description]
     atPosition:GAMEOVERLAY_SCORE_VALUE_POSITION];
     
     _highScoreLabel = [self createHighScoreLabelAtPosition:GAMEOVERLAY_HI_SCORE_LABEL_POSITION];
     _highScoreValue = [self createLiveValueLabelForValue:[highScore description]
     atPosition:GAMEOVERLAY_HI_SCORE_VALUE_POSITION];
    
            [self addChild:_scoreLabel];
            [self addChild:_scoreValue];
            [self addChild:_highScoreLabel];
            [self addChild:_highScoreValue]; */
    
    _gameoverLabel = [self createGameoverLabelAtPosition:GAMEOVERLAY_GAME_OVER_LABEL_POSITION];

    [self addChild:_gameoverLabel];
}

- (void)setupMagic
{
    NSString *magicFilePath = [[NSBundle mainBundle] pathForResource:@"Magic"
                                                              ofType:@"sks"];
    
    _magicNode = [NSKeyedUnarchiver unarchiveObjectWithFile:magicFilePath];
    _magicNode.position = CGPointMake(CENTERX, CENTERY);
    _magicNode.particlePositionRange = CGVectorMake(GetScreenRect().size.width,
                                                    GetScreenRect().size.height);
    [self addChild:_magicNode];
}

- (SKSpriteNode *)createPlayButtonAtPosition:(CGPoint)position
{
    //Create a node
    SKSpriteNode *playButton = [GameUtils getPlayButton];
    playButton.position = position;

    return playButton;
}

- (SKLabelNode *)createScoreLabelAtPosition:(CGPoint)position
{
    SKLabelNode *scoreLabel = CREATE_LABEL_NODE(scoreLabel,
                                                ScoreLabelString,
                                                GameoverScoreLabelFontName,
                                                GameoverOverlayScoreFontSize,
                                                GameoverOverlayScoreColor,
                                                position,
                                                GameSceneScoreLabelZPosition
                                                );
    return scoreLabel;
}


- (SKLabelNode *)createHighScoreLabelAtPosition:(CGPoint)position
{
    SKLabelNode *highScoreLabel = CREATE_LABEL_NODE(highScoreLabel,
                                                    HighScoreLabelString,
                                                    GameoverHighScoreLabelFontName,
                                                    GameoverOverlayHighScoreFontSize,
                                                    GameoverOverlayHighScoreColor,
                                                    position,
                                                    GameSceneScoreLabelZPosition
                                                    );
    
    return highScoreLabel;
}

- (SKLabelNode *)createLiveValueLabelForValue:(NSString *)value atPosition:(CGPoint)position
{
    SKLabelNode *liveValueLabel = CREATE_LABEL_NODE(liveValueLabel,
                                                    value,
                                                    GameoverLiveValueLabelFontName,
                                                    GameoverOverlayHighScoreFontSize,
                                                    GameoverOverlayLiveValueColor,
                                                    position,
                                                    GameSceneScoreLabelZPosition);
    
    return liveValueLabel;
}

- (SKLabelNode *)createGameoverLabelAtPosition:(CGPoint)position
{
    SKLabelNode *gameoverLabel = CREATE_LABEL_NODE(gameoverLabel,
                                                   GameoverLabelString,
                                                   GameoverLiveValueLabelFontName,
                                                   GameoverOverlayGameoverFontSize,
                                                   GameoverOverlayGameoverLabelColor,
                                                   position,
                                                   GameSceneScoreLabelZPosition);
    
    return gameoverLabel;
}

- (void)fadein
{
    SKAction *fadeinAction = [SKAction fadeAlphaTo:GameoverOverlayFinalAlpha
                                          duration:GameoverOverlayAlphaFadeAnimationDuration];
    [self runAction:fadeinAction];
}

- (void)fadeoutWithCompletion:(void (^)())block
{
    SKAction *fadeoutAction = [SKAction fadeAlphaTo:GameoverOverlayDismissAlpha
                                           duration:GameoverOverlayAlphaFadeAnimationDuration];
    
    [self runAction:fadeoutAction completion:block];
}

- (void)dealloc
{
    [self disableTouchLayers];
}

@end

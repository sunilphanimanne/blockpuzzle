//
//  MinuteBlock.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 24/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "MinuteBlock.h"

#import "GameConstants.h"

@implementation MinuteBlock

+ (instancetype)minuteBlockWithColor:(SKColor *)color
{
    return [self minuteBlockWithColor:color ofLength:GetPuzzleBlockLength()];
}

+ (instancetype)minuteBlockWithColor:(SKColor *)color ofLength:(CGFloat)length
{
    MinuteBlock *minuteBlock = [[self class] spriteNodeWithImageNamed:MinuteBlockName];
    
    minuteBlock.anchorPoint = CGPointMake(0.5, 0.5);
    minuteBlock.size = CGSizeMake(length, length);
    minuteBlock.color = color;
    minuteBlock.colorBlendFactor = 1.0;
    
    return minuteBlock;
}


@end

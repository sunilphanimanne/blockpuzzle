//
//  LevelManager.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 06/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ScoreListener.h"

@protocol LevelListener;

@interface LevelManager : NSObject <ScoreListener>

@property (nonatomic, readonly) NSUInteger currentLevel;
@property (nonatomic, readonly) NSUInteger totalLevels;
@property (nonatomic, readonly) CGFloat currentLevelPercentage;

- (BOOL)subscribeForLevelUpdates:(id <LevelListener>)listener;
- (BOOL)unsubscribeForLevelUpdates:(id <LevelListener>)listener;
- (BOOL)unsubscribeAllListeners;

- (BlockShapeType)shapeForLevel:(NSUInteger)level;

@end

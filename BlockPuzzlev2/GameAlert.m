//
//  GameAlert.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 14/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//


#import "GameAlert.h"

#import "GameScene.h"
#import "GameConstants.h"

static const CGFloat AlertButtonZoomInValue = 1.5;
static const CGFloat AlertButtonZoomOutValue = 1.0;
static const CGFloat AlertButtonZoomInAnimationDuration = 0.1;
static const CGFloat AlertButtonZoomOutAnimationDuration = 0.3;

NSString *const RightMarkFileName = @"rightmark";
NSString *const WrongMarkFileName = @"wrongmark";

@interface GameAlert ()
{
@private
    SKLabelNode *_messageNode;
    SKSpriteNode *_okButton;
    SKSpriteNode *_cancelButton;
    BOOL _isContentCreated;
    BOOL _showCancelButton;
    UIGestureRecognizer *_singleTapGestureRecognizer;
}
@end

@implementation GameAlert

- (instancetype)initForInfoDisplayWithMessage:(NSString *)message
                                     fontSize:(CGFloat)fontSize
                                    fontColor:(SKColor *)fontColor
{
    return [self initWithMessage:message
                showCancelButton:NO
                        fontSize:fontSize
                       fontColor:fontColor];
}

- (instancetype)initWithMessage:(NSString *)message
               showCancelButton:(BOOL)showCancelButton
                       fontSize:(CGFloat)fontSize
                      fontColor:(SKColor *)fontColor
{
    CGFloat absoluteWidth = widthInPercent(93.f);
    CGRect frame = CGRectMake(CENTERX - absoluteWidth/2.,
                              CENTERY - heightInPercent(15.f),
                              absoluteWidth, heightInPercent(30.f));
    
    if ((self = [[self class] shapeNodeWithRect:frame
                                   cornerRadius:30])) {
        self.fillColor = [SKColor blackColor];
        self.strokeColor = [SKColor clearColor];
        
        _showCancelButton = showCancelButton;
        
        _messageNode = [self createLabelWithText:message
                                        fontSize:fontSize
                                       fontColor:fontColor];

        _okButton = [self createOKButton];
        
        if (_showCancelButton) {
            _cancelButton = [self createCancelButton];
        }
        
        self.alpha = GameResetAlertAlpha;
        
        _singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
        
        _isContentCreated = YES;
    }
    
    return self;
}

- (instancetype)initWithMessage:(NSString *)message
                       fontSize:(CGFloat)fontSize
                      fontColor:(SKColor *)fontColor
{
    return [self initWithMessage:message showCancelButton:YES
                        fontSize:fontSize
                       fontColor:fontColor];
}

- (SKLabelNode *)createLabelWithText:(NSString *)message
                            fontSize:(CGFloat)fontSize
                           fontColor:(SKColor *)fontColor
{
    SKLabelNode *label = CREATE_LABEL_NODE(label,
                                           message,
                                           GameResetAlertTextFontName,
                                           fontSize,
                                           fontColor,
                                           CGPointMake(CENTERX, CENTERY + heightInPercent(3.f)),
                                           GameSceneScoreLabelZPosition
                                           );
    _messageNode = label;
    
    [self addChild:label];
    
    return label;
}

- (SKSpriteNode *)createOKButton
{
    SKSpriteNode *button = [self createButtonWithImage:RightMarkFileName];
    
    CGFloat x = CENTERX;
    CGFloat y = CENTERY - heightInPercent(4.f);
    
    if (_showCancelButton) {
        x = CENTERX - widthInPercent(33.f);
        y = CENTERY - heightInPercent(7.5f);
    }
    
    button.position = CGPointMake(x, y);
    
    [self addChild:button];
    return button;
}

- (SKSpriteNode *)createCancelButton
{
    SKSpriteNode *button = [self createButtonWithImage:WrongMarkFileName];
    button.position = CGPointMake(CENTERX + widthInPercent(33.),
                                  CENTERY - heightInPercent(7.5f));
    [self addChild:button];
    return button;
}

- (SKSpriteNode *)createButtonWithImage:(NSString *)imageName
{
    SKSpriteNode *button = [SKSpriteNode spriteNodeWithImageNamed:imageName];
    button.alpha = 1;
    return button;
}

- (void)setMessage:(NSString *)message
{
    if (![_messageNode.text isEqualToString:message]) {
        _messageNode.text = message;
    }
}

- (void)showInScene:(SKScene *)scene completion:(void (^)())block
{
    SCENE.touchController.shouldBypassTouches = YES;
    [scene addChild:self];
    [SCENE.view addGestureRecognizer:_singleTapGestureRecognizer];
    
    if (block) {
        block();
    }
}

- (void)showInScene:(SKScene *)scene
{
    [self showInScene:scene completion:nil];
}

- (void)dismissWithCompletion:(void (^)())block
{
    SCENE.touchController.shouldBypassTouches = NO;
    [self removeFromParent];
    [SCENE.view removeGestureRecognizer:_singleTapGestureRecognizer];
    
    if (block) {
        block();
    }
}

- (void)dismiss
{
    [self dismissWithCompletion:nil];
}

#pragma mark - Touch related methods

-(void)handleSingleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    DDLogDebug(@"GameAlert:handleSingleTapGesture:");
    
    CGPoint touchLocation = [tapGestureRecognizer locationInView:SCENE.view];
    
    touchLocation = [SCENE convertPointFromView:touchLocation];
    
    DDLogDebug(@"TouchLocation:%@ OkButtonFrame:%@ CancelButtonFrame:%@", NSStringFromCGPoint(touchLocation), NSStringFromCGRect(_okButton.frame), NSStringFromCGRect(_cancelButton.frame));
    SKSpriteNode *button = nil;
    BOOL isAlertCancelled = YES;
    
    if (CGRectContainsPoint(_okButton.frame, touchLocation)) {
        button = _okButton;
        isAlertCancelled = NO;
    }else if(CGRectContainsPoint(_cancelButton.frame, touchLocation)) {
        button = _cancelButton;
        isAlertCancelled = YES;
    }else {
        isAlertCancelled = YES;
    }
    
    if (button) {
        [self animateButton:button completion:^{
            [self dismissAndExecuteAction:isAlertCancelled];
        }];
    }else {
        [self dismissAndExecuteAction:isAlertCancelled];
    }
}

- (void)dismissAndExecuteAction:(BOOL)isAlertCancelled
{
    [self dismiss];
    
    if (_actionBlock) {
        _actionBlock(self, isAlertCancelled);
    }
}

- (void)animateButton:(SKSpriteNode *)button completion:(void (^)())block
{
    if (button) {
        SKAction *zoomInAction = [SKAction scaleTo:AlertButtonZoomInValue
                                          duration:AlertButtonZoomInAnimationDuration];
        
        SKAction *zoomOutAction = [SKAction scaleTo:AlertButtonZoomOutValue
                                           duration:AlertButtonZoomOutAnimationDuration];
        
        SKAction *sequence = [SKAction sequence:@[zoomInAction, zoomOutAction]];
        
        [button runAction:sequence completion:^{
            block();
        }];
    }
}

@end

//
//  GameKitHelper.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 30/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "GameKitHelper.h"

#import "GameScene.h"
#import "GameUtils.h"
#import "GameContext.h"
#import "GameConstants.h"
#import "ScoreListener.h"
#import "LevelListener.h"
#import "ScoreKeeper+Private.h"

@interface GameKitHelper () <GKGameCenterControllerDelegate,
                             ScoreListener,
                             LevelListener>
{
@private
    NSString *_leaderboardID;
}

@end

@implementation GameKitHelper

- (id)init
{
    self = [super init];
    if (self) {
        _authenticated = NO;
    }
    return self;
}

+ (instancetype)sharedHelper
{
    static GameKitHelper *sharedGameKitHelper;
    static dispatch_once_t helperToken;
    dispatch_once(&helperToken, ^{
        sharedGameKitHelper = [[GameKitHelper alloc] init];
    });
    return sharedGameKitHelper;
}

- (void)authenticateLocalPlayer
{
    [self authenticateLocalPlayerWithCompletionHandler:nil];
}

- (void)authenticateLocalPlayerWithCompletionHandler:(void(^)(NSError *error))completionHandler
{
    LOCALPLAYER.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (error) {
            completionHandler(error);
        }else {
            if (viewController) {
                [self presentAuthenticateViewController:viewController];
            }else {
                if (LOCALPLAYER.isAuthenticated) {
                    //enable the gamecenter
                    _authenticated = YES;
                    
                    //load the leadboardID
                    [self loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString * _Nullable leaderboardIdentifier, NSError * _Nullable error) {
                        
                        if (error) {
                            DDLogError(@"GameKitHelper: Error Loading the default Leadboard ID:%@ Error:%@", leaderboardIdentifier,
                                error.localizedDescription);
                        }else {
                            DDLogInfo(@"GameKitHelper: LeaderboardID:%@ loaded successfully!",
                                       leaderboardIdentifier);
                        }
                        
                        //subscribe for scoreupdates in order to post them to the gamecenter!
                        [SCENE.scoreKeeper subscribeForScoreUpdates:self];
                        [SCENE.levelManager subscribeForLevelUpdates:self];
                        
                        if (completionHandler) {
                            completionHandler(error);
                        }
                    }];
                }
            }
        }
    };
}

- (void)loadDefaultLeaderboardIdentifier
{
    [self loadDefaultLeaderboardIdentifierWithCompletionHandler:nil];
}

- (void)loadDefaultLeaderboardIdentifierWithCompletionHandler:(void(^__nullable)(NSString * __nullable leaderboardIdentifier, NSError * __nullable error))completionHandler
{
    // Get the default leaderboard identifier.
    [LOCALPLAYER loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString *leaderboardIdentifier, NSError *error) {
        
        if (error) {
            DDLogError(@"Error:%@", [error localizedDescription]);
        }else {
            _leaderboardID = leaderboardIdentifier;
            DDLogInfo(@"LeaderBoardID:%@", _leaderboardID);
            
            if (completionHandler) {
                completionHandler(leaderboardIdentifier, error);
            }
        }
    }];
}

- (void)presentAuthenticateViewController:(UIViewController *)viewController
{
    UIViewController *gameViewController = [GameContext sharedContext].viewController;
    
    if (gameViewController) {
        DDLogDebug(@"GameKitHelper: Presenting the Authentication ViewController...");
        [gameViewController presentViewController:viewController
                                         animated:YES
                                       completion:nil];
    }else {
        DDLogError(@"Error: Unable to present GameKit ViewController as the gameContext.viewController is nil!");
    }
}

- (void)reportHighScoreMissedDueToInternetConnectivityIssues
{
    PlayerManager *playerManager = [PlayerManager sharedManager];
    Player *currentPlayer = playerManager.currentPlayer;
    
    if (currentPlayer) {
        [[GameKitHelper sharedHelper] reportScoreToGameCenter:currentPlayer.highScore];
    }else {
        DDLogError(@"GameViewController: CurrentPlayer is nil, cannot report last high score!");
    }
}

-(void)reportScoreToGameCenter:(int64_t)gameScore
{
    if (_leaderboardID) {
        GKScore *score = [[GKScore alloc] initWithLeaderboardIdentifier:_leaderboardID];
        score.value = gameScore;
        
        [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
            if (error) {
                DDLogError(@"Error:%@", [error localizedDescription]);
            }else {
                DDLogDebug(@"Score reported Succesfully!");
            }
        }];
    }else {
        DDLogError(@"Error:Unable to report score as leadboardID is nil!");
    }
}

-(void)showLeaderboardAndAchievements:(GameCenterDisplayType)displayType
{
    GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
    
    gcViewController.gameCenterDelegate = self;
    
    if (displayType == GameCenterDisplayTypeLeaderBoard) {
        gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
        gcViewController.leaderboardIdentifier = _leaderboardID;
    }else if (displayType == GameCenterDisplayTypeAchievements){
        gcViewController.viewState = GKGameCenterViewControllerStateAchievements;
    }else {
        //Should not be the case...
    }
    
    UIViewController *gameViewController = [GameContext sharedContext].viewController;
    
    [gameViewController presentViewController:gcViewController
                                     animated:YES completion:nil];
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}


- (void)resetScore
{
//    [GKAchievement resetAchievementsWithCompletionHandler:
}
#pragma mark - Acheivement methods

- (void)updateLevelAcheivementForLevel:(NSUInteger)level
{
    NSString *levelID = [self gameCenterLevelIDForLevel:level];
    GKAchievement *levelAchievement = [[GKAchievement alloc] initWithIdentifier:levelID];
    levelAchievement.percentComplete = [GameUtils getCurrentLevelPercentage];
    [self reportAcheivements:@[levelAchievement]];
}

- (NSString *)gameCenterLevelIDForLevel:(NSUInteger)level
{
    NSString *gameCenterLevelID = nil;
    NSArray *gameCenterLevelIDs = @[@"Level1", @"Level2", @"Level3", @"Level4"];
    
    if ((level - 1) < [gameCenterLevelIDs count]) {
        gameCenterLevelID = gameCenterLevelIDs[level - 1];
    }
    
    return gameCenterLevelID;
}

- (void)reportAcheivements:(NSArray *)achievements
{
    [GKAchievement reportAchievements:achievements withCompletionHandler:^(NSError *error) {
        if (error) {
            DDLogError(@"Error: While reporting Acheivement: %@", [error localizedDescription]);
        }else {
            DDLogDebug(@"Acheivement reported succesfully!");
        }
    }];
}

#pragma mark - ScoreListener methods

- (void)scoreType:(ScoreKeeperScoreType)scoreType
         newValue:(int64_t)newValue oldValue:(int64_t)oldValue
          context:(NSDictionary *)contextDict
{
    if (scoreType == ScoreKeeperScoreTypeScore) {
        [self reportScoreToGameCenter:newValue];
    }
}

#pragma mark - LevelListener methods

- (void)gameAdvancedToNewLevel:(NSUInteger)newLevel
                      oldLevel:(NSUInteger)oldLevel
                       context:(NSDictionary *)context
{
#if 0
    [self updateLevelAcheivementForLevel:newLevel];
#endif
}

@end

//
//  GameKitHelper.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 30/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

@import GameKit;

#import "GameConstants.h"

@interface GameKitHelper : NSObject

@property (nonatomic, readonly, getter=isAuthenticated) BOOL authenticated;

+ (instancetype)sharedHelper;

- (void)authenticateLocalPlayer;
- (void)authenticateLocalPlayerWithCompletionHandler:(void(^)(NSError *error))
completionHandler;

- (void)reportScoreToGameCenter:(int64_t)gameScore;
- (void)reportHighScoreMissedDueToInternetConnectivityIssues;

- (void)showLeaderboardAndAchievements:(GameCenterDisplayType)displayType;

- (NSString *)gameCenterLevelIDForLevel:(NSUInteger)level;
@end

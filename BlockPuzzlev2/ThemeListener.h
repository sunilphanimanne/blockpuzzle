//
//  ThemeListener.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Theme.h"

@protocol ThemeListener <NSObject>

@required
- (void)themeApplied:(Theme *)newTheme from:(Theme *)oldTheme;

@end
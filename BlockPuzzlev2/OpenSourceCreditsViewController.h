//
//  OpenSourceCreditsViewController.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 22/02/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpenSourceCreditsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *creditsWebView;

@property (nonatomic, copy) NSString *openSourceCreditsHTMLPage;

- (IBAction)dismissAction:(id)sender;

@end

//
//  BufferBlockTouchLayer.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "TouchLayer.h"

#import "BlockShape.h"

@interface BufferBlockTouchLayer : TouchLayer

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, weak) BlockShape *shape;

@end

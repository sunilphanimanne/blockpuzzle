//
//  PlayButtonTouchLayer.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 01/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "PlayButtonTouchLayer.h"

#import "GameScene.h"
#import "GameConstants.h"

static const CGFloat PlayButtonZoomInValue = 1.5;
static const CGFloat PlayButtonZoomOutValue = 1.0;
static const CGFloat PlayButtonZoomInAnimationDuration = 0.1;
static const CGFloat PlayButtonZoomOutAnimationDuration = 0.3;

@interface PlayButtonTouchLayer ()
{
    BOOL _touchesStarted;
}
@end

@implementation PlayButtonTouchLayer

- (instancetype)init
{
    if (self = [super init]) {
        self.zPosition = PlayButtonTouchLayerZPosition;
        self.isMutuallyExclusive = YES;
    }
    return self;
}

- (void)touchesBeganAtPoint:(CGPoint)point
{
    DDLogDebug(@"PlayButtonTouchLayer: touchesBeganAtPoint:");
    _touchesStarted = YES;
    
    SKAction *zoomInAction = [SKAction scaleTo:PlayButtonZoomInValue
                                      duration:PlayButtonZoomInAnimationDuration];
    
    [self.playButton runAction:zoomInAction];
}

- (void)touchesEndedAtPoint:(CGPoint)point
{
    if (_touchesStarted) {
        DDLogDebug(@"PlayButtonTouchLayer: touchesEndedAtPoint:");
        
        SKAction *zoomOutAction = [SKAction scaleTo:PlayButtonZoomOutValue
                                           duration:PlayButtonZoomOutAnimationDuration];
        [self.playButton runAction:zoomOutAction
                        completion:^{
                            [SCENE restartGame];
                        }
         ];
        
        _touchesStarted = NO;
    }
}

- (void)touchesOutOfRangeAtPoint:(CGPoint)point
{
    if (_touchesStarted) {
        DDLogDebug(@"PlayButtonTouchLayer: touchesOutOfRangeAtPoint:");
        
        SKAction *zoomOutAction = [SKAction scaleTo:PlayButtonZoomOutValue
                                           duration:PlayButtonZoomOutAnimationDuration];
        [self.playButton runAction:zoomOutAction];
    }
}

@end

//
//  ScoreboardTouchLayer.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 31/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "TouchLayer.h"

@interface ScoreboardTouchLayer : TouchLayer

@property (nonatomic, assign) CGRect sidebarMenuFrame;

@end

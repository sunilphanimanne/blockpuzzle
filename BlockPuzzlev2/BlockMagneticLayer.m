//
//  BlockMagneticLayer.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 08/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "BlockMagneticLayer.h"

#import "GameScene.h"
#import "GameConstants.h"

@implementation BlockMagneticLayer

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super init]) {
        _frame = frame;
    }
    
    return self;
}

- (BOOL)findAttractingBlockPosition:(CGPoint *)attractingPosition
                             column:(NSInteger *)column
                             andRow:(NSInteger *)row
                           forPoint:(CGPoint)point
{
    BOOL isSuccess = NO;
    
    if (self.isMagneticEffectOn) {
         *column = NSNotFound;
         *row = NSNotFound;
        
        if ([GameUtils findPuzzleBlockColumn:column
                                         row:row
                                  atPosition:point
                                    forShape:CURRENT_BUFF_BLOCK]) {
            
            DDLogDebug(@"Col####%ld Row####%ld", (long)*column, (long)*row);
            
            *attractingPosition = [GameUtils puzzleBoardPointForColumn:*column row:*row];
            
            DDLogDebug(@"Attracting position:%@", NSStringFromCGPoint(*attractingPosition));
            isSuccess = YES;
        }
    }
    
    return isSuccess;
}

@end

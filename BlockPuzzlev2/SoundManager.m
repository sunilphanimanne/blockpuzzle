//
//  SoundManager.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 24/04/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "SoundManager.h"

#import "GameScene.h"
#import "GameUtils.h"
#import "GameConstants.h"
#import "PersistanceManager.h"

NSString *const SoundManagerPersistanceKey = @"Persistence.SoundManagerKey";


@interface SoundManager ()
{
    NSMutableDictionary *_preloadedSFXFiles;
}

@property (nonatomic, strong) AVAudioPlayer *musicPlayer;

@end

@implementation SoundManager

+ (instancetype)sharedInstance
{
    static SoundManager *sharedInstance = nil;
    static dispatch_once_t soundManagerToken;
    dispatch_once(&soundManagerToken, ^{
        sharedInstance = [self loadInstance];
    });
    return sharedInstance;
}

+ (instancetype)sharedManager {
    return [self sharedInstance];
}

#pragma mark - NSDecoder methods

- (instancetype)init
{
    if (self = [super init]) {
        _isSFXEnabled = YES;
        _isMusicEnabled = YES;
        
        [self preloadSFXFiles];
    }
    
    return self;
}

- (void)preloadSFXFiles
{
    if (!_preloadedSFXFiles) {
        _preloadedSFXFiles = [NSMutableDictionary dictionaryWithCapacity:3];
    }
    
    NSArray *sfxNameArray = @[BlocksClickonSFXName,
                              BlocksLineClearSFXName, 
                              BlocksFallingSFXName];
    
    for (NSString *sfxName in sfxNameArray) {
        SKAction *sfxAction = [SKAction playSoundFileNamed:sfxName
                                             waitForCompletion:YES];
        _preloadedSFXFiles[sfxName] = sfxAction;
    }
    
    sfxNameArray = nil;
}

- (SKAction *)sfxActionForName:(NSString *)sfxName
{
    return _preloadedSFXFiles[sfxName];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [self init])) {
        if([aDecoder containsValueForKey:SoundManagerisSFXEnabledKey]) {
            _isSFXEnabled = [aDecoder decodeBoolForKey:SoundManagerisSFXEnabledKey];
        }
        
        if([aDecoder containsValueForKey:SoundManagerisMusicEnabledKey]) {
            _isMusicEnabled = [aDecoder decodeBoolForKey:SoundManagerisMusicEnabledKey];
        }
        
        NSString *musicFile = [aDecoder decodeObjectForKey:SoundManagerMusicFileKey];
        
        if (musicFile) {
            self.musicPlayer = [self createAudioPlayerToPlay:musicFile];
        }
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeBool:_isSFXEnabled forKey:SoundManagerisSFXEnabledKey];
    [aCoder encodeBool:_isMusicEnabled forKey:SoundManagerisMusicEnabledKey];
    
    if (self.musicPlayer.url) {
        NSString *fileName = [self.musicPlayer.url lastPathComponent];
        
        if (fileName) {
            [aCoder encodeObject:fileName forKey:SoundManagerMusicFileKey];
        }
    }
}

+ (instancetype)loadInstance
{
    return [PersistanceManager loadInstanceForClass:self
                                             andKey:SoundManagerPersistanceKey];
}

- (void)save
{
    [PersistanceManager saveObject:self forKey:SoundManagerPersistanceKey];
}

- (void)playSFX:(NSString *)sfx
{
    if (_isSFXEnabled) {
        DDLogDebug(@"Playing SFX:%@", sfx);
        [self playSound:sfx];
    }
}

- (void)playSound:(NSString *)sound
{
    SKAction *sfxAction = [self sfxActionForName:sound];
    
    if (sfxAction) {
        DDLogDebug(@"Playing sound:%@", sound);
        [SCENE runAction:sfxAction];
    }else {
        DDLogError(@"Error finding Preloaded SFX Action for: %@", sound);
    }
}

- (void)playSound:(NSString *)sound withKey:(NSString *)key
{
    DDLogDebug(@"Playing sound:%@", sound);
    [SCENE runAction:[SKAction playSoundFileNamed:sound
                                waitForCompletion:YES] withKey:key];
}

- (void)stopSoundWithKey:(NSString *)key
{
    [SCENE removeActionForKey:key];
}

- (BOOL)isSoundPlayingWithKey:(NSString *)key
{
    BOOL soundExists = NO;
    if([SCENE actionForKey:key]) {
        soundExists = YES;
    }
    return soundExists;
}


- (void)playMusic:(NSString *)music
{
    if (_isMusicEnabled) {
        DDLogDebug(@"Playing music:%@", music);
        [self playMusicUsingAVPlayer:music];
    }
}

- (void)pauseMusic
{
    DDLogDebug(@"Pausing music");
    [self pauseMusicUsingAVPlayer];
}

- (void)resumeMusic
{
    if (_isMusicEnabled) {
        DDLogDebug(@"Resuming music");
        [self continueMusicAVPlayer];
    }
}

- (void)stopMusic
{
    DDLogDebug(@"Stopping music");
    [self stopMusicUsingAVPlayer];
}

- (void)playMusicUsingAVPlayer:(NSString *)music
{
    @synchronized (self) {
        AVAudioPlayer *player = self.musicPlayer;
        
        if (!player) {
            player = [self createAudioPlayerToPlay:music];
            self.musicPlayer = player;
        }else {
            [self stopMusicUsingAVPlayer];
        }
        
        [player play];
    }
}

- (AVAudioPlayer *)createAudioPlayerToPlay:(NSString *)music
{
    NSString *musicPath = [[NSBundle mainBundle] pathForResource:music ofType:@""];
    NSURL *musicURL = [NSURL fileURLWithPath:musicPath];
    
    return [self createAudioPlayerUsingURL:musicURL];
}


- (AVAudioPlayer*)createAudioPlayerUsingURL:(NSURL *)musicURL
{
    NSError *err = NULL;
    AVAudioPlayer *player = [[AVAudioPlayer alloc] initWithContentsOfURL:musicURL
                                                                   error:&err];
    player.volume = GetGameMusicVolume();
    player.numberOfLoops = -1;
    
    return player;
}

- (void)pauseMusicUsingAVPlayer
{
    @synchronized (self) {
        AVAudioPlayer *player = self.musicPlayer;
        if (player && player.isPlaying) {
            [player pause];
        }
    }
}

- (void)stopMusicUsingAVPlayer
{
    @synchronized (self) {
        AVAudioPlayer *player = self.musicPlayer;
        
        if (player) {
            [player pause];
            player.currentTime = 0.;
        }
    }
}

- (void)continueMusicAVPlayer
{
    @synchronized (self) {
        AVAudioPlayer *player = self.musicPlayer;
        
        if (player) {
            [player play];
        }
    }
}

- (void)setIsSFXEnabled:(BOOL)isSFXEnabled
{
    if (_isSFXEnabled != isSFXEnabled) {
        _isSFXEnabled = isSFXEnabled;
        
        [self save];
    }
}

- (void)setIsMusicEnabled:(BOOL)isMusicEnabled
{
    if (_isMusicEnabled != isMusicEnabled) {
        _isMusicEnabled = isMusicEnabled;
        
        if (_isMusicEnabled) {
            [self resumeMusic];
        }else {
            [self pauseMusic];
        }
        
        [self save];
    }
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end

//
//  Utils.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 31/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <appirater/Appirater.h>

@import Firebase;

@interface Utils : NSObject

+ (void)configureLogging;
+ (void)configureGoogleAdmob;
+ (void)configureFirebase;
+ (void)configureRemoteConfig;
+ (void)configureAppiraterAndShow;
+ (void)configureRemoteNotifications:(id)delegate;
+ (void)configureLocalNotifications;

+ (void)fetchRemoteConfigValuesWithExpirationDuration:(NSTimeInterval)expiration
                                           completion:(FIRRemoteConfigFetchCompletion)completion;

+ (FIRRemoteConfigValue *)getRemoteConfigValueWithKey:(NSString *)key;

+ (void)showAlertForRemoteNotificationUserInfo:(NSDictionary *)userInfo
                              inViewController:(UIViewController *)viewController;

@end

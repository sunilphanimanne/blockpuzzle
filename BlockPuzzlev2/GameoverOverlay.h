//
//  GameoverOverlay.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 27/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameoverOverlay : SKSpriteNode

@property (nonatomic, assign) NSNumber *score;
@property (nonatomic, assign) NSNumber *highScore;

- (void)fadein;
- (void)fadeoutWithCompletion:(void (^)())block;

@end

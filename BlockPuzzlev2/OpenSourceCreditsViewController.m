//
//  OpenSourceCreditsViewController.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 22/02/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import "OpenSourceCreditsViewController.h"

#import "GameConstants.h"

@implementation OpenSourceCreditsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadTheOpenSourceCreditsHTML];
}

- (void)loadTheOpenSourceCreditsHTML
{
    NSError *fileError = nil;
    NSString *htmlPageContentString = [GameUtils stringContentsOfFile:self.openSourceCreditsHTMLPage
                                                                error:&fileError];
    if (!fileError) {
        [self.creditsWebView loadHTMLString:htmlPageContentString
                                    baseURL:nil];
    }
}

- (IBAction)dismissAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end

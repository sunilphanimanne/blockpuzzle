//
//  TouchLayer.m
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 07/03/16.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "TouchLayer.h"

#import "GameScene.h"
#import "GameConstants.h"

@implementation TouchLayer

@synthesize frame;
@synthesize zPosition;
@synthesize touchesEnabled;
@synthesize isMutuallyExclusive;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.touchesEnabled = YES;
    }
    return self;
}
@end

//
//  AVVideoDemoController.h
//  BlockPuzzle
//
//  Created by Sunil Phani Manne on 16/01/17.
//  Copyright © 2017 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AVVideoDemoController : AVPlayerViewController

- (instancetype)initWithVideoName:(NSString *)videoName
          presentOnViewController:(UIViewController *)viewController;

- (void)playDemo:(void (^)())completion;
- (void)playDemoIfRequired:(void (^)())completion;

@end
